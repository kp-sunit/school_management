<?php
ob_start();
session_start();
include_once("./includes/config.php");

require('fpdf181/fpdf.php');

function hex2dec($couleur = "#000000"){
    $R = substr($couleur, 1, 2);
    $rouge = hexdec($R);
    $V = substr($couleur, 3, 2);
    $vert = hexdec($V);
    $B = substr($couleur, 5, 2);
    $bleu = hexdec($B);
    $tbl_couleur = array();
    $tbl_couleur['R']=$rouge;
    $tbl_couleur['V']=$vert;
    $tbl_couleur['B']=$bleu;
    return $tbl_couleur;
}

//conversion pixel -> millimeter at 72 dpi
function px2mm($px){
    return $px*25.4/72;
}

function txtentities($html){
    $trans = get_html_translation_table(HTML_ENTITIES);
    $trans = array_flip($trans);
    return strtr($html, $trans);
}
////////////////////////////////////

class PDF_HTML extends FPDF
{
//variables of html parser
protected $B;
protected $I;
protected $U;
protected $HREF;
protected $fontList;
protected $issetfont;
protected $issetcolor;

function __construct($orientation='P', $unit='mm', $format='A4')
{
    //Call parent constructor
    parent::__construct($orientation,$unit,$format);
    //Initialization
    $this->B=0;
    $this->I=0;
    $this->U=0;
    $this->HREF='';
    $this->fontlist=array('arial', 'times', 'courier', 'helvetica', 'symbol');
    $this->issetfont=false;
    $this->issetcolor=false;
}

function WriteHTML($html)
{
    //HTML parser
    $html=strip_tags($html,"<b><u><i><a><img><p><br><strong><em><font><tr><blockquote>"); //supprime tous les tags sauf ceux reconnus
    $html=str_replace("\n",' ',$html); //remplace retour à la ligne par un espace
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE); //éclate la chaîne avec les balises
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(5,stripslashes(txtentities($e)));
        }
        else
        {
            //Tag
            if($e[0]=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag, $attr)
{
    //Opening tag
    switch($tag){
        case 'STRONG':
            $this->SetStyle('B',true);
            break;
        case 'EM':
            $this->SetStyle('I',true);
            break;
        case 'B':
        case 'I':
        case 'U':
            $this->SetStyle($tag,true);
            break;
        case 'A':
            $this->HREF=$attr['HREF'];
            break;
        case 'IMG':
            if(isset($attr['SRC']) && (isset($attr['WIDTH']) || isset($attr['HEIGHT']))) {
                if(!isset($attr['WIDTH']))
                    $attr['WIDTH'] = 0;
                if(!isset($attr['HEIGHT']))
                    $attr['HEIGHT'] = 0;
                $this->Image($attr['SRC'], $this->GetX(), $this->GetY(), px2mm($attr['WIDTH']), px2mm($attr['HEIGHT']));
            }
            break;
        case 'TR':
        case 'BLOCKQUOTE':
        case 'BR':
            $this->Ln(5);
            break;
        case 'P':
            $this->Ln(10);
            break;
        case 'FONT':
            if (isset($attr['COLOR']) && $attr['COLOR']!='') {
                $coul=hex2dec($attr['COLOR']);
                $this->SetTextColor($coul['R'],$coul['V'],$coul['B']);
                $this->issetcolor=true;
            }
            if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist)) {
                $this->SetFont(strtolower($attr['FACE']));
                $this->issetfont=true;
            }
            break;
    }
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='STRONG')
        $tag='B';
    if($tag=='EM')
        $tag='I';
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
    if($tag=='FONT'){
        if ($this->issetcolor==true) {
            $this->SetTextColor(0);
        }
        if ($this->issetfont) {
            $this->SetFont('arial');
            $this->issetfont=false;
        }
    }
}

function SetStyle($tag, $enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
    {
        if($this->$s>0)
            $style.=$s;
    }
    $this->SetFont('',$style);
}

function PutLink($URL, $txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(5,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}

}//end of class

	
	 $sql="SELECT * FROM `school_students` WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'";
//	exit;
	$rs=mysql_query($sql) or die(mysql_error());
	if($row=mysql_fetch_array($rs))
	{

		$fetch_class=mysql_query("select * from `classname`  where id='".$row['class_id']."'");	
		$class=mysql_fetch_array($fetch_class);
                $rowsimg = mysql_fetch_array(mysql_query("SELECT `image` FROM `school_admin` WHERE `id`='".mysql_real_escape_string($row['userid'])."'"));

                $rowsection = mysql_fetch_array(mysql_query("SELECT `sectionname` FROM `sectionname` WHERE `id`='" . mysql_real_escape_string($row['section_id']) . "'"));

                $rowreligion = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_religion` WHERE `id`='" . mysql_real_escape_string($row['religion']) . "'"));
                $ocuf = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_ocupation` WHERE `id`='" . mysql_real_escape_string($row['fatherocupation']) . "'"));
                $ocum = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_ocupation` WHERE `id`='" . mysql_real_escape_string($row['motherocupation']) . "'"));
                $sqlpro=mysql_query("SELECT * FROM `school_adminprofile` where userid=1"); 
                $rowpro=mysql_fetch_array($sqlpro);

                $rowparent = mysql_fetch_array(mysql_query("SELECT * FROM `school_admin` WHERE `id`='".mysql_real_escape_string($row['parentid'])."'"));
                $rowsession = mysql_fetch_array(mysql_query("SELECT `scheme` FROM `school_exam_scheme` WHERE `id`='" . mysql_real_escape_string($row['session_id']) . "'"));

                $rowshift = mysql_fetch_array(mysql_query("SELECT `shiftname` FROM `shiftname` WHERE `id`='" . mysql_real_escape_string($row['shiftid']) . "'"));
                $rowgroup = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_new_group` WHERE `id`='" . mysql_real_escape_string($row['group']) . "'"));
                $studentty = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_studenttype` WHERE `id`='" . mysql_real_escape_string($row['studenttype'])."'"));
                $speciqu = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_quota` WHERE `id`='" . mysql_real_escape_string($row['specialquata'])."'"));
                $sqladmin=mysql_query("SELECT * FROM `school_admin` where id=1"); 
                $rowadmin=mysql_fetch_array($sqladmin);
                 if($rowadmin['image']==''){
                   $image_link='http://mhcds.dartmouth.edu/images/made/uploads/mhcds/images/img_profile_goldberg_301_303_s_c1.jpg';
                 }else{
                    $image_link=SITE_URL.'upload/documents/'.$rowadmin['image'];
                 }

                 if($rowsimg['image']==''){
                   $image_linkstu=SITE_URL.'upload/no.png';
                 }else{
                    $image_linkstu=SITE_URL.'upload/documents/'.$rowsimg['image'];
                 }
                 if(empty($rowgroup['name'])){
                    $groupname='None';
                }else{
                    $groupname=stripslashes($rowgroup['name']);
                } 

 if(empty($studentty['name'])){
    $studentty='None';
}else{
    $studentty=stripslashes($studentty['name']);
} 

$fetch_sql="select * from `school_subjectgroup`  where 1";
if(!empty($row['class_id']))
{
    $fetch_sql.=" and class_id='".$row['class_id']."'";
}
//            if(!empty($_REQUEST["section_id"]))
//            {
//                $fetch_sql.=" and section_id='".$_REQUEST['section_id']."'";
//            }
if(!empty($row['shiftid']))
{
    $fetch_sql.=" and shift_id='".$row['shiftid']."'";
}
if(!empty($row['group']))
{
    $fetch_sql.=" and allgroup='".$row['group']."'";
}
$fetch_groupsubject=mysql_query($fetch_sql) or die(mysql_error());

$subjectgroup=mysql_fetch_assoc($fetch_groupsubject);

$assign_subject=mysql_fetch_assoc(mysql_query("select * from school_assign_subject where student_primary_id='".$row['id']."'"));
$religion_subject=  array();
$group_subject=array();
$gr_third_fourth_sub=array();
if(!empty($assign_subject))
{

$common_subject=explode(",",$subjectgroup['common_subject']);
$religion_subject=explode(",",$assign_subject['religion_subject_id']);

$group_subject=explode(",",$subjectgroup['group_subject']);

if(!empty($assign_subject['thirdfourth_subject_id']))
{
$gr_third_fourth_sub=explode(",",$assign_subject['thirdfourth_subject_id']);
}
$subject_lists=  array_filter(array_merge($common_subject,$religion_subject,$group_subject,$gr_third_fourth_sub));
$subject_lists=array_unique($subject_lists); 

$query=mysql_query("select * from `allsubject` where is_deleted=0 and id in (".implode(',',$subject_lists).") order by list_order") or die(mysql_error());
while($sub=mysql_fetch_assoc($query))
{
    $subject_sortcodes[]=$sub['subjectcode'];
}
if(!empty($assign_subject['fourth_subject_id'])){
$fourth_subject=mysql_fetch_assoc(mysql_query("select * from `allsubject` where id='".$assign_subject['fourth_subject_id']."'")); 
}
}
$header='<div style="width:100%;margin:0 auto;text-align: center;"><img height='.$rowadmin['logo_height'].' width='.$rowadmin['logo_width'].' src="'.$image_link.'" ></div>';
$header.='<div style="width:100%;margin:0 auto;text-align: center;color:#000000;font-size:'.$rowadmin['institute_fontsize'].'px;font-weight:bold;">'.$rowpro['institution'].'</div>';
$header.='<div style="width:100%;margin:0 auto;text-align: center;color:#3a91d9;border-bottom:1px solid #ccc;font-size:'.$rowadmin['heading_fontsize'].'px;font-weight:bold; padding-bottom:'.$rowadmin['space_from_heading'].'px;">SUBJECT\'S INFORMATION</div>';
$signs= array();
if($_REQUEST['signature'])
{
    foreach ($_REQUEST['signature'] as $key =>$val)
    {
        $signs[$_REQUEST['signature_order'][$key]]=$val;
        
        
        
    }
    ksort($signs); 
    foreach ($signs as $sign)
    {
        $signature[]=$sign;
    }
      $count=count($signature);
//    echo "<pre>";
//    print_r()
    

}
if($count==1)
{
    $footer='<div style="float:left;width:100%;text-align:right;">______________________________________<br />'.$signature[0].'</div>';  

}
elseif ($count==2) {
    $footer='<div style="float:left;width:40%;">______________________________________<br />'.$signature[0].'</div>'
            . '<div style="float:left;width:60%;text-align:right;">______________________________________<br />'.$signature[1].'</div>';  

}
else
{
    $footer='<div style="float:left;width:32%;text-align:right;">______________________________<br />'.$signature[0].'</div><div style="float:left;width:32%;text-align:right;">___________________________<br />'.$signature[1].'</div>'
            . '<div style="float:left;width:32%;text-align:right;">_________________________<br />'.$signature[2].'</div>';  
}    
$footer.="<div style='width:100%;padding-top:25px;'>Print Date:".date('d-m-Y')." Page No:{PAGENO}</div>";
$html.='<table style="width:100%;"  cellpadding="2">
<tr><td style="width:175px; padding-left:50px;"><img height="100"  width="100" src="'.$image_linkstu.'" style="float:right" /></td><td style="font-weight:bold;width:365px; ">'.$row['name'].'<br />Student ID:&nbsp;'.$row['studentid'].'</td><th style="margin-left:1100px;"><br /><br /><br /><br />'.date('j  F Y ').'</th></tr>
</table>

<table style="width:100%;"  cellpadding="3">

<tr><td style="color:#000;font-size:14px;font-weight:bold;width:100%;color:#3a91d9;" colspan="4" >Personal Information </td></tr>
<tr>
<td style="font-size:14px;text-align:right;width:25%;">Applicant Name (Bang)</td>
<th style="font-family:nikosh;font-size:14px;text-align:left;" > : '.  $row['studentnameben'].'</th>'
        . '<td style="font-size:14px;text-align:right;">Mother</td>'
        . '<th style="font-size:14px;text-align:left;"> : '.$row['mothername'].'</th>
            </tr>
<tr>
    <td style="font-size:14px;text-align:right;">Class</td>
    <th style="font-size:14px;text-align:left;"> : '.$class['classname'].'</th>'
    .'<td style="font-size:14px;text-align:right;">Shift</td>'
    . '<th style="font-size:14px;text-align:left;"> : '.$rowshift['shiftname'].'</th>
</tr>
<tr>
    <td style="font-size:14px;text-align:right;">Group</td>
    <th style="font-size:14px;text-align:left;"> : '.$groupname.'</th>'
 . '<td style="font-size:14px;text-align:right;">Section</td>'
 . '<th style="font-size:14px;text-align:left;"> : '.$rowsection['sectionname'].'</th>
 </tr>
<tr>
    <td style="font-size:14px;text-align:right;">Roll</td>
    <th style="font-size:14px;text-align:left;"> : '.$row['roll'].'</th>'
 . '<td style="font-size:14px;text-align:right;">Session</td>'
  . '<th style="font-size:14px;text-align:left;"> : '.$rowsession['scheme'].'</th>
 </tr>
<tr>
    <td style="font-size:14px;text-align:right;">Date of Birth </td>
    <th style="font-size:14px;text-align:left;"> : '.$row['dob'].'</th>'
  . '<td  style="font-size:14px;text-align:right;">Gender</td>'
  . '<th style="font-size:14px;text-align:left;"> : '.ucwords($row['gender']).'</th>
</tr>
<tr>
    <td  style="font-size:14px;text-align:right;">Blood Group</td>
    <th style="font-size:14px;text-align:left;"> : '.ucfirst($row['bloodgroup']).'</th>'
  . '<td  style="font-size:14px;text-align:right;">Religion </td>'
  . '<th style="font-size:14px;text-align:left;"> : '.$rowreligion['name'].'</th></tr>
<tr>
    <td  style="font-size:14px;text-align:right;">Student Type </td>
    <th style="font-size:14px;text-align:left;"> : '.$studentty.'</th>
    <td  style="font-size:14px;text-align:right;">Special Quota </td>
    <th style="font-size:14px;text-align:left;"> : '.$speciqu['name'].'</th></tr>

    

<tr>
        <td style="color: #3a91d9;font-size:14px;font-weight: bold;" colspan="4">Subject\'s Information  </td></tr>';
$html.= '<tr><td colspan=4 align:center;>';
$html.='<table style="width:100%;border-collapse: collapse;"  cellpadding="0">';
$html.='<tr>
            <th style="border: 1px solid black;text-align:center;">Order</th>
            <th style="border: 1px solid black;text-align:center;">Subject Name</th>
            <th style="border: 1px solid black;text-align:center;">Subject Short Name</th>
            <th style="border: 1px solid black;text-align:center;">Subject Code</th>
        </tr>';
$query=mysql_query("select * from `allsubject` where is_deleted=0 and id in (".implode(',',$subject_lists).") order by list_order") or die(mysql_error());
$p=1;
while($sub=mysql_fetch_assoc($query))
{

$html.='<tr>
            <td style="border: 1px solid black;text-align:center;">'.$p.'</td>
            <td style="border: 1px solid black;text-align:left;">'.$sub['subjectname'].'</td>
            <td style="border: 1px solid black;text-align:left;">'.$sub['subject_sort_name'].'</td>
            <td style="border: 1px solid black;text-align:center;">'.$sub['subjectcode'].'</td>
        </tr>';
$p++;
}
if(!empty($fourth_subject))
{
    $html.='<tr>
            <td style="border: 1px solid black;text-align:center;">'.$p.'</td>
            <td style="border: 1px solid black;text-align:left;">'.$fourth_subject['subjectname'].'</td>
            <td style="border: 1px solid black;text-align:left;">'.$fourth_subject['subject_sort_name'].'</td>
            <td style="border: 1px solid black;text-align:center;">'.$fourth_subject['subjectcode'].'</td>
        </tr>';
    
}

$html.="</table>";
$html.='</td></tr>';
        

$html.='</tr>';


$html.='

   </table></body></html>';
//echo $html;exit;

include("mpdf60/mpdf.php");
$mpdf=new mPDF('', 'A4-P','',''); 
$mpdf->setAutoTopMargin ='stretch';
$mpdf->setAutoBottomMargin ='stretch';
$mpdf->SetHTMLHeader($header);  
$mpdf->SetHTMLFooter($footer); 
$mpdf->WriteHTML($html);	
$mpdf->Output('studentsubject.pdf','D');
exit;	
}
else
{
$msg="Invalid Username or Password.";
$_SESSION['msg']=$msg;
}


?>

  

