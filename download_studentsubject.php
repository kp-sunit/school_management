<?php
ob_start();
session_start();
include_once("./includes/config.php");

require('fpdf181/fpdf.php');

function hex2dec($couleur = "#000000"){
    $R = substr($couleur, 1, 2);
    $rouge = hexdec($R);
    $V = substr($couleur, 3, 2);
    $vert = hexdec($V);
    $B = substr($couleur, 5, 2);
    $bleu = hexdec($B);
    $tbl_couleur = array();
    $tbl_couleur['R']=$rouge;
    $tbl_couleur['V']=$vert;
    $tbl_couleur['B']=$bleu;
    return $tbl_couleur;
}

//conversion pixel -> millimeter at 72 dpi
function px2mm($px){
    return $px*25.4/72;
}

function txtentities($html){
    $trans = get_html_translation_table(HTML_ENTITIES);
    $trans = array_flip($trans);
    return strtr($html, $trans);
}
////////////////////////////////////

class PDF_HTML extends FPDF
{
//variables of html parser
protected $B;
protected $I;
protected $U;
protected $HREF;
protected $fontList;
protected $issetfont;
protected $issetcolor;

function __construct($orientation='P', $unit='mm', $format='A4')
{
    //Call parent constructor
    parent::__construct($orientation,$unit,$format);
    //Initialization
    $this->B=0;
    $this->I=0;
    $this->U=0;
    $this->HREF='';
    $this->fontlist=array('arial', 'times', 'courier', 'helvetica', 'symbol');
    $this->issetfont=false;
    $this->issetcolor=false;
}

function WriteHTML($html)
{
    //HTML parser
    $html=strip_tags($html,"<b><u><i><a><img><p><br><strong><em><font><tr><blockquote>"); //supprime tous les tags sauf ceux reconnus
    $html=str_replace("\n",' ',$html); //remplace retour à la ligne par un espace
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE); //éclate la chaîne avec les balises
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(5,stripslashes(txtentities($e)));
        }
        else
        {
            //Tag
            if($e[0]=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag, $attr)
{
    //Opening tag
    switch($tag){
        case 'STRONG':
            $this->SetStyle('B',true);
            break;
        case 'EM':
            $this->SetStyle('I',true);
            break;
        case 'B':
        case 'I':
        case 'U':
            $this->SetStyle($tag,true);
            break;
        case 'A':
            $this->HREF=$attr['HREF'];
            break;
        case 'IMG':
            if(isset($attr['SRC']) && (isset($attr['WIDTH']) || isset($attr['HEIGHT']))) {
                if(!isset($attr['WIDTH']))
                    $attr['WIDTH'] = 0;
                if(!isset($attr['HEIGHT']))
                    $attr['HEIGHT'] = 0;
                $this->Image($attr['SRC'], $this->GetX(), $this->GetY(), px2mm($attr['WIDTH']), px2mm($attr['HEIGHT']));
            }
            break;
        case 'TR':
        case 'BLOCKQUOTE':
        case 'BR':
            $this->Ln(5);
            break;
        case 'P':
            $this->Ln(10);
            break;
        case 'FONT':
            if (isset($attr['COLOR']) && $attr['COLOR']!='') {
                $coul=hex2dec($attr['COLOR']);
                $this->SetTextColor($coul['R'],$coul['V'],$coul['B']);
                $this->issetcolor=true;
            }
            if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist)) {
                $this->SetFont(strtolower($attr['FACE']));
                $this->issetfont=true;
            }
            break;
    }
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='STRONG')
        $tag='B';
    if($tag=='EM')
        $tag='I';
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
    if($tag=='FONT'){
        if ($this->issetcolor==true) {
            $this->SetTextColor(0);
        }
        if ($this->issetfont) {
            $this->SetFont('arial');
            $this->issetfont=false;
        }
    }
}

function SetStyle($tag, $enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
    {
        if($this->$s>0)
            $style.=$s;
    }
    $this->SetFont('',$style);
}

function PutLink($URL, $txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(5,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}

}//end of class

	
	 $sql="select * from school_students   where 1 and is_deleted=0";
         if ($_REQUEST['class_id'] != '') {
                                                $sql .= " and class_id = '" . $_REQUEST['class_id'] . "'";
                                            }

                                            if ($_REQUEST['section_id'] != '') {
                                                $sql .= " and section_id = '" . $_REQUEST['section_id'] . "'";
                                            }
                                            if ($_REQUEST['shift_id'] != '') {
                                                $sql .= " and shiftid = '" . $_REQUEST['shift_id'] . "'";
                                            }

                                            if ($_REQUEST['group'] != '') {
                                                $sql .= " and group = '" . $_REQUEST['group'] . "'";
                                            }
                                            
                                            //echo $sql;exit;
//	exit;
	$rs=mysql_query($sql) or die(mysql_error());
	if($row=mysql_fetch_array($rs))
	{

		$fetch_class=mysql_query("select * from `classname`  where id='".$row['class_id']."'");	
		$class=mysql_fetch_array($fetch_class);
              
                $sqlpro=mysql_query("SELECT * FROM `school_adminprofile` where userid=1"); 
                $rowpro=mysql_fetch_array($sqlpro);
                $rowparent = mysql_fetch_array(mysql_query("SELECT * FROM `school_admin` WHERE `id`='".mysql_real_escape_string($row['parentid'])."'"));
                
                $sqladmin=mysql_query("SELECT * FROM `school_admin` where id=1"); 
                $rowadmin=mysql_fetch_array($sqladmin);
                $content="";
                
              while ($product = mysql_fetch_array($rs)) {
              $rowimage = mysql_fetch_array(mysql_query("SELECT `image`,`email`,`mobile` FROM `school_admin` WHERE `id`='" . mysql_real_escape_string($product['userid']) . "'"));



                                                if ($rowimage['image'] != '') {
                                                    $image_link = 'upload/documents/' . $rowimage['image'];
                                                } else {
                                                    $image_link = 'upload/no.png';
                                                }


                                                $rowshift = mysql_fetch_array(mysql_query("SELECT `shiftname` FROM `shiftname` WHERE `id`='" . mysql_real_escape_string($product['shiftid']) . "'"));

                                                $rowclass = mysql_fetch_array(mysql_query("SELECT `classname` FROM `classname` WHERE `id`='" . mysql_real_escape_string($product['class_id']) . "'"));

                                                $rowsection = mysql_fetch_array(mysql_query("SELECT `sectionname` FROM `sectionname` WHERE `id`='" . mysql_real_escape_string($product['section_id']) . "'"));

                                                $rowgroup = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_groupstudent` WHERE `id`='" . mysql_real_escape_string($product['group']) . "'"));
                                                $rowreligion = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_religion` WHERE `id`='" . mysql_real_escape_string($product['religion']) . "'"));
                                                $rowsession = mysql_fetch_array(mysql_query("SELECT `scheme` FROM `school_exam_scheme` WHERE `id`='" . mysql_real_escape_string($product['session_id']) . "'"));
                                                $rowstudenttype = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_studenttype` WHERE `id`='" . mysql_real_escape_string($product['studenttype']) . "'"));    
                                                $row_school_assign_subject=mysql_fetch_assoc(mysql_query("select * from school_assign_subject where student_primary_id='".$product['id']."'"));
                                                $select_subjects=array();
                                                $assign_subjects= array();
                                                if(!empty($row_school_assign_subject))
                                                {
                                                    if($row_school_assign_subject["group_subject_id"])
                                                    {
                                                        $assign_subjects[]=$row_school_assign_subject["group_subject_id"];
                                                    }
                                                    if($row_school_assign_subject["religion_subject_id"])
                                                    {
                                                        $assign_subjects[]=$row_school_assign_subject["religion_subject_id"];
                                                    }
                                                    if($row_school_assign_subject["thirdfourth_subject_id"])
                                                    {
                                                        $assign_subjects[]=$row_school_assign_subject["thirdfourth_subject_id"];
                                                    }
                                                    if($row_school_assign_subject["fourth_subject_id"])
                                                    {
                                                        $assign_subjects[]=$row_school_assign_subject["fourth_subject_id"];
                                                    }
                                                     $assign_subjects=implode(",",$assign_subjects);
                                                   
                                                   
                                                    $fetch_subject=mysql_query("select * from `allsubject`  where id in(".$assign_subjects.")") or die(mysql_error());
                                                    while($row_subject=mysql_fetch_assoc($fetch_subject))
                                                    {
                                                        $select_subjects[]=$row_subject['subjectcode'];
                                                    }
                                                   
                                                    
                                                }
                                                else{
                                                    $select_subjects="";
                                                }
                                                
                                                
                                                $content.="<tr style='border: 1px solid black'>
                                                    <td style='text-align:center;border: 1px solid black'> ".$product["id"]."</td>                                                    
                                                    <td style='text-align:center;border: 1px solid black'><img src='".$image_link."' alt='' style='width:75px; height:75px;' /></td>
                                                    <td style='text-align:center;border: 1px solid black'>".$product['roll']."</td>
                                                    <td style='text-align:center;border: 1px solid black'>". stripslashes($product['name'])."</td>                                                    
                                                    <td style='text-align:center;border: 1px solid black'>
                                                        ". stripslashes($product['dob'])."/<br>
                                                         ". stripslashes($product['gender'])."/<br>
                                                         ".$rowreligion['name']."
                                                    </td>                                                    
                                                    <td style='text-align:center;border: 1px solid black'>".stripslashes($rowshift['shiftname'])." / <br>".stripslashes($rowclass['classname'])."/<br>".stripslashes($rowsection['sectionname'])."</td>
                                                    <td style='text-align:center;border: 1px solid black'>".stripslashes($rowgroup['name'])." / <br />".stripslashes($rowsession['scheme'])."/<br/>".stripslashes($rowstudenttype['name'])."</td>
                                                    <td style='text-align:center;border: 1px solid black'>".implode(",",$select_subjects)."</td>";
              }

                
                
 if($rowadmin['image']==''){
   $image_link='http://mhcds.dartmouth.edu/images/made/uploads/mhcds/images/img_profile_goldberg_301_303_s_c1.jpg';
 }else{
    $image_link=SITE_URL.'upload/documents/'.$rowadmin['image'];
 }

 if($rowsimg['image']==''){
   $image_linkstu=SITE_URL.'upload/no.png';
 }else{
    $image_linkstu=SITE_URL.'upload/documents/'.$rowsimg['image'];
 }
 if(empty($rowgroup['name'])){
    $groupname='None';
}else{
    $groupname=stripslashes($rowgroup['name']);
} 

 if(empty($studentty['name'])){
    $studentty='None';
}else{
    $studentty=stripslashes($studentty['name']);
} 




$html='<html><head><meta charset="utf-8"/>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <style>@page *{
    margin-top: 1cm;
   
   
}</style>


</head> <body><table style="width:100%;"  cellpadding="2">
       <tr><td colspan="3" style="text-align: center;"> <img height="60" width="60" src="'.$image_link.'" ></td></tr>
       <tr><td colspan="3"  style="text-align: center;color:#000000;font-size:20px;font-weight:bold;">'.$rowpro['institution'].'</td></tr>
        <tr><td colspan="3"  style="text-align: center;font-size:18px;">List of Assign Subjects</td></tr>
</table>
<div style="width:80%;margin:0 auto;">
<table style="width:100%;"  cellpadding="3">

<tr>
    <td colspan="3">
        <table style="width:100%;border-collapse: collapse;" >
        <tr style="border: 1px solid black">
            <th style="text-align:center;border: 1px solid black">ID</th>
            <th style="text-align:center;border: 1px solid black">Photo</th>
            <th style="text-align:center;border: 1px solid black">Roll</th>
            <th style="text-align:center;border: 1px solid black">Name</th>
            <th style="text-align:center;border: 1px solid black">Date of Birth/ <br />
                                                    Gender/<br>
                                                    Religion</th>
            <th style="text-align:center;border: 1px solid black">Shift/ <br />
                                                    Class/<br>
                                                    Section</th>
            <th style="text-align:center;border: 1px solid black">Group/  <br />
                                                    Session/<br>
                                                    Type</th>
            <th style="text-align:center;border: 1px solid black">Subject Code</th>

         </tr>   
         '.$content.'
             
<tr style="padding-top:15px;">
<td  style="padding-top:15px;text-align:right;font-weight:bold;text-align:right;" colspan="2" >Signature</td>
<td  style="padding-top:15px;" colspan="5" ></td>
<td  style="padding-top:15px;font-weight:bold;text-align:center;" >Signature</td>
</tr>
<tr style="padding-top:15px;">
<td ></td>
<td  style="padding-top:15px;font-weight:bold;text-align:center;" colspan="2" >Class  Teacher</td>
<td  style="padding-top:15px;" colspan="4" ></td>
<td  style="padding-top:15px;font-weight:bold;text-align:center;"  >Head of the Institute</td>
</tr>
        </table>

     </td>
</tr>





   </table>
</div>
</body></html>';
//echo $html;exit;

	include("mpdf60/mpdf.php");
	
	$mpdf=new mPDF('', 'A4-P','',''); 
   // $mpdf->autoLangToFont = true;
	$mpdf->WriteHTML($html);

	$mpdf->Output('subjectdistribution.pdf','D');
	exit;	

	}
	else
	{
		$msg="Invalid Username or Password.";
        $_SESSION['msg']=$msg;
	}


?>

  

