<?php 
   include_once("./includes/session.php");
   
   //include_once("includes/config.php");
   
   include_once("./includes/config.php");
   require('fpdf181/fpdf.php');

   
   $url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
   
   ?>
<?php
   if(isset($_REQUEST['print']))
   {
       $sql="select * from `school_students` where 1";
       if(!empty($_REQUEST["studentid"]))
       {
           $sql.=" and studentid='".$_REQUEST["studentid"]."'";
       }
       if(!empty($_REQUEST["shift_id"]))
       {
           $sql.=" and shiftid='".$_REQUEST["shift_id"]."'";
       }
       if(!empty($_REQUEST["class_id"]))
       {
           $sql.=" and class_id='".$_REQUEST["class_id"]."'";
       }
       if(!empty($_REQUEST["section_id"]))
       {
           $sql.=" and section_id='".$_REQUEST["section_id"]."'";
       }
       if(!empty($_REQUEST["group"]))
       {
           $sql.=" and school_students.group='".$_REQUEST["group"]."'";
       }
//       echo $sql;
//       exit;
       $rs=mysql_query($sql) or die(mysql_error());
       $html="";
	while($row=mysql_fetch_assoc($rs))
	{
            

		$fetch_class=mysql_query("select * from `classname`  where id='".$row['class_id']."'");	
		$class=mysql_fetch_array($fetch_class);
                $sqlpro=mysql_query("SELECT * FROM `school_adminprofile` where userid=1"); 
                $rowpro=mysql_fetch_array($sqlpro);

                $sqladmin=mysql_query("SELECT * FROM `school_admin` where id=1"); 
                $rowadmin=mysql_fetch_array($sqladmin);
                 if($rowadmin['image']==''){
                   $image_link='http://mhcds.dartmouth.edu/images/made/uploads/mhcds/images/img_profile_goldberg_301_303_s_c1.jpg';
                 }else{
                    $image_link=SITE_URL.'upload/documents/'.$rowadmin['image'];
                 }
                 $profile_img=!empty($row['image'])?"./upload/documents/".$row['image']:"./upload/no.png";
            $html.='<div class="full_width" style="width:100%;float: left;padding:5px;overflow: hidden;position: relative;margin: 0 auto;">
        <div class="logo_part" style="width: 98%;float: left;padding: 10px;display: flex;align-items:center;margin: 0 auto;">
            <div class="logo" style="width: 20%;float:left;">
                <img src="'.$image_link.'" style="height:200px;" >
            </div>
            <div class="name_logo" style="width:70%;float: left; text-align: center;">
                <h3 style="color: #da026e;font-size:40px;font-weight: 700; margin: 0;">'.$rowpro['institution'].'</h3>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div class="admit" style="width: 70%; margin:20px auto; text-align: center;color:#000099;float: left;font-size: 28px;font-weight: 700;">Admit Card</div>
        <div class="logo_part" style="width: 98%;float: left;padding: 15px; border: 1px solid #000;margin: 0 auto;">
            <div class="information" style="width: 68%;float: left; padding: 5px;">
            
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">Roll </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">: '.sprintf("%03d", $row['roll']).' </h4>
                    </div>
                </div>
                
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width:35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">Class </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">: '.$class['classname'].' </h4>
                    </div>
                </div>
                
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">Name </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">: '.$row['name'].' </h4>
                    </div>
                </div>
                
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">Fathers Name </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">: '.$row['guardianname'].' </h4>
                    </div>
                </div>
                
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">Mothers Name </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">: '.$row['mothername'].' </h4>
                    </div>
                </div>
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">Date of Birth </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">: '.$row['dob'].'</h4>
                    </div>
                </div>
            </div>
            <div class="pics" style="width: 20%;float: right;padding: 5px;">
                <div class="ad_pic" style="padding: 5px;text-align: center;">
                    <img src="'.$profile_img.'" style="border: 1px solid #000; padding: 5px; margin: 0 auto; width: auto;height: 150px">
                </div>
                <div class="ad_pic" style="padding: 5px;text-align: center;margin: 15px auto;">
                    <img src="./upload/documents/'.$rowpro['signaturehead'].'" style="margin: 0 auto; width: auto;height: 100px">
                </div>
                <div class="ad_pic" style="padding: 5px;text-align: center;margin: 0 auto;">
                    <h4 style="color: #000;font-size:14px;font-weight: 600;margin: 2px 0;">Signature Head of Institute</h4>
                 
                </div>
                
            </div>
        </div>
        <div class="admit" style="background:#d9d9d9; width: 100%; margin:20px auto; text-align: center;color:#921600;float: left;font-size: 31px;font-weight: 700;">General Instruction of Applicants</div>
        <div class="logo_part" style="width: 98%;float: left;padding: 15px; border: 1px solid #000;margin: 0 auto;">
            <div class="information" style="width: 100%;float: left; padding: 5px;">
            
                <div class="infos" style="width:70%;float: left;">
                    <div class="fields" style="width: 100%;float:left">
                        <h4 style="color: #000;font-size:10px;font-weight: 600; margin: 10px 0;">'.$rowpro['instructionpdf'].' </h4>
                    </div>
                </div>
              
                </div>
            </div>
        </div>';

            
        }
        
       echo $html;
        
   }
   ?>
<script>
    window.print();
</script>    