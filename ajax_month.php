<?php 
   include_once("./includes/session.php");
   include_once("./includes/config.php");
    
   if($_REQUEST["action"]=='fetchmonth')
   {
       $months= array(1=>"January",2=>"February",3=>"March",4=>"April",5=>"May",6=>"June",7=>"July",8=>"August",9=>"September",10=>"October",11=>"November",
                 12=>"December"   
       );
      
       $select_year=$_REQUEST['select_year'];
       $select_month=$_REQUEST['select_month'];
       if(!empty($select_month))
       {
           $newmonths=array($select_month=>$months[$select_month]);
       }
       else
       {
         $newmonths=$months;

       }
 ?>      
<table class="table table-striped table-hover table-bordered">
                                       <thead>
                                           <tr>
                                                <?php 
                                                foreach ($newmonths as $key=> $name)
                                                {
                                                ?>
                                                <th><?php echo $name; ?></th>
                                        <?php } ?>
                                                
                                   </tr>
                                       </thead>
                                       <tbody id="main_body">
                                           <tr>
                                              <?php 
                                                foreach ($newmonths as $key=> $name)
                                                {
                                                 $number = cal_days_in_month(CAL_GREGORIAN, $key, $select_year); // 31
                                               
    
                                                ?> 
                                               <td>
                                                   <table class="table table-bordered">
                                                       <tr>
                                                       <tr>
                                                           <td>Date</td>
                                                           <td>Day</td>
                                                           <td>
                                                               <div class="checker">
                                                                   <span>
                                                                    <input type="checkbox" class="bilk_column" name="<?php echo $select_year.'_'.$key; ?>_all" id="<?php echo $select_year.'_'.$key; ?>">
                                                                   </span>
                                                               </div>
                                                           </td>
                                                       </tr>
                                                       
                                                           <?php 
                                                           for($i=1;$i<=$number;$i++)
                                                           {
                                                           ?>
                                                           <tr>
                                                           <td><?php echo $i; ?></td>
                                                           <td><?php echo date('D',strtotime($select_year.'-'.$key.'-'.$i)); ?></td>
                                                           <td>
                                                               <div class="checker">
                                                                   <span>
                                                                      <input type="checkbox" class="<?php echo $select_year.'_'.$key.'chk'; ?>" name="<?php $select_year.'-'.$key.'-'.$i; ?>">

                                                                   </span>    
                                                               </div>
                                                               </td>
                                                          </tr>
                                                          <?php }?>
                                                     
                                                       
                                                   </table>
                                                   
                                               </td>
                                               <?php }?>
                                               
                                               
                                           </tr>
                                           
                                           
                                           
                                       </tbody>
                                   
                               </table>
   <?php }?>


