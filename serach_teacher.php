<?php
ob_start();
?>
<link  href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link  href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
<?php
   include_once("./includes/session.php");
   include_once("./includes/config.php");
   $url = basename(__FILE__) . "?" . (isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : 'cc=cc');
    if(isset($_REQUEST['action']) && $_REQUEST['action']=='delete')
   {
   	$item_id=$_REQUEST['cid'];
   	
   	$deleteQry = "DELETE FROM `school_schooldays` WHERE `id` = '" . mysql_real_escape_string($item_id) . "'";
   	mysql_query($deleteQry);	
   	header('Location:view_schoolday.php');
   }
   if(isset($_REQUEST['action']) && $_REQUEST['action']=='delete_all')
   {
   	$item_id=implode(",",$_REQUEST['ids']);
   	
   	 $deleteQry = "DELETE FROM `school_schooldays` WHERE `id` IN ( '" . mysql_real_escape_string($item_id) . "')";
   	mysql_query($deleteQry);	
   	header('Location:view_schoolday.php');
   }
   ?>
<script language="javascript">
   function del(aa, bb)
   
   {
   
       var a = confirm("Are you sure, you want to delete this?")
   
       if (a)
   
       {
   
           location.href = "view_schoolday.php?cid=" + aa + "&action=delete&parentid="+bb;
   
       }
   
   }
   
   
   
   
   
   
   
</script>
<?php include("includes/header.php"); ?>
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
   <!-- BEGIN SIDEBAR -->
   <?php include("includes/left_panel.php"); ?>
   <!-- END SIDEBAR -->
   <!-- BEGIN CONTENT -->
   <div class="page-content-wrapper">
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- /.modal -->
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN STYLE CUSTOMIZER -->
         <!-- END STYLE CUSTOMIZER -->
         <!-- BEGIN PAGE HEADER-->
         <h3 class="page-title"> Generate Teacher Id Card </h3>
         <div class="page-bar">
            <ul class="page-breadcrumb">
               <li> <i class="fa fa-home"></i> <a href="index.php">Home</a> <i class="fa fa-angle-right"></i> </li>
               <li> <a href="#">Generate  Teacher Id Card</a> <i class="fa fa-angle-right"></i> </li>
               <!--<li>
                  <a href="#">Editable Datatables</a>
                  
                  </li>-->
            </ul>
            
         </div>
         <!-- END PAGE HEADER-->
         <!-- BEGIN PAGE CONTENT-->
         <div class="row">
            <div class="col-md-12">
               <div class="portlet box blue">
                  <div class="portlet-title">
                     <div class="caption">
                        <i class="fa fa-gift"></i>Generate Teacher Id Card
                     </div>
                     <div class="tools">
                     </div>
                  </div>
                  <div class="portlet-body form">
                     <!-- BEGIN FORM-->
                     <form  class="form-horizontal" method="post" action="print_teacheridcard.php" enctype="multipart/form-data" id="student_filter">
                        <div class="form-body">
                           <div class="form-group">
                              <label class="col-md-3 control-label">Template</label>
                              <div class="col-md-5">
                                <select class="form-control"  name="template"  id="template" >
                                                <option value=""> Select Template</option>
                                                <?php
                                                $sql=mysql_query("select * from school_template where 1 order by temporder");
                                                while($shift=mysql_fetch_assoc($sql))
                                                {
                                                ?>
                                                <option value="<?php echo $shift['id'] ?>"><?php echo $shift['name'] ?></option>
                                                <?php } ?>
                                                
                                                ?>

                                            </select>
                              </div>
                           </div> 
                           <div class="form-group">
                              <label class="col-md-3 control-label">Signature</label>
                              <div class="col-md-5">
                                  <select class="form-control"  name="signature"  id="signature" required="">
                                                <option value=""> Select Signature</option>
                                                <?php
                                                $sql=mysql_query("select * from school_signature where status=1");
                                                while($shift=mysql_fetch_assoc($sql))
                                                {
                                                ?>
                                                <option value="<?php echo $shift['id'] ?>"><?php echo $shift['name'] ?></option>
                                                <?php } ?>
                                                
                                                ?>

                                            </select>
                              </div>
                           </div>  
                           <div class="form-group">
                              <label class="col-md-3 control-label">Shift</label>
                              <div class="col-md-5">
                                <select class="form-control"  name="shift"  id="shift" >
                                                <option value=""> Select Shift</option>
                                                <?php
                                                $sql=mysql_query("select * from shiftname where 1");
                                                while($shift=mysql_fetch_assoc($sql))
                                                {
                                                ?>
                                                <option value="<?php echo $shift['id'] ?>"><?php echo $shift['shiftname'] ?></option>
                                                <?php } ?>
                                                
                                                ?>

                                            </select>
                              </div>
                           </div>
                            
                            <div class="form-group">
                              <label class="col-md-3 control-label">Teacher ID</label>
                              <div class="col-md-5">
                                  <input type="text" name="teacher_id" class="form-control" id="teacher_id">
                              </div>
                           </div>
                        

                                           <div class="form-group">
                                        <label class="col-md-3 control-label">Validiy From  </label>
                                        <div class="col-md-5">

                                           

                                            <input type="text" class="form-control date-picker" required="" name="Validityfromdate" id="Validityfromdate" value="<?php echo $_REQUEST['Validityfromdate']; ?>" />
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Validiy To</label>
                                        <div class="col-md-5">

                                           

                                            <input type="text" class="form-control date-picker" required="" name="Validitytodate" id="Validitytodate" value="<?php echo $_REQUEST['Validitytodate']; ?>" />
                                        </div>

                                    </div> 
                        </div>
                        <div class="form-actions fluid">
                           <div class="row">
                              <div class="col-md-offset-3 col-md-9">
                                 <button type="submit" class="btn blue"  name="submit1" id="submit_btn">Submit</button>
                              </div>
                           </div>
                        </div>
                     </form>
                     <!-- END FORM-->
                  </div>
               </div>
            </div>
         </div>
        
         <!-- END PAGE CONTENT -->
      </div>
   </div>
   <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<div class="page-footer">
   <?php include("includes/footer.php"); ?>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<!--<script src="assets/admin/pages/scripts/table-editable.js"></script>-->
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   jQuery(document).ready(function () {
   
       Metronic.init(); // init metronic core components
   
       Layout.init(); // init current layout
   
       QuickSidebar.init(); // init quick sidebar
   
       Demo.init(); // init demo features
   
      // TableEditable.init();
   
   });
   
   $('.delete_all').on('click', function(e) { 
   var allVals = [];  
   var allValspar = [];  
   $(".sub_chk:checked").each(function() {  
   allVals.push($(this).attr('data-id'));
   allValspar.push($(this).attr('data-parent'));
   });  
   //alert(allVals.length); return false;  
   if(allVals.length <=0)  
   {  
   alert("Please select row."); 
   return false; 
   } 
   
   location.href = "view_schoolday.php?cid=" + allVals + "&action=deletemulti&parentid="+allValspar;
   
   }); 
   
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script type="text/javascript">
   function deleteConfirm() {
   
       var result = confirm("Are you sure to delete product?");
   
       if (result) {
   
           return true;
   
       } else {
   
           return false;
   
       }
   
   }
   
   
   
   $(document).ready(function () {

                $( ".date-picker" ).datepicker({
                dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
                yearRange: "-120:+0"
    });
   
   
   
       var table = $('#sample_editable_1').DataTable( {
   
   
       dom: 'Bfrtip',
       pageLength:14,
       orderCellsTop:true,
   
      buttons: [
           
               {
                   extend: 'excelHtml5',
                    messageTop: null,
                     filename: 'schoolday',
                   exportOptions: {
                       columns: [1,2,3,4]
                   }
               },
//           {
//               extend: 'pdfHtml5',
//               messageTop: null,
//                filename: 'schoolday',
//               exportOptions: {
//                   columns: [1,2,3,4]
//               }
//           },
       ],
       lengthMenu: [
   
               [5, 15, 20, -1],
   
               [5, 15, 20, "All"] // change per page values here
   
           ],
   
          
   } );
   
          $('.dataTables_filter').find('input').addClass('form-control input-small input-inline');
          $('#sample_editable_1 tfoot th').each( function () {
          var title = $(this).text();
         $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
   });
   
   
            table.columns().every( function () {
       var that = this;
   
       $( 'input', this.footer() ).on( 'keyup change', function () {
           if ( that.search() !== this.value ) {
               that
                   .search( this.value )
                   .draw();
           }
       } );
   } );
   
   
   
   
          $('.dataTables_filter').find('input').addClass('form-control input-small input-inline');
   
          $(".download_excel").click(function(){
          $("#student_filter").attr("action","student_excel.php");  
          $("#submit_btn").trigger("click");
          });
       $('.selectradio').click(function () {
           if ($(this).val() == 'eiin') {
               $('#show_eiin').show();
           } else {
   
               $('#show_eiin').hide();
   
           }
   
   
   
   
   
       });
   
   
       $('#select_all').on('click', function () {
   
           if (this.checked) {
   
               $('.checkbox').each(function () {
   
                   this.checked = true;
   
               });
   
           } else {
   
               $('.checkbox').each(function () {
   
                   this.checked = false;
   
               });
   
           }
   
       });
   
   
   
       $('.checkbox').on('click', function () {
   
           if ($('.checkbox:checked').length == $('.checkbox').length)
   
           {
   
               $('#select_all').prop('checked', true);
   
           } else {
   
               $('#select_all').prop('checked', false);
   
           }
   
       });
       $(".check_all").click(function(){
       chkbxid=$(this).attr("id");
       if(this.checked)
       {
           $("."+chkbxid+"chk").prop("checked",true);
           $("."+chkbxid+"chk").parent().addClass("checked");
       }
       else
       {
          $("."+chkbxid+"chk").prop("checked",false);
          $("."+chkbxid+"chk").parent().removeClass("checked");
   
       }
       
       });
       $(".datecheckbx").click(function(){
        string=$(this).attr("class");
        $current_class=string.replace('datecheckbx', '').trim(); 
        $total_checked=parseInt($('input.'+$current_class+':checked').length)
        $totalbox=parseInt($('input.'+$current_class+'').length);
        if(this.checked)
        {
            if($totalbox==$total_checked)
            {
                $all_chkbx_id=$current_class.replace("chk",'').trim();
                $("#"+$all_chkbx_id).prop("checked",true);
                $("#"+$all_chkbx_id).parent().addClass("checked");
            }
        }
        else
        {
            $all_chkbx_id=$current_class.replace("chk",'').trim();
            $("#"+$all_chkbx_id).prop("checked",false);
            $("#"+$all_chkbx_id).parent().removeClass("checked");
        }
       });
   
   });
   
   
   
   
   
   
   
</script>
<form id="new_form" method="post"></form>
<script>
   $(document).ready(function () {
   
       $(".san_open").parent().parent().addClass("active open");
       $(".dt-buttons").append("<a class='dt-button'  href='javascript:void(0)' id='pdf_btn'><span>PDF</span></a>");
       $(".dt-buttons").append('<a class="dt-button"  onclick=deleteConfirmAll()><span>Delete All</span></a>');
       $("#student_filter").submit(function(event){
       event.preventDefault()    ;
       shift=$("#shift").val();
       teacher_id=$("#teacher_id").val();
       template=$("#template").val();
       signature=$("#signature").val();
       Validitytodate=$("#Validitytodate").val();

       Validityfromdate=$("#Validityfromdate").val();
       $params={shift:shift,teacher_id:teacher_id,action:'searchteacher'};
       
       $.post("ajax.php",$params,function(result){
       if(result.trim()>0)
       {
        $("#new_form").append("<input type='hidden'name='shift' value="+shift+">");  
        $("#new_form").append("<input type='hidden'name='teacher_id' value="+teacher_id+">");  
        $("#new_form").append("<input type='hidden'name='template' value="+template+">");  
        $("#new_form").append("<input type='hidden'name='signature' value="+signature+">"); 
        $("#new_form").append("<input type='hidden'name='Validitytodate' value="+Validitytodate+">"); 


         $("#new_form").append("<input type='hidden'name='Validityfromdate' value="+Validityfromdate+">"); 
        $("#new_form").attr("action","print_teacheridcard.php");
        $("#new_form").submit();

       }else
       {
           alert("No Teacher Found");
       }
       });
       })
       $("#pdf_btn").click(function(){
       download_pdf('print_schoolday.php');
       
       });
       
    $('body').on('focus',".newdatepicker", function(){
                             $(this).datepicker({
                                dateFormat:"dd-mm-yy",
                                changeYear:true,
                                changeMonth:true
                             });
                             
                         });
   });
   
</script>
<style type="text/css">
   tfoot {
   display: table-header-group;
   }
   tfoot input {
   width: 100%;
   padding: 6px;
   box-sizing: border-box;
   font-size: 12px;
   }
</style>
</body><!-- END BODY -->
</html>

