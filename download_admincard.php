<?php
ob_start();
session_start();
include_once("./includes/config.php");

require('fpdf181/fpdf.php');

function hex2dec($couleur = "#000000"){
    $R = substr($couleur, 1, 2);
    $rouge = hexdec($R);
    $V = substr($couleur, 3, 2);
    $vert = hexdec($V);
    $B = substr($couleur, 5, 2);
    $bleu = hexdec($B);
    $tbl_couleur = array();
    $tbl_couleur['R']=$rouge;
    $tbl_couleur['V']=$vert;
    $tbl_couleur['B']=$bleu;
    return $tbl_couleur;
}

//conversion pixel -> millimeter at 72 dpi
function px2mm($px){
    return $px*25.4/72;
}

function txtentities($html){
    $trans = get_html_translation_table(HTML_ENTITIES);
    $trans = array_flip($trans);
    return strtr($html, $trans);
}
////////////////////////////////////

class PDF_HTML extends FPDF
{
//variables of html parser
protected $B;
protected $I;
protected $U;
protected $HREF;
protected $fontList;
protected $issetfont;
protected $issetcolor;

function __construct($orientation='P', $unit='mm', $format='A4')
{
    //Call parent constructor
    parent::__construct($orientation,$unit,$format);
    //Initialization
    $this->B=0;
    $this->I=0;
    $this->U=0;
    $this->HREF='';
    $this->fontlist=array('arial', 'times', 'courier', 'helvetica', 'symbol');
    $this->issetfont=false;
    $this->issetcolor=false;
}

function WriteHTML($html)
{
    //HTML parser
    $html=strip_tags($html,"<b><u><i><a><img><p><br><strong><em><font><tr><blockquote>"); //supprime tous les tags sauf ceux reconnus
    $html=str_replace("\n",' ',$html); //remplace retour à la ligne par un espace
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE); //éclate la chaîne avec les balises
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(5,stripslashes(txtentities($e)));
        }
        else
        {
            //Tag
            if($e[0]=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag, $attr)
{
    //Opening tag
    switch($tag){
        case 'STRONG':
            $this->SetStyle('B',true);
            break;
        case 'EM':
            $this->SetStyle('I',true);
            break;
        case 'B':
        case 'I':
        case 'U':
            $this->SetStyle($tag,true);
            break;
        case 'A':
            $this->HREF=$attr['HREF'];
            break;
        case 'IMG':
            if(isset($attr['SRC']) && (isset($attr['WIDTH']) || isset($attr['HEIGHT']))) {
                if(!isset($attr['WIDTH']))
                    $attr['WIDTH'] = 0;
                if(!isset($attr['HEIGHT']))
                    $attr['HEIGHT'] = 0;
                $this->Image($attr['SRC'], $this->GetX(), $this->GetY(), px2mm($attr['WIDTH']), px2mm($attr['HEIGHT']));
            }
            break;
        case 'TR':
        case 'BLOCKQUOTE':
        case 'BR':
            $this->Ln(5);
            break;
        case 'P':
            $this->Ln(10);
            break;
        case 'FONT':
            if (isset($attr['COLOR']) && $attr['COLOR']!='') {
                $coul=hex2dec($attr['COLOR']);
                $this->SetTextColor($coul['R'],$coul['V'],$coul['B']);
                $this->issetcolor=true;
            }
            if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist)) {
                $this->SetFont(strtolower($attr['FACE']));
                $this->issetfont=true;
            }
            break;
    }
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='STRONG')
        $tag='B';
    if($tag=='EM')
        $tag='I';
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
    if($tag=='FONT'){
        if ($this->issetcolor==true) {
            $this->SetTextColor(0);
        }
        if ($this->issetfont) {
            $this->SetFont('arial');
            $this->issetfont=false;
        }
    }
}

function SetStyle($tag, $enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
    {
        if($this->$s>0)
            $style.=$s;
    }
    $this->SetFont('',$style);
}

function PutLink($URL, $txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(5,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}

}//end of class
if(isset($_POST['submit']))
{
	$un=stripslashes(trim($_REQUEST['un']));
	$pass=$_REQUEST['pass'];
	 $sql="SELECT * FROM `school_studentadmission` WHERE `username`='".mysql_real_escape_string($_REQUEST['un'])."'  and `password`='".mysql_real_escape_string($pass)."'";
//	exit;
	$rs=mysql_query($sql) or die(mysql_error());
	if($row=mysql_fetch_array($rs))
	{

		$fetch_class=mysql_query("select * from `classname`  where id='".$row['class_id']."'");	
		$class=mysql_fetch_array($fetch_class);


$sqlpro=mysql_query("SELECT * FROM `school_adminprofile` where userid=1"); 
$rowpro=mysql_fetch_array($sqlpro);

$sqladmin=mysql_query("SELECT * FROM `school_admin` where id=1"); 
$rowadmin=mysql_fetch_array($sqladmin);
 if($rowadmin['image']==''){
   $image_link='http://mhcds.dartmouth.edu/images/made/uploads/mhcds/images/img_profile_goldberg_301_303_s_c1.jpg';
 }else{
    $image_link=SITE_URL.'upload/documents/'.$rowadmin['image'];
 }

$html='<div class="full_width" style="width:100%;float: left;padding:5px;border: 3px #002060 dotted;overflow: hidden;position: relative;margin: 0 auto;">
        <div class="logo_part" style="width: 98%;float: left;padding: 10px; border: 2px solid #000;display: flex;align-items:center;margin: 0 auto;">
            <div class="logo" style="width: 20%;float:left;">
                <img src="'.$image_link.'" >
            </div>
            <div class="name_logo" style="width:70%;float: left; text-align: center;">
                <h3 style="color: #da026e;font-size:49px;font-weight: 700; margin: 0;">'.$rowpro['institution'].'</h3>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div class="admit" style="width: 70%; margin:20px auto; text-align: center;color:#000099;float: left;font-size: 28px;font-weight: 700;">Admit Card</div>
        <div class="logo_part" style="width: 98%;float: left;padding: 15px; border: 1px solid #000;margin: 0 auto;">
            <div class="information" style="width: 68%;float: left; padding: 5px;">
            
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">Roll </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">: '.sprintf("%03d", $row['roll']).' </h4>
                    </div>
                </div>
                
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width:35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">Class </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">: '.$class['classname'].' </h4>
                    </div>
                </div>
                
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">Name </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">: '.$row['name'].' </h4>
                    </div>
                </div>
                
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">Fathers Name </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">: '.$row['fathername'].' </h4>
                    </div>
                </div>
                
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">Mothers Name </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">: '.$row['mothername'].' </h4>
                    </div>
                </div>
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">Date of Birth </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;">: '.date('d/m/Y',strtotime($row['dob'])).'</h4>
                    </div>
                </div>
                
                
            </div>
            <div class="pics" style="width: 20%;float: right;padding: 5px;">
                <div class="ad_pic" style="padding: 5px;text-align: center;">
                    <img src="./upload/documents/'.$row['image'].'" style="border: 1px solid #000; padding: 5px; margin: 0 auto; width: 225px;height: 250px">
                </div>
                <div class="ad_pic" style="padding: 5px;text-align: center;margin: 15px auto;">
                    <img src="./upload/documents/'.$rowpro['signaturehead'].'" style="margin: 0 auto; width: 264px;height: 90px">
                </div>
                <div class="ad_pic" style="padding: 5px;text-align: center;margin: 0 auto;">
                    <h4 style="color: #000;font-size:16px;font-weight: 600;margin: 2px 0;">Signature Head of Institute</h4>
                 
                </div>
                
            </div>
        </div>

        <div class="admit" style="background:#d9d9d9; width: 100%; margin:20px auto; text-align: center;color:#921600;float: left;font-size: 35px;font-weight: 700;">General Instruction of Applicants</div>
        
        <div class="logo_part" style="width: 98%;float: left;padding: 15px; border: 1px solid #000;margin: 0 auto;">
            <div class="information" style="width: 100%;float: left; padding: 5px;">
            
                <div class="infos" style="width:70%;float: left;">
                    <div class="fields" style="width: 100%;float:left">
                        <h4 style="color: #000;font-size:15px;font-weight: 600; margin: 10px 0;">'.$rowpro['instructionpdf'].' </h4>
                    </div>
                </div>
                
                
                
                
                
                
              
                </div>
                
                
            </div>
            
        </div>';

    include("mpdf60/mpdf.php");

$mpdf=new mPDF('utf-8', 'A4-P'); 

$mpdf->WriteHTML($html);

$mpdf->Output('admitcard.pdf','D');
exit;	

	}
	else
	{
		$msg="Invalid Username or Password.";
        $_SESSION['msg']=$msg;
	}
}

?><!DOCTYPE html>



<!-- 



Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2



Version: 3.7.0



Author: KeenThemes



Website: http://www.keenthemes.com/



Contact: support@keenthemes.com



Follow: www.twitter.com/keenthemes



Like: www.facebook.com/keenthemes



Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes



License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.



-->



<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->



<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->



<!--[if !IE]><!-->



<html lang="en">



<!--<![endif]-->



<!-- BEGIN HEAD -->



<head>



<meta charset="utf-8"/>



<title>Admin Panel</title>



<meta http-equiv="X-UA-Compatible" content="IE=edge">



<meta content="width=device-width, initial-scale=1.0" name="viewport"/>



<meta http-equiv="Content-type" content="text/html; charset=utf-8">



<meta content="" name="description"/>



<meta content="" name="author"/>



<!-- BEGIN GLOBAL MANDATORY STYLES -->



<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>



<link href="<?php echo BASE_URL;?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>



<link href="<?php echo BASE_URL;?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>



<link href="<?php echo BASE_URL;?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>



<link href="<?php echo BASE_URL;?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>



<!-- END GLOBAL MANDATORY STYLES -->



<!-- BEGIN PAGE LEVEL STYLES -->



<link href="<?php echo BASE_URL;?>assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>



<link href="<?php echo BASE_URL;?>assets/admin/pages/css/login-soft.css" rel="stylesheet" type="text/css"/>



<!-- END PAGE LEVEL SCRIPTS -->



<!-- BEGIN THEME STYLES -->



<link href="<?php echo BASE_URL;?>assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>



<link href="<?php echo BASE_URL;?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>



<link href="<?php echo BASE_URL;?>assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>



<link id="style_color" href="<?php echo BASE_URL;?>assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>



<link href="<?php echo BASE_URL;?>assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>



<!-- END THEME STYLES -->



<link rel="shortcut icon" href="favicon.ico"/>
<style>


.login .content{height: 275px;}
</style>



</head>



<!-- END HEAD -->



<!-- BEGIN BODY -->



<body class="login">



<!-- BEGIN LOGO -->



<div class="logo">



	<a href="index.php">



	<img src="<?php echo BASE_URL;?>logo.png" style="width:150px; height:100px;" alt=""/>



	</a>

<div style="margin-top:5px; color:#000000"> <strong>Conceptualised  & Designed by : Pother Thikana Design & Technologies</strong></div>

</div>



<!-- END LOGO -->



<!-- BEGIN SIDEBAR TOGGLER BUTTON -->



<div class="menu-toggler sidebar-toggler">



</div>



<!-- END SIDEBAR TOGGLER BUTTON -->



<!-- BEGIN LOGIN -->



<div class="content">



	<!-- BEGIN LOGIN FORM -->

<div style="text-align:center;"><?php echo  $_SESSION['msg']; echo  $_SESSION['msg']='';  ?></div>

	<form class="login-form" action="" method="post">



		<h3 class="form-title">Download Admin card </h3>



		<div class="alert alert-danger display-hide">



			<button class="close" data-close="alert"></button>



			<span>



			Enter any username and password. </span>



		</div>



		<div class="form-group">



			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->



			<label class="control-label visible-ie8 visible-ie9">Username</label>



			<div class="input-icon">



				<i class="fa fa-user"></i>



				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="un"/>



			</div>



		</div>



		<div class="form-group">



			<label class="control-label visible-ie8 visible-ie9">Password</label>



			<div class="input-icon">



				<i class="fa fa-lock"></i>



				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="pass"/>



			</div>



		</div>



		<div class="form-actions">
			<p><button type="submit" class="btn blue pull-right" name="submit" style="width:100%; padding:5px; margin:5px;">
			Download Admin card <i class="m-icon-swapright m-icon-white"></i>
			</button></p>
         	<p><button type="button" onClick="location.href='index.php'" class="btn blue pull-right" style="width:100%; padding:5px; margin:5px;">Back To Home  <i class="m-icon-swapright m-icon-white"></i></button></p>
		</div>
		
		<!--<div class="login-options">



			<h4>Or login with</h4>



			<ul class="social-icons">



				<li>



					<a class="facebook" data-original-title="facebook" href="javascript:;">



					</a>



				</li>



				<li>



					<a class="twitter" data-original-title="Twitter" href="javascript:;">



					</a>



				</li>



				<li>



					<a class="googleplus" data-original-title="Goole Plus" href="javascript:;">



					</a>



				</li>



				<li>



					<a class="linkedin" data-original-title="Linkedin" href="javascript:;">



					</a>



				</li>



			</ul>



		</div>-->



		






	</form>



	<!-- END LOGIN FORM -->



	<!-- BEGIN FORGOT PASSWORD FORM -->



	<!--<form class="forget-form" action="index.html" method="post">



		<h3>Forget Password ?</h3>



		<p>



			 Enter your e-mail address below to reset your password.



		</p>



		<div class="form-group">



			<div class="input-icon">



				<i class="fa fa-envelope"></i>



				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>



			</div>



		</div>



		<div class="form-actions">



			<button type="button" id="back-btn" class="btn">



			<i class="m-icon-swapleft"></i> Back </button>



			<button type="submit" class="btn blue pull-right">



			Submit <i class="m-icon-swapright m-icon-white"></i>



			</button>



		</div>



	</form>-->



	<!-- END FORGOT PASSWORD FORM -->



	<!-- BEGIN REGISTRATION FORM -->



	



	<!-- END REGISTRATION FORM -->



</div>



<!-- END LOGIN -->



<!-- BEGIN COPYRIGHT -->



<div class="copyright">



	 <?php echo date('Y');?> &copy; Developed By <a href="http://www.stivl.com/" target="_blank">STIVL BD</a>



</div>



<!-- END COPYRIGHT -->



<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->



<!-- BEGIN CORE PLUGINS -->



<!--[if lt IE 9]>



<script src="<?php echo BASE_URL;?>assets/global/plugins/respond.min.js"></script>



<script src="<?php echo BASE_URL;?>assets/global/plugins/excanvas.min.js"></script> 



<![endif]-->



<script src="<?php echo BASE_URL;?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>



<script src="<?php echo BASE_URL;?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>



<script src="<?php echo BASE_URL;?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>



<script src="<?php echo BASE_URL;?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>



<script src="<?php echo BASE_URL;?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>



<script src="<?php echo BASE_URL;?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>



<!-- END CORE PLUGINS -->



<!-- BEGIN PAGE LEVEL PLUGINS -->



<script src="<?php echo BASE_URL;?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>



<script src="<?php echo BASE_URL;?>assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>



<script type="text/javascript" src="<?php echo BASE_URL;?>assets/global/plugins/select2/select2.min.js"></script>



<!-- END PAGE LEVEL PLUGINS -->



<!-- BEGIN PAGE LEVEL SCRIPTS -->



<script src="<?php echo BASE_URL;?>assets/global/scripts/metronic.js" type="text/javascript"></script>



<script src="<?php echo BASE_URL;?>assets/admin/layout/scripts/layout.js" type="text/javascript"></script>



<script src="<?php echo BASE_URL;?>assets/admin/layout/scripts/demo.js" type="text/javascript"></script>



<script src="<?php echo BASE_URL;?>assets/admin/pages/scripts/login-soft.js" type="text/javascript"></script>



<!-- END PAGE LEVEL SCRIPTS -->



<script>



jQuery(document).ready(function() {     



  Metronic.init(); // init metronic core components



Layout.init(); // init current layout



  Login.init();



  Demo.init();



       // init background slide images



       $.backstretch([



        "<?php echo BASE_URL;?>assets/admin/pages/media/bg/1.jpg",



        "<?php echo BASE_URL;?>assets/admin/pages/media/bg/2.jpg",



        "<?php echo BASE_URL;?>assets/admin/pages/media/bg/3.jpg",



        "<?php echo BASE_URL;?>assets/admin/pages/media/bg/4.jpg"



        ], {



          fade: 1000,



          duration: 8000



    }



    );



});



</script>



<!-- END JAVASCRIPTS -->



</body>



<!-- END BODY -->



</html>