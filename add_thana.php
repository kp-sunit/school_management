<?php 
include_once("./includes/session.php");
//include_once("includes/config.php");
include_once("./includes/config.php");
$url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
?>
<?php

if(isset($_REQUEST['submit']))
{

	$name = isset($_POST['name']) ? $_POST['name'] : '';
	$division_id = isset($_POST['division_id']) ? $_POST['division_id'] : '';
	$district_id = isset($_POST['district_id']) ? $_POST['district_id'] : '';
	$upozela_id = isset($_POST['upozela_id']) ? $_POST['upozela_id'] : '';
	
	$fields = array(
		'name' => mysql_real_escape_string($name),
		'division_id' => mysql_real_escape_string($division_id),
		'district_id'=> mysql_real_escape_string($district_id),
		'upozela_id' => mysql_real_escape_string($upozela_id),
		);

		$fieldsList = array();
		foreach ($fields as $field => $value) {
			$fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
		}
					 
	 if($_REQUEST['action']=='edit')
	  {		  
	 $editQuery = "UPDATE `school_thana` SET " . implode(', ', $fieldsList)
			. " WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'";
			
		//	exit;

		if (mysql_query($editQuery)) {
			$_SESSION['msg'] = "Category Updated Successfully";
		}
		else {
			$_SESSION['msg'] = "Error occuried while updating Category";
		}
			
			
			
		header('Location:list_thana.php');
		exit();
	
	 }
	 else
	 {
	 
	 $addQuery = "INSERT INTO `school_thana` (`" . implode('`,`', array_keys($fields)) . "`)"
			. " VALUES ('" . implode("','", array_values($fields)) . "')";
			
			
		mysql_query($addQuery);
		$last_id=mysql_insert_id();
		
		

		header('Location:list_thana.php');
		exit();
	
	 }
				
				
}


if($_REQUEST['action']=='edit')
{
$categoryRowset = mysql_fetch_array(mysql_query("SELECT * FROM `school_thana` WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'"));

}

//Division list

$sql_division = mysql_query("SELECT * FROM `school_divisions` WHERE id <> ''");

$sql_districts = mysql_query("SELECT * FROM `school_districts` WHERE id <> ''");
?>

<?php include('includes/header.php');?>
<!-- END HEADER -->


<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include('includes/left_panel.php');?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN STYLE CUSTOMIZER -->
			<?php //include('includes/style_customize.php');?>
			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">Thana </h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Thana </a>
						
					</li>
					
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php echo $_REQUEST['action']=='edit'?"Edit":"Add";?> Thana
										</div>
										<div class="tools">
											
											
											
										</div>
									</div>
										<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<form  class="form-horizontal" method="post" action="add_thana.php" enctype="multipart/form-data">
										<input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />

										<input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>" />


										<div class="form-body">
											


										<div class="form-group">
										<label class="col-md-3 control-label">Division</label>
										<div class="col-md-4">
										<select class="form-control" name="division_id" onchange="select_sub(this.value)" id="division_id" required>
										<option value="">Select Division</option>
										<?php 
										if(count($sql_division) > 0)
										{
										while($row_division = mysql_fetch_array($sql_division))
										{
										?>
										<option value="<?php echo $row_division['id'];?>" <?php if($categoryRowset['division_id'] == $row_division['id']){ ?>selected<?php }?>><?php echo $row_division['name'];?></option>	
										<?php	
										}
										}
										?>
										</select>
										</div>
										</div>	

										<div class="form-group">
										<label class="col-md-3 control-label">District</label>
										<div class="col-md-4">
										<select class="form-control" id="disctrict_list" name="district_id" onfocus="select_subsub(this.value)" required>
										<option value="">Select District</option>
										<?php 
											if(isset($categoryRowset['district_id']))
											{
											$SQL ="SELECT * FROM `school_districts` where id = '".$categoryRowset['district_id']."'";
											$result = mysql_query($SQL);

											while($row1=mysql_fetch_array($result))
											{ 
											?>
											<option value="<?php echo $row1['id']; ?>" <?php if($categoryRowset['district_id']==$row1['id']) { echo "selected";}?> > <?php echo $row1['name']; ?></option>
											<?php
											}
											}
											?>
										
										</select>
										</div>
										</div>	
										
										<div class="form-group">
										<label class="col-md-3 control-label">Upozela</label>
										<div class="col-md-4">
										<select class="form-control" id="upozela_list" name="upozela_id">
										<option value="">Select Upozela</option>
										<?php 
										if(isset($categoryRowset['upozela_id']))
										{
										$sql_upozela ="SELECT * FROM `school_upozela` where id = '".$categoryRowset['upozela_id']."'";
										$result_upozela = mysql_query($sql_upozela);

										while($row1_upozela=mysql_fetch_array($result_upozela))
										{ 
										?>
										<option value="<?php echo $row1_upozela['id']; ?>" <?php if($categoryRowset['upozela_id']==$row1_upozela['id']) { echo "selected";}?> > <?php echo $row1_upozela['name']; ?></option>
										<?php
										}
										}
										?>
										
										</select>
										</div>
										</div>	
										
										
										<div class="form-group">
										<label class="col-md-3 control-label">Name</label>
										<div class="col-md-4">
										<input type="text" class="form-control" placeholder="Enter text"  value="<?php echo $categoryRowset['name'];?>" name="name" required>

										</div>
										</div>

										</div>

										<div class="form-actions fluid">
										<div class="row">
										<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn blue"  name="submit">Submit</button>

										</div>
										</div>
										</div>
										</form>
										<!-- END FORM-->
										</div>
								</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	
	
	
<style>
.thumb{
    height: 60px;
    width: 60px;
    padding-left: 5px;
    padding-bottom: 5px;
}

</style>

<script>

     
window.preview_this_image = function (input) {

    if (input.files && input.files[0]) {
        $(input.files).each(function () {
            var reader = new FileReader();
            reader.readAsDataURL(this);
            reader.onload = function (e) {
                $("#previewImg").append("<span><img class='thumb' src='" + e.target.result + "'><img border='0' src='../images/erase.png'  border='0' class='del_this' style='z-index:999;margin-top:-34px;'></span>");
            }
        });
    }
}
</script>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	<?php //include('includes/quick_sidebar.php');?>
	<!-- END QUICK SIDEBAR -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include('includes/footer.php'); ?>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/form-samples.js"></script>
<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>

<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init(); // init quick sidebar
Demo.init(); // init demo features
   FormSamples.init();
   
    if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true,
                language: "xx"
            });
        }
   
});


    
</script>
<script type="text/javascript">
            function select_sub(id) {
                $.ajax({
                    type: "post",
                    url: "ajax_district.php?action=cat",
                    data: {id: id},
                    success: function (msg) {
                        $('#disctrict_list').html(msg);
                    }
                });
            }
			
			
			function select_subsub(id) {
                $.ajax({
                    type: "post",
                    url: "ajax_upozela.php?action=sub_cat",
                    data: {id: id},
                    success: function (msg) {
                        $('#upozela_list').html(msg);
                    }
                });
            }
			
			
			
        </script>
<script>

$(document).ready(function(){
    $(".san_open").parent().parent().addClass("active open");
});
document.getElementById("division_id").focus();
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
