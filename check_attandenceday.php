

<?php
   ob_start();
   ?>
<link  href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link  href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
<?php
   include_once("./includes/session.php");
   
   //include_once("includes/config.php");
   
   include_once("./includes/config.php");
   
   $url = basename(__FILE__) . "?" . (isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : 'cc=cc');
   
   
   if(isset($_REQUEST['adddata']))
   {
     header("Location:view_schoolday.php");
   }
   ?>
<script language="javascript">
   function del(aa, bb)
   
   {
   
       var a = confirm("Are you sure, you want to delete this?")
   
       if (a)
   
       {
   
           location.href = "view_student.php?cid=" + aa + "&action=delete&parentid="+bb;
   
       }
   
   }
   
   
   
   function inactive(aa)
   {
       location.href = "list_teacher.php?cid=" + aa + "&action=inactive"
   }
   
   function active(aa)
   {
       location.href = "list_teacher.php?cid=" + aa + "&action=active";
   }
   
   
   
</script>
<?php include("includes/header.php"); ?>
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
   <!-- BEGIN SIDEBAR -->
   <?php include("includes/left_panel.php"); ?>
   <!-- END SIDEBAR -->
   <!-- BEGIN CONTENT -->
   <div class="page-content-wrapper">
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- /.modal -->
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN STYLE CUSTOMIZER -->
         <!-- END STYLE CUSTOMIZER -->
         <!-- BEGIN PAGE HEADER-->
         <h3 class="page-title">  School Days  </h3>
         <div class="page-bar">
            <ul class="page-breadcrumb">
               <li> <i class="fa fa-home"></i> <a href="index.php">Home</a> <i class="fa fa-angle-right"></i> </li>
               <li> <a href="#"> School Days </a> <i class="fa fa-angle-right"></i> </li>
               <!--<li>
                  <a href="#">Editable Datatables</a>
                  
                  </li>-->
            </ul>
         </div>
         <!-- END PAGE HEADER-->
         <!-- BEGIN PAGE CONTENT-->
         <div class="row">
            <div class="col-md-12">
               <div class="portlet box blue">
                  <div class="portlet-title">
                     <div class="caption">
                        <i class="fa fa-gift"></i>Search 
                     </div>
                     <div class="tools">
                     </div>
                  </div>
                  <div class="portlet-body form">
                     <!-- BEGIN FORM-->
                     <form  class="form-horizontal" method="post" action="" enctype="multipart/form-data" id="student_filter">
                        <div class="form-body">
                           <div class="form-group">
                              <label class="col-md-3 control-label">Class</label>
                              <div class="col-md-5">
                                 <select class="form-control"  name="class_id" required="" >
                                    <option value=""> select Class</option>
                                    <?php
                                       $fetch_class = mysql_query("select * from `classname`  where status=1 order by frontorder");
                                       
                                       $numclass = mysql_num_rows($fetch_class);
                                       
                                       if ($numclass > 0) {
                                       
                                           while ($class = mysql_fetch_array($fetch_class)) {
                                               ?>
                                    <option <?php
                                       if ($_REQUEST['class_id'] == $class['id']) {
                                           echo 'selected';
                                       }
                                       ?> value="<?php echo $class['id']; ?>"><?php echo $class['classname']; ?></option>
                                    <?php
                                       }
                                       }
                                       ?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-3 control-label">Section</label>
                              <div class="col-md-5">
                                 <select class="form-control"  name="section_id" required="" >
                                    <option value=""> select Section</option>
                                    <?php
                                       $fetch_section = mysql_query("select * from `sectionname`  where 1");
                                       
                                       $numsection = mysql_num_rows($fetch_section);
                                       
                                       if ($numsection > 0) {
                                       
                                           while ($section = mysql_fetch_array($fetch_section)) {
                                               ?>
                                    <option <?php
                                       if ($_REQUEST['section_id'] == $section['id']) {
                                           echo 'selected';
                                       }
                                       ?> value="<?php echo $section['id']; ?>"><?php echo $section['sectionname']; ?></option>
                                    <?php
                                       }
                                       }
                                       ?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-3 control-label">Shift</label>
                              <div class="col-md-5">
                                 <select class="form-control"  name="shift_id" required="" >
                                    <option value=""> select Shift</option>
                                    <?php
                                       $fetch_shift = mysql_query("select * from `shiftname`  where 1");
                                       
                                       $numshift = mysql_num_rows($fetch_shift);
                                       
                                       if ($numshift > 0) {
                                       
                                           while ($shift = mysql_fetch_array($fetch_shift)) {
                                               ?>
                                    <option <?php
                                       if ($_REQUEST['shift_id'] == $shift['id']) {
                                           echo 'selected';
                                       }
                                       ?> value="<?php echo $shift['id']; ?>"><?php echo $shift['shiftname']; ?></option>
                                    <?php
                                       }
                                       }
                                       ?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-3 control-label">Group</label>
                              <div class="col-md-5">
                                 <select class="form-control" name="group">
                                    <option value="">Select Group</option>
                                    <?php 
                                       //Query to get the Designation list
                                       
                                       $sql_designation = "SELECT * FROM school_new_group WHERE name <> ''";
                                       
                                       $res_designation = mysql_query($sql_designation);
                                       
                                       
                                       
                                       if(mysql_num_rows($res_designation) > 0)
                                       
                                       {
                                       
                                           while ($row_designation = mysql_fetch_array($res_designation)) {
                                       
                                       ?>
                                    <option <?php if($_REQUEST['group']==$row_designation['id']) {echo 'selected';} ?> value="<?php echo $row_designation['id'];?>"><?php echo $row_designation['name'];?></option>
                                    <?php
                                       }
                                       
                                       }
                                       
                                       ?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-3 control-label">Month</label>
                              <div class="col-md-5">
                                 <?php
                                    // set the month array
                                    $formattedMonthArray = array("1" => "January", "2" => "February", "3" => "March", "4" => "April",
                                                        "5" => "May", "6" => "June", "7" => "July", "8" => "August",
                                                        "9" => "September", "10" => "October", "11" => "November", "12" => "December",
                                                    );
                                    ?>
                                 <!-- displaying the dropdown list -->
                                 <select name="month" required="" class="form-control"  >
                                    <option value="">Select Month</option>
                                    <?php
                                       foreach ($formattedMonthArray as $key=>$month)
                                       {
                                       ?>
                                    <option value="<?php echo $key; ?>" <?php echo $_REQUEST['month']==$key?"selected":""; ?>><?php echo $month; ?></option>
                                    <?php }?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="form-actions fluid">
                           <div class="row">
                              <div class="col-md-offset-3 col-md-9">
                                  <input type="submit" class="btn blue"  name="submit" id="submit_btn" value="submit">
                              </div>
                           </div>
                        </div>
                     </form>
                     <?php if($_REQUEST['submit']){ 
                        
                         $datestr=date("Y-".$_REQUEST['month'].'-d');
                         $date=date('d,F,Y',strtotime($datestr));
                         ?>
                     <div class="col-md-12" style=" text-align:center; font-weight:bold; font-size:14px;"><?php echo $date; ?></div>
                     <div class="portlet-body form">
                         <table  class="table table-striped table-hover table-bordered" id="sample_editable_1">
                        <thead>
                           <tr>
                              <th>Roll</th>
                              <th>Name</th>
                              <th>Regular Attendance</th>
                              <th>Late Attendance</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php 	
                              
                                        $fetch_sql="select * from `school_students`  where is_deleted=0";
                                        if(!empty($_REQUEST["class_id"]))
                                        {
                                            $fetch_sql.=" and class_id='".$_REQUEST['class_id']."'";
                                        }
                                        if(!empty($_REQUEST["section_id"]))
                                        {
                                            $fetch_sql.=" and section_id='".$_REQUEST['section_id']."'";
                                        }
                                        if(!empty($_REQUEST["shift_id"]))
                                        {
                                            $fetch_sql.=" and shiftid='".$_REQUEST['shift_id']."'";
                                        }
                                        if(!empty($_REQUEST["group"]))
                                        {
                                            $fetch_sql.=" and school_students.group='".$_REQUEST['group']."'";
                                        }
                              
                                        $fetch_shift=mysql_query($fetch_sql);	
                              
                              		$numshift=mysql_num_rows($fetch_shift);
                              
                              		if($numshift>0)
                              
                              		{
                              
                              		while($shift=mysql_fetch_array($fetch_shift))
                              		{ 
                              		
                              		?>
                           
                           <tr>
                              <td><?php echo $shift['roll'];  ?> </td>
                              <td><?php echo $shift['name'];  ?> </td>
                              <td><input type="checkbox" name="studen_id[]" checked="checked" value="<?php echo $shift['userid']; ?>"> </td>
                              <td><input type="checkbox" name="latestuden_id[]" checked="checked" value="<?php echo $shift['userid']; ?>"> </td>
                              <?php 
                                 
                                 } }
                                 
                                 
                                 ?>
                        </tbody>
                     </table>  
                     </div>
                     <?php } ?>
                     
                     <!-- END FORM-->
                  </div>
               </div>
            </div>
         </div>
         <!-- END PAGE CONTENT -->
      </div>
   </div>
   <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<div class="page-footer">
   <?php include("includes/footer.php"); ?>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<!--<script src="assets/admin/pages/scripts/table-editable.js"></script>-->
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>
<script>
   jQuery(document).ready(function () {
   
       Metronic.init(); // init metronic core components
   
       Layout.init(); // init current layout
   
       QuickSidebar.init(); // init quick sidebar
   
       Demo.init(); // init demo features
   
      // TableEditable.init();
   
   });
   
   $('.delete_all').on('click', function(e) { 
   var allVals = [];  
   var allValspar = [];  
   $(".sub_chk:checked").each(function() {  
   allVals.push($(this).attr('data-id'));
   allValspar.push($(this).attr('data-parent'));
   });  
   //alert(allVals.length); return false;  
   if(allVals.length <=0)  
   {  
   alert("Please select row."); 
   return false; 
   } 
   
   location.href = "view_student.php?cid=" + allVals + "&action=deletemulti&parentid="+allValspar;
   
   }); 
   
</script>
<script type="text/javascript">
   function deleteConfirm() {
   
       var result = confirm("Are you sure to delete product?");
   
       if (result) {
   
           return true;
   
       } else {
   
           return false;
   
       }
   
   }
   
   
   
   $(document).ready(function () {
   
   
   
       var table = $('#sample_editable_1').DataTable( {
   
   
       dom: 'Bfrtip',
       pageLength:7,
       searching:false,
      buttons: [
           
   //            {
   //                extend: 'excelHtml5',
   //                 messageTop: null,
   //                  filename: 'Data export',
   //                exportOptions: {
   //                    columns: [ 1,2,3,4,5,6,7,8,9,10 ]
   //                }
   //            },
   //           {
   //               extend: 'pdfHtml5',
   //               messageTop: null,
   //                filename: 'Data export',
   //               exportOptions: {
   //                   columns: [ 1,2,3,4,5,6,7,8,9,10 ]
   //               }
   //           },
       ],
       lengthMenu: [
   
               [5, 15, 20, -1],
   
               [5, 15, 20, "All"] // change per page values here
   
           ],
   
          
   } );
   
          $('.dataTables_filter').find('input').addClass('form-control input-small input-inline');
          $('#sample_editable_1 tfoot th').each( function () {
          var title = $(this).text();
         $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
   });
   
   
            table.columns().every( function () {
       var that = this;
   
       $( 'input', this.footer() ).on( 'keyup change', function () {
           if ( that.search() !== this.value ) {
               that
                   .search( this.value )
                   .draw();
           }
       } );
   } );
   
   
   
   
          $('.dataTables_filter').find('input').addClass('form-control input-small input-inline');
   
          $(".download_excel").click(function(){
          $("#student_filter").attr("action","student_excel.php");  
          $("#submit_btn").trigger("click");
          });
       $('.selectradio').click(function () {
           if ($(this).val() == 'eiin') {
               $('#show_eiin').show();
           } else {
   
               $('#show_eiin').hide();
   
           }
   
   
   
   
   
       });
   
   
       $('#select_all').on('click', function () {
   
           if (this.checked) {
   
               $('.checkbox').each(function () {
   
                   this.checked = true;
   
               });
   
           } else {
   
               $('.checkbox').each(function () {
   
                   this.checked = false;
   
               });
   
           }
   
       });
   
   
   
       $('.checkbox').on('click', function () {
   
           if ($('.checkbox:checked').length == $('.checkbox').length)
   
           {
   
               $('#select_all').prop('checked', true);
   
           } else {
   
               $('#select_all').prop('checked', false);
   
           }
   
       });
       $(".check_all").click(function(){
       chkbxid=$(this).attr("id");
       if(this.checked)
       {
           $("."+chkbxid+"chk").prop("checked",true);
           $("."+chkbxid+"chk").parent().addClass("checked");
       }
       else
       {
          $("."+chkbxid+"chk").prop("checked",false);
          $("."+chkbxid+"chk").parent().removeClass("checked");
   
       }
       
       });
       $(".datecheckbx").click(function(){
        string=$(this).attr("class");
        $current_class=string.replace('datecheckbx', '').trim(); 
        $total_checked=parseInt($('input.'+$current_class+':checked').length)
        $totalbox=parseInt($('input.'+$current_class+'').length);
        if(this.checked)
        {
            if($totalbox==$total_checked)
            {
                $all_chkbx_id=$current_class.replace("chk",'').trim();
                $("#"+$all_chkbx_id).prop("checked",true);
                $("#"+$all_chkbx_id).parent().addClass("checked");
            }
        }
        else
        {
            $all_chkbx_id=$current_class.replace("chk",'').trim();
            $("#"+$all_chkbx_id).prop("checked",false);
            $("#"+$all_chkbx_id).parent().removeClass("checked");
        }
       });
   
   });
   
   
   
   
   
   
   
</script>
<script>
   $(document).ready(function () {
   
       $(".san_open").parent().parent().addClass("active open");
   
   });
   
</script>
<style type="text/css">
   tfoot {
   display: table-header-group;
   }
   tfoot input {
   width: 100%;
   padding: 6px;
   box-sizing: border-box;
   font-size: 12px;
   }
</style>
</body><!-- END BODY -->
</html>

