<?php
ob_start();
session_start();
include_once("./includes/config.php");

require('fpdf181/fpdf.php');

function hex2dec($couleur = "#000000"){
    $R = substr($couleur, 1, 2);
    $rouge = hexdec($R);
    $V = substr($couleur, 3, 2);
    $vert = hexdec($V);
    $B = substr($couleur, 5, 2);
    $bleu = hexdec($B);
    $tbl_couleur = array();
    $tbl_couleur['R']=$rouge;
    $tbl_couleur['V']=$vert;
    $tbl_couleur['B']=$bleu;
    return $tbl_couleur;
}

//conversion pixel -> millimeter at 72 dpi
function px2mm($px){
    return $px*25.4/72;
}

function txtentities($html){
    $trans = get_html_translation_table(HTML_ENTITIES);
    $trans = array_flip($trans);
    return strtr($html, $trans);
}
////////////////////////////////////

class PDF_HTML extends FPDF
{
//variables of html parser
protected $B;
protected $I;
protected $U;
protected $HREF;
protected $fontList;
protected $issetfont;
protected $issetcolor;

function __construct($orientation='P', $unit='mm', $format='A4')
{
    //Call parent constructor
    parent::__construct($orientation,$unit,$format);
    //Initialization
    $this->B=0;
    $this->I=0;
    $this->U=0;
    $this->HREF='';
    $this->fontlist=array('arial', 'times', 'courier', 'helvetica', 'symbol');
    $this->issetfont=false;
    $this->issetcolor=false;
}

function WriteHTML($html)
{
    //HTML parser
    $html=strip_tags($html,"<b><u><i><a><img><p><br><strong><em><font><tr><blockquote>"); //supprime tous les tags sauf ceux reconnus
    $html=str_replace("\n",' ',$html); //remplace retour à la ligne par un espace
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE); //éclate la chaîne avec les balises
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(5,stripslashes(txtentities($e)));
        }
        else
        {
            //Tag
            if($e[0]=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag, $attr)
{
    //Opening tag
    switch($tag){
        case 'STRONG':
            $this->SetStyle('B',true);
            break;
        case 'EM':
            $this->SetStyle('I',true);
            break;
        case 'B':
        case 'I':
        case 'U':
            $this->SetStyle($tag,true);
            break;
        case 'A':
            $this->HREF=$attr['HREF'];
            break;
        case 'IMG':
            if(isset($attr['SRC']) && (isset($attr['WIDTH']) || isset($attr['HEIGHT']))) {
                if(!isset($attr['WIDTH']))
                    $attr['WIDTH'] = 0;
                if(!isset($attr['HEIGHT']))
                    $attr['HEIGHT'] = 0;
                $this->Image($attr['SRC'], $this->GetX(), $this->GetY(), px2mm($attr['WIDTH']), px2mm($attr['HEIGHT']));
            }
            break;
        case 'TR':
        case 'BLOCKQUOTE':
        case 'BR':
            $this->Ln(5);
            break;
        case 'P':
            $this->Ln(10);
            break;
        case 'FONT':
            if (isset($attr['COLOR']) && $attr['COLOR']!='') {
                $coul=hex2dec($attr['COLOR']);
                $this->SetTextColor($coul['R'],$coul['V'],$coul['B']);
                $this->issetcolor=true;
            }
            if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist)) {
                $this->SetFont(strtolower($attr['FACE']));
                $this->issetfont=true;
            }
            break;
    }
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='STRONG')
        $tag='B';
    if($tag=='EM')
        $tag='I';
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
    if($tag=='FONT'){
        if ($this->issetcolor==true) {
            $this->SetTextColor(0);
        }
        if ($this->issetfont) {
            $this->SetFont('arial');
            $this->issetfont=false;
        }
    }
}

function SetStyle($tag, $enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
    {
        if($this->$s>0)
            $style.=$s;
    }
    $this->SetFont('',$style);
}

function PutLink($URL, $txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(5,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}

}//end of class

	

	 $sql="SELECT * FROM `school_subjectgroup` WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'";
//	exit;
	$rs=mysql_query($sql) or die(mysql_error());
	if($row=mysql_fetch_array($rs))
	{

		$fetch_class=mysql_query("select * from `classname`  where id='".$row['class_id']."'");	
		$class=mysql_fetch_array($fetch_class);
              
                $sqlpro=mysql_query("SELECT * FROM `school_adminprofile` where userid=1"); 
                $rowpro=mysql_fetch_array($sqlpro);
                $rowparent = mysql_fetch_array(mysql_query("SELECT * FROM `school_admin` WHERE `id`='".mysql_real_escape_string($row['parentid'])."'"));
                $rowshift = mysql_fetch_array(mysql_query("SELECT `shiftname` FROM `shiftname` WHERE `id`='" . mysql_real_escape_string($row['shift_id']) . "'"));
                $rowgroup = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_new_group` WHERE `id`='" . mysql_real_escape_string($row['allgroup']) . "'"));
                $studentty = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_studenttype` WHERE `id`='" . mysql_real_escape_string($row['studenttype'])."'"));
                $speciqu = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_quota` WHERE `id`='" . mysql_real_escape_string($row['specialquata'])."'"));
                $sqladmin=mysql_query("SELECT * FROM `school_admin` where id=1"); 
                $fourth_subjects = explode(",", $row['fourth_subject']);
                $group_subjects = explode(",", $row['group_subject']);
                $gr_third_fourth_subs = explode(",", $row['gr_third_fourth_sub']);
                $common_subjects = explode(",", $row['common_subject']);
                $religion_subjects = explode(",", $row['religion_subject']);
                $all_subjects= array_merge($common_subjects,$religion_subjects,$fourth_subjects,$group_subjects,$gr_third_fourth_subs);
               $all_subjects= array_filter($all_subjects);
                $rowadmin=mysql_fetch_array($sqladmin);
 if($rowadmin['image']==''){
   $image_link='http://mhcds.dartmouth.edu/images/made/uploads/mhcds/images/img_profile_goldberg_301_303_s_c1.jpg';
 }else{
    $image_link=SITE_URL.'upload/documents/'.$rowadmin['image'];
 }

 if($rowsimg['image']==''){
   $image_linkstu=SITE_URL.'upload/no.png';
 }else{
    $image_linkstu=SITE_URL.'upload/documents/'.$rowsimg['image'];
 }
 if(empty($rowgroup['name'])){
    $groupname='None';
}else{
    $groupname=stripslashes($rowgroup['name']);
} 

 if(empty($studentty['name'])){
    $studentty='None';
}else{
    $studentty=stripslashes($studentty['name']);
} 

$fetch_subject=mysql_query("select * from `allsubject`  where id IN(".  implode(",", $all_subjects).")  order by list_order");	
$numsubject=mysql_num_rows($fetch_subject);	
if($numsubject>0)

{
$count=0;
while($subject=mysql_fetch_array($fetch_subject))
{
    
    
    $subjects.="<tr style='border: 1px solid black'>"
            . "<td style='border: 1px solid black;text-align:center;'>".++$count."</td>"
            . "<td style='border: 1px solid black;'>".$subject["subjectname"]."</td>"
            . "<td style='border: 1px solid black;text-align:center;'>".$subject["subject_sort_name"]."</td>"
            . "<td style='border: 1px solid black;text-align:center;'>".$subject["subjectcode"]."</td>"
            . "</tr>";

}
        

}
$header='<div style="width:100%;margin:0 auto;text-align: center;"><img height="60" width="60" src="'.$image_link.'" ></div>';
$header.='<div style="width:100%;margin:0 auto;text-align: center;color:#000000;font-size:20px;font-weight:bold;">'.$rowpro['institution'].'</div>';
$header.='<div style="width:100%;margin:0 auto;text-align: center;color:#000000;font-size:20px;font-weight:bold; padding-bottom:15px;">Subject List</div>';
$footer='<div style="float:left;width:40%;">___________________________<br />Class Teacher</div> <div style="float:left;width:60%;text-align:right;">_______________________________<br />Head of the Institute</div>'; 

$html='
<table style="width:100%;"  cellpadding="3">
<tr style="background-color:#ccc;">
<td valign="middle" style="font-weight:bold;">Shift - '.$rowshift['shiftname'].'</td>
<td valign="middle" style="font-weight:bold;">Class - '.$class['classname'].'</td>
<td valign="middle" style="font-weight:bold;">Group - '.$groupname.'</td>
</tr>
<tr>
    <td colspan="3">
        <table style="width:100%;border-collapse: collapse;" >
        <tr style="border: 1px solid black">
            <th style="text-align:center;border:1px solid black;">Order</th>
            <th style="text-align:center;border:1px solid black;">Subject Name</th>
            <th style="text-align:center;border:1px solid black;">Subject Short Name</th>
            <th style="text-align:center;border:1px solid black;">Subject Code</th>
         </tr>   
         '.$subjects.'

        </table>

     </td>
</tr>




   </table>
';
//echo $html;exit;

	include("mpdf60/mpdf.php");
	
	$mpdf=new mPDF('', 'A4-P','',''); 
        $mpdf->setAutoTopMargin ='stretch';
        $mpdf->setAutoBottomMargin ='stretch';
        $mpdf->SetHTMLHeader($header);  
        $mpdf->SetHTMLFooter($footer); 
   // $mpdf->autoLangToFont = true;
	$mpdf->WriteHTML($html);

	$mpdf->Output('subjectdistribution.pdf','D');
	exit;	

	}
	else
	{
		$msg="Invalid Username or Password.";
        $_SESSION['msg']=$msg;
	}


?>

  

