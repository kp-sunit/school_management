<?php
ob_start();
?>
<link  href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link  href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
<?php
   include_once("./includes/session.php");
   
   //include_once("includes/config.php");
   
   include_once("./includes/config.php");
   
   $url = basename(__FILE__) . "?" . (isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : 'cc=cc');
   
   
   if(isset($_REQUEST['submit']))
   {
           $month=!empty($_REQUEST['monthval'])?$_REQUEST['monthval']:'';
           $year=!empty($_REQUEST['yearval'])?$_REQUEST['yearval']:'';  
           $dval=cal_days_in_month(CAL_GREGORIAN,$month,$year);
           $working_day= array();
           for($count=1;$count<=$dval;$count++)
           {
               $date =$year."-".sprintf("%02d",$month)."-".sprintf("%02d",$count);
               if(!empty($_REQUEST[$date]))
               {
                   $working_day[$date]= 1;
               }
               else
               {
                   $working_day[$date]= 0;
               }
           }
           $check_all=!empty($_REQUEST["check_all"])?1:0;
           mysql_query("update school_schooldays set select_year='".$year."',select_month='".$month."',working_day='".json_encode($working_day)."',check_all='".$check_all."' where id='".$_REQUEST['id']."'") or die(mysql_error());
           header("Location:view_schoolday.php");
   }
   
  $categoryRowset = mysql_fetch_array(mysql_query("select * from `school_schooldays` where `id`='".mysql_real_escape_string($_REQUEST['id'])."'"));
  $working_days=json_decode($categoryRowset['working_day']);
//  echo "<pre>";
//  print_r($working_days);
//  exit;
  
   
   
   
   
   
   ?>
<script language="javascript">
   function del(aa, bb)
   
   {
   
       var a = confirm("Are you sure, you want to delete this?")
   
       if (a)
   
       {
   
           location.href = "view_student.php?cid=" + aa + "&action=delete&parentid="+bb;
   
       }
   
   }
   
   
   
   function inactive(aa)
   {
       location.href = "list_teacher.php?cid=" + aa + "&action=inactive"
   }
   
   function active(aa)
   {
       location.href = "list_teacher.php?cid=" + aa + "&action=active";
   }
   
   
   
</script>
<?php include("includes/header.php"); ?>
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
   <!-- BEGIN SIDEBAR -->
   <?php include("includes/left_panel.php"); ?>
   <!-- END SIDEBAR -->
   <!-- BEGIN CONTENT -->
   <div class="page-content-wrapper">
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- /.modal -->
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN STYLE CUSTOMIZER -->
         <!-- END STYLE CUSTOMIZER -->
         <!-- BEGIN PAGE HEADER-->
         <h3 class="page-title">  School Days  </h3>
         <div class="page-bar">
            <ul class="page-breadcrumb">
               <li> <i class="fa fa-home"></i> <a href="index.php">Home</a> <i class="fa fa-angle-right"></i> </li>
               <li> <a href="#"> School Days </a> <i class="fa fa-angle-right"></i> </li>
               <!--<li>
                  <a href="#">Editable Datatables</a>
                  
                  </li>-->
            </ul>
         </div>
         <!-- END PAGE HEADER-->
         <!-- BEGIN PAGE CONTENT-->
         <div class="row">
            <div class="col-md-12">
               <div class="portlet box blue">
                  <div class="portlet-title">
                     <div class="caption">
                        <i class="fa fa-gift"></i>Edit School Days 
                     </div>
                     <div class="tools">
                     </div>
                  </div>
                  <div class="portlet-body form">
                     <!-- BEGIN FORM-->
                     <form  class="form-horizontal" method="post" action="" enctype="multipart/form-data" id="student_filter">
                         <input type="hidden" name="monthval" value="<?php echo $categoryRowset['select_month'] ?>">  
                         <input type="hidden" name="yearval" value="<?php echo $categoryRowset['select_year'] ?>">  
                         <input type="hidden" name="id" value="<?php echo $categoryRowset['id'] ?>">  
                        <div class="form-body">
                           <div class="form-group">
                              <label class="col-md-3 control-label">Year</label>
                              <div class="col-md-5">
                                  <select class="form-control"  name="year" required="" disabled="" >
                                    <option value=""> Select Year</option>
                                     <?php
                                    for($i=2010;$i<=date('Y');$i++)
                                    {      
                                    ?>
                                    <option value="<?php echo $i; ?>" <?php echo $i==$categoryRowset['select_year']?"selected":""; ?>><?php echo $i; ?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-3 control-label">Month</label>
                              <div class="col-md-5">
                                 <?php
                                    // set the month array
                                    $formattedMonthArray = array("1" => "January", "2" => "February", "3" => "March", "4" => "April",
                                                        "5" => "May", "6" => "June", "7" => "July", "8" => "August",
                                                        "9" => "September", "10" => "October", "11" => "November", "12" => "December",
                                                    );
                                    ?>
                                 <!-- displaying the dropdown list -->
                                 <select name="month[]"  class="form-control" disabled="">
                                 <?php
                                 foreach ($formattedMonthArray as $key =>$month)
                                 {
                                 ?>
                                     <option value="<?php echo $key; ?>" <?php echo $key==$categoryRowset['select_month']?"selected":""; ?>><?php echo $month; ?></option>    
                                 <?php } ?>    
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-3 control-label"></label>
                              <div class="col-md-5">
                                
                                          <table class="table table-striped table-hover table-bordered">
                                             
                                             <tr>
                                                <td>Date</td>
                                                <td>Day</td>
                                                <td><input type="checkbox" name="check_all" value="1" class="check_all" id="check_all" <?php echo !empty($categoryRowset['check_all'])?"checked":""; ?>></td>
                                             </tr>
                                             <?php   
                                               
                                                foreach ($working_days as $date =>$val)
                                                {
                                                
                                                   
                                                ?>
                                             <tr>
                                                <td><?php echo date("j",strtotime($date)); ?></td>
                                                <td><?php echo date("l",strtotime($date)); ?></td>
                                                <td>
                                                   <input type="checkbox" class="datecheckbx" name="<?php echo $date; ?>" value="1" <?php echo $val?"checked":""; ?>>
                                                </td>
                                             </tr>
                                             <?php } ?>
                                          </table>
                                       </td>
                                    
                                    </tr>
                                 
                              </div>
                           </div>
                            <div class="form-actions fluid">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn blue"  name="submit" id="submit_btn">Submit</button>

                                        </div>
                                    </div>
                                </div>
                        </div>
                     </form>
                     <!-- END FORM-->
                  </div>
               </div>
            </div>
         </div>
         <!-- END PAGE CONTENT -->
      </div>
   </div>
   <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<div class="page-footer">
   <?php include("includes/footer.php"); ?>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<!--<script src="assets/admin/pages/scripts/table-editable.js"></script>-->
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>
<script>
   jQuery(document).ready(function () {
   
       Metronic.init(); // init metronic core components
   
       Layout.init(); // init current layout
   
       QuickSidebar.init(); // init quick sidebar
   
       Demo.init(); // init demo features
   
      // TableEditable.init();
   
   });
   
   $('.delete_all').on('click', function(e) { 
   var allVals = [];  
   var allValspar = [];  
   $(".sub_chk:checked").each(function() {  
   allVals.push($(this).attr('data-id'));
   allValspar.push($(this).attr('data-parent'));
   });  
   //alert(allVals.length); return false;  
   if(allVals.length <=0)  
   {  
   alert("Please select row."); 
   return false; 
   } 
   
   location.href = "view_student.php?cid=" + allVals + "&action=deletemulti&parentid="+allValspar;
   
   }); 
   
</script>
<script type="text/javascript">
   function deleteConfirm() {
   
       var result = confirm("Are you sure to delete product?");
   
       if (result) {
   
           return true;
   
       } else {
   
           return false;
   
       }
   
   }
   
   
   
   $(document).ready(function () {
   
   
   
       var table = $('#sample_editable_1').DataTable( {
   
   
       dom: 'Bfrtip',
   
      buttons: [
           
   //            {
   //                extend: 'excelHtml5',
   //                 messageTop: null,
   //                  filename: 'Data export',
   //                exportOptions: {
   //                    columns: [ 1,2,3,4,5,6,7,8,9,10 ]
   //                }
   //            },
           {
               extend: 'pdfHtml5',
               messageTop: null,
                filename: 'Data export',
               exportOptions: {
                   columns: [ 1,2,3,4,5,6,7,8,9,10 ]
               }
           },
       ],
       lengthMenu: [
   
               [5, 15, 20, -1],
   
               [5, 15, 20, "All"] // change per page values here
   
           ],
   
          
   } );
   
          $('.dataTables_filter').find('input').addClass('form-control input-small input-inline');
          $('#sample_editable_1 tfoot th').each( function () {
          var title = $(this).text();
         $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
   });
   
   
            table.columns().every( function () {
       var that = this;
   
       $( 'input', this.footer() ).on( 'keyup change', function () {
           if ( that.search() !== this.value ) {
               that
                   .search( this.value )
                   .draw();
           }
       } );
   } );
   
   
   
   
          $('.dataTables_filter').find('input').addClass('form-control input-small input-inline');
   
          $(".download_excel").click(function(){
          $("#student_filter").attr("action","student_excel.php");  
          $("#submit_btn").trigger("click");
          });
       $('.selectradio').click(function () {
           if ($(this).val() == 'eiin') {
               $('#show_eiin').show();
           } else {
   
               $('#show_eiin').hide();
   
           }
   
   
   
   
   
       });
   
   
       $('#select_all').on('click', function () {
   
           if (this.checked) {
   
               $('.checkbox').each(function () {
   
                   this.checked = true;
   
               });
   
           } else {
   
               $('.checkbox').each(function () {
   
                   this.checked = false;
   
               });
   
           }
   
       });
   
   
   
       $('.checkbox').on('click', function () {
   
           if ($('.checkbox:checked').length == $('.checkbox').length)
   
           {
   
               $('#select_all').prop('checked', true);
   
           } else {
   
               $('#select_all').prop('checked', false);
   
           }
   
       });
       $("#check_all").click(function(){
       if(this.checked)
       {
           $(".datecheckbx").prop("checked",true);
           $(".datecheckbx").parent().addClass("checked");
       }
       else
       {
          $(".datecheckbx").prop("checked",false);
          $(".datecheckbx").parent().removeClass("checked");
   
       }
       
       });
       $(".datecheckbx").click(function(){
       
        $total_checked=parseInt($('input.datecheckbx:checked').length)
        $totalbox=parseInt($('input.datecheckbx').length);
        if(this.checked)
        {
            if($totalbox==$total_checked)
            {
               
                $("#check_all").prop("checked",true);
                $("#check_all").parent().addClass("checked");
            }
        }
        else
        {
            $("#check_all").prop("checked",false);
            $("#check_all").parent().removeClass("checked");
        }
       });
   
   });
   
   
   
   
   
   
   
</script>
<script>
   $(document).ready(function () {
   
       $(".san_open").parent().parent().addClass("active open");
   
   });
   
</script>
<style type="text/css">
   tfoot {
   display: table-header-group;
   }
   tfoot input {
   width: 100%;
   padding: 6px;
   box-sizing: border-box;
   font-size: 12px;
   }
</style>
</body><!-- END BODY -->
</html>

