<?php
ob_start();
include_once('includes/session.php');
include_once("includes/config.php");
include_once("includes/functions.php");
include("chat/chat.php");
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2
Version: 3.6.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
  <!--<![endif]-->
  <!-- BEGIN HEAD -->
  <head>
    <?php
$pagename12 = end(explode('/', $_SERVER['REQUEST_URI']));
$pagename22 = explode('.', $pagename12);
$curpagename = $pagename22[0];
$curpagename = str_replace("_", " ", $curpagename);
?>
    <meta charset="utf-8"/>
    <title>
      <?php echo ucfirst($curpagename); ?>
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
    <link id="style_color" href="assets/admin/layout/css/themes/blue.css" rel="stylesheet" type="text/css"/>
    <link href="assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
    <style>
      h3.page-title{
        display:none;
      }
    </style>
  </head>
  <!-- END HEAD -->
  <!-- BEGIN BODY -->
  <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
  <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
  <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
  <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
  <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
  <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
  <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
  <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
  <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
  <body class="page-header-fixed page-quick-sidebar-over-content ">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
      <!-- BEGIN HEADER INNER -->
      <div class="page-header-inner">
        <!-- BEGIN LOGO -->

	        <div class="col-md-1 col-sm-1 col-xs-1" style="padding-top:13px;">
	          <?php 
	$sqlpro=mysql_query("SELECT * FROM `school_adminprofile` where userid=1"); 
	$rowpro=mysql_fetch_array($sqlpro);
	?>
	          <?php 
	$sqladmin=mysql_query("SELECT * FROM `school_admin` where id=1"); 
	$rowadmin=mysql_fetch_array($sqladmin);
	?>
	          <a href="dashboard.php">
	            <?php
	//  $fetch_logo=mysql_fetch_array(mysql_query("select * from `dimri_sitesettings` where `id`=1"));
	?>
	            <?php if($rowadmin['image']==''){
	?>
	            <img src=""  alt="logo" class="logo-default" style="height:25px; width:100px;" />
	            <?php }else{ 
	$image_link=SITE_URL.'upload/documents/'.$rowadmin['image'];
	?>
	            <img src="<?php echo $image_link ?>" alt="logo" class="logo-default" style="height:25px;" />
	            <?php }?> 
	          </a>
	          <div class="menu-toggler sidebar-toggler hide">
	            ADMIN PANEL
	          </div>
	        </div>
	        <div class="col-md-7 col-sm-11 col-xs-9" style="padding-top:7px;">
	          <div style="margin-top:5px; color:#fff; font-size:18px;" class="school-name"> 
	            <strong>
	              <?php echo $rowpro['institution'] ?>
	            </strong>
	          </div>
	        </div>

        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
          
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
      
        <div class="top-menu">
          <!--  ======================== get notificatins from stat tabel for logged in user like student,teacher,gardian  ================ -->
          <?php
$user_type = $_SESSION['user_type']; //1=>admin,2=>Stuff,3=>Teacher,4=>Student,5=>Parent
$user_id = $_SESSION['myy'];
switch ($user_type) {
case '1':
//echo "select count(*) as `total_new_msg` from `school_sent_sms_admin` where `to_user_id` ='" . $user_id . "' and `is_new`= '1' "; exit;
$new_messages = mysql_fetch_assoc(mysql_query("select count(*) as `total_new_msg` from `school_sent_sms_admin` where `to_user_id` ='" . $user_id . "' and `is_new`= '1' "));
break;
case '3':
$new_messages = mysql_fetch_assoc(mysql_query("select count(*) as `total_new_msg` from `school_sent_sms_teacher` where `to_teacher_id` ='" . $user_id . "' and `is_new`= '1' "));
//$new_messages['total_new_msg'];
break;
case '4':
$new_messages = mysql_fetch_assoc(mysql_query("select count(*) as `total_new_msg` from `school_sent_sms_student` where `student_id` ='" . $user_id . "' and `is_new`= '1' "));
break;
case '5':
$new_messages = mysql_fetch_assoc(mysql_query("select count(*) as `total_new_msg` from `school_sent_sms_guardian` where `guardian_id` ='" . $user_id . "' and `is_new`= '1' "));
break;
default:
break;
}
?>
          <!--  ======================== END get notificatins from stat tabel for logged in user like student,teacher,gardian  ============= -->
          <ul class="nav navbar-nav pull-right">
            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
              <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <i class="icon-bell">
                </i>
                <?php 
//echo $new_messages['total_new_msg'] ;
if ($new_messages['total_new_msg'] > 0) { ?>
                <span class="badge badge-default">
                  <?php echo $new_messages['total_new_msg']; ?> 
                </span>
                <?php } ?>
              </a>
              <ul class="dropdown-menu">
                <li class="external">
                  <h3>
                    <span class="bold">
                      <?php echo ($new_messages['total_new_msg'] > 0) ? $new_messages['total_new_msg'] : 0; ?> unread
                    </span> messages
                  </h3>
                  <a href="list_new_messages.php">view all
                  </a> 
                </li>
              </ul>
            </li>
            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
              <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <i class="icon-envelope-open">
                </i>
                <?php 
//echo $new_messages['total_new_msg'] ;
if ($new_messages['total_new_msg'] > 0) { ?>
                <?php /*?>
                <span class="badge badge-default">
                  <?php echo $new_messages['total_new_msg']; ?> 
                </span>
                <?php */?>
                <?php } ?>
              </a>
            </li>
           
            <!-- END TODO DROPDOWN -->
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
            <li class="dropdown dropdown-user">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <!--<img alt="" class="img-circle" src="assets/admin/layout/img/avatar3_small.jpg"/>-->
                <span class="username username-hide-on-mobile">
                  <!-- Get the current User type -->
                  <?php
/* =======================Query to get the loggedin user type================================ */
$sql_user_type = mysql_query("SELECT * FROM  `school_admin` WHERE username = '" . $_SESSION['username'] . "'");
$row_user_type = mysql_fetch_array($sql_user_type);
/* =======================Query to get the loggedin user type================================ */
if ($row_user_type['is_admin'] == 1) {
$header_name = 'Admin';
} elseif ($row_user_type['is_admin'] == 0) {
$header_name = 'Sub Admin';
} elseif ($row_user_type['is_admin'] == 2) {
$admin_id = $row_user_type['id'];
$sql_insti_name = mysql_query("SELECT * FROM  `exammanage_institution` WHERE admin_id = '" . $admin_id . "'");
$row_insti_name = mysql_fetch_array($sql_insti_name);
$header_name = $row_insti_name['name'];
} elseif ($row_user_type['is_admin'] == 3) {
$admin_id = $row_user_type['id'];
$sql_teac_name = mysql_query("SELECT * FROM  `exammanage_teachers` WHERE user_id = '" . $admin_id . "'");
$row_teac_name = mysql_fetch_array($sql_teac_name);
$header_name = $row_teac_name['fname'] . ' ' . $row_teac_name['mname'] . ' ' . $row_teac_name['lname'];
}
?>
                  Welcome, &nbsp;
                  <?php echo $header_name; ?>
                  <?php if ($_SESSION['username'] != '') { ?>	(
                  <?php echo $_SESSION['username']; ?>)
                  <?php } ?>
                </span>
                <i class="fa fa-angle-down">
                </i>
              </a>
              <ul class="dropdown-menu dropdown-menu-default">
                <li>
                  <a href="changeusername.php">
                    <i class="icon-pencil">
                    </i> Change Username 
                  </a>
                </li>
                <li>
                  <a href="changeemail.php">
                    <i class="icon-pencil">
                    </i> Change Email 
                  </a>
                </li>
                <li>
                  <a href="changepassword.php">
                    <i class="icon-pencil">
                    </i> Change Password 
                  </a>
                </li>
                <li>
                  <a href="logout.php">
                    <i class="icon-key">
                    </i> Log Out 
                  </a>
                </li>
              </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
            <li class="dropdown dropdown-quick-sidebar-toggler">
              <!--<a href="logout.php" class="dropdown-toggle">
<i class="icon-logout"></i>
</a>-->
            </li>
            <!-- END QUICK SIDEBAR TOGGLER -->
          </ul>
        </div>

        <!-- END TOP NAVIGATION MENU -->
      </div>
      <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
<div class="clearfix"></div>