<?php 
  ob_start();  
  include_once("./includes/session.php");
  //include_once("includes/config.php"); 
   include_once("./includes/config.php");
   $url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
   
   ?>
<?php
   if(isset($_REQUEST['submit']))
   
   {
   
   	$shift_id = isset($_POST['shift_id']) ? $_POST['shift_id'] : '';
   	$class_id = isset($_POST['class_id']) ? $_POST['class_id'] : '';
   	$subject_id = isset($_POST['subject_id']) ? $_POST['subject_id'] : '';
   	$total_mark = isset($_POST['total_mark']) ? $_POST['total_mark'] : '';
   	$id = isset($_POST['id']) ? $_POST['id'] : '';
   	$noncqandmcq = isset($_POST['noncqandmcq']) ? $_POST['noncqandmcq'] : '';
   	$cq = isset($_POST['cq']) ? $_POST['cq'] : '';
   	$mcq = isset($_POST['mcq']) ? $_POST['mcq'] : '';
   	$paractical = isset($_POST['paractical']) ? $_POST['paractical'] : '';
   	$ca = isset($_POST['ca']) ? $_POST['ca'] : '';
   	$order = isset($_POST['order']) ? $_POST['order'] : '';
   	$fields = array(
   
   		'shift_id' => mysql_real_escape_string($shift_id),
   		'class_id'=> mysql_real_escape_string($class_id),
   		'subject_id'=> mysql_real_escape_string($subject_id),
   		'total_mark' => mysql_real_escape_string($total_mark),
   		'noncqandmcq' => mysql_real_escape_string($noncqandmcq),
   		'cq' => mysql_real_escape_string($cq),
               	'mcq' => mysql_real_escape_string($mcq),
   		'paractical' => mysql_real_escape_string($paractical),
   		'ca' => mysql_real_escape_string($ca),
   		'order' => mysql_real_escape_string($order),

   		);
   		$fieldsList = array();
   
   		foreach ($fields as $field => $value) {
   
   			$fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
   
   		}
   
   	 if($_REQUEST['action']=='edit')
   
   	  {		  
   
   	 $editQuery = "UPDATE `school_passmarks` SET " . implode(', ', $fieldsList)
   
   			. " WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'";
   
   		//	exit;
   		if (mysql_query($editQuery)) {
   			$_SESSION['msg'] = "Teaching plan Updated Successfully";
   
   		}
   
   		else {
   
   			$_SESSION['msg'] = "Error occuried while updating Teaching plan";
   
   		}
   		header('Location:list_passmark.php');
   
   		exit();
   
   	
   
   	 }
   
   	 else
   
   	 {
   	 $addQuery = "INSERT INTO `school_passmarks` (`" . implode('`,`', array_keys($fields)) . "`)"
   
   			. " VALUES ('" . implode("','", array_values($fields)) . "')";
   
   			
   
   			//exit;
   		mysql_query($addQuery) or die(mysql_error());   
   		header('Location:list_passmark.php');
   		exit();
   	 }
  
   } 
   if($_REQUEST['action']=='edit')
   
   {
   
   $categoryRowset = mysql_fetch_array(mysql_query("SELECT * FROM `school_passmarks` WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'"));
   
   
   
   }
   
   ?>
<?php include('includes/header.php');?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<?php include('includes/left_panel.php');?>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
   <div class="page-content">
      <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <!-- /.modal -->
      <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <!-- BEGIN STYLE CUSTOMIZER -->
      <?php //include('includes/style_customize.php');?>
      <!-- END STYLE CUSTOMIZER -->
      <!-- BEGIN PAGE HEADER-->
      <h3 class="page-title">Add Pass Marks </h3>
      <div class="page-bar">
         <ul class="page-breadcrumb">
            <li>
               <i class="fa fa-home"></i>
               <a href="dashboard.php">Home</a>
               <i class="fa fa-angle-right"></i>
            </li>
            <li>
               <a href="#">Add Pass Marks </a>
            </li>
         </ul>
      </div>
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->
      <div class="row">
         <div class="col-md-12">
            <div class="portlet box blue">
               <div class="portlet-title">
                  <div class="caption">
                     <i class="fa fa-gift"></i><?php echo $_REQUEST['action']=='edit'?"Edit":"Add";?> Pass Marks
                  </div>
                  <div class="tools">
                  </div>
               </div>
               <div class="portlet-body form">
                  <!-- BEGIN FORM-->
                  <form  class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                     <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
                     <input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>" />
                     <div class="form-body">
                        
                        <div class="form-group">
                           <label class="col-md-3 control-label">Shift</label>
                            <div class="col-md-4">
                                <select name="shift_id" required class="form-control">
                                  <option value="">Select Shift</option>
                                  <?php 
                                  $sql_shift=mysql_query("select * from `shiftname` where is_deleted=0");
                                  while($shift=mysql_fetch_assoc($sql_shift))
                                  {
                                  ?>
                                  <option value="<?php echo $shift["id"] ?>" <?php echo $categoryRowset["shift_id"]==$shift["id"]?"selected":"";  ?>> <?php echo $shift["shiftname"] ?></option>
                                  <?php }?>
                                  
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-3 control-label">Class</label>
                           <div class="col-md-4">
                               <select name="class_id" required class="form-control">
                                  <option value="">Select Class</option>
                                  <?php 
                                  $sql_classname=mysql_query("select * from `classname` where status=1 order by frontorder");
                                  while($classname=mysql_fetch_assoc($sql_classname))
                                  {
                                  ?>
                                  <option value="<?php echo $classname["id"] ?>" <?php echo $categoryRowset["class_id"]==$classname["id"]?"selected":"";  ?>> <?php echo $classname["classname"] ?></option>
                                  <?php }?>
                                  
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-3 control-label">Subject</label>
                           <div class="col-md-4">
                           <select name="subject_id" required class="form-control">
                                  <option value="">Select Subject</option>
                                  
                                  <?php 
                                  $sql_allsubject=mysql_query("select * from `allsubject` where is_deleted=0 order by list_order");
                                  while($allsubject=mysql_fetch_assoc($sql_allsubject))
                                  {
                                  ?>
                                  <option value="<?php echo $allsubject["id"] ?>" <?php echo $categoryRowset["subject_id"]==$allsubject["id"]?"selected":"";  ?>> <?php echo $allsubject["subjectname"] ?></option>
                                  <?php }?>
                              </select>    
                           </div>
                        </div>
                         <div class="form-group">
                           <label class="col-md-3 control-label">Total Mark</label>
                           <div class="col-md-4">
                             <input type="text" name="total_mark" class="form-control" value="<?php echo $categoryRowset["total_mark"] ?>" required onkeypress='return ((event.charCode >= 48 && event.charCode <= 57) || event.charCode==0)'>
                           </div>
                        </div>
                         <div class="form-group">
                           <label class="col-md-3 control-label">Non CQ & MCQ</label>
                           <div class="col-md-4">
                             <input type="text" name="noncqandmcq" class="form-control" value="<?php echo $categoryRowset["noncqandmcq"] ?>" required onkeypress='return ((event.charCode >= 48 && event.charCode <= 57) || event.charCode==0)'>
                           </div>
                        </div>
                       <div class="form-group">
                           <label class="col-md-3 control-label">CQ</label>
                           <div class="col-md-4">
                             <input type="text" name="cq" class="form-control" value="<?php echo $categoryRowset["cq"] ?>" required onkeypress='return ((event.charCode >= 48 && event.charCode <= 57) || event.charCode==0)'>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-3 control-label">MCQ</label>
                           <div class="col-md-4">
                             <input type="text" name="mcq" class="form-control" value="<?php echo $categoryRowset["mcq"] ?>" required onkeypress='return ((event.charCode >= 48 && event.charCode <= 57) || event.charCode==0)'>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-3 control-label">Practical</label>
                           <div class="col-md-4">
                             <input type="text" name="paractical" class="form-control" value="<?php echo $categoryRowset["paractical"] ?>" required onkeypress='return ((event.charCode >= 48 && event.charCode <= 57) || event.charCode==0)'>
                           </div>
                        </div> 
                       <div class="form-group">
                           <label class="col-md-3 control-label">CA</label>
                           <div class="col-md-4">
                             <input type="text" name="ca" class="form-control" value="<?php echo $categoryRowset["ca"] ?>" required onkeypress='return ((event.charCode >= 48 && event.charCode <= 57) || event.charCode==0)'>
                           </div>
                       </div>
                     
                         
                        <div class="form-actions fluid">
                           <div class="row">
                              <div class="col-md-offset-3 col-md-9">
                                 <button type="submit" class="btn blue"  name="submit">Submit</button>
                              </div>
                           </div>
                        </div>
                  </form>
                  <!-- END FORM-->
                  </div>
               </div>
            </div>
         </div>
         <!-- END PAGE CONTENT-->
      </div>
   </div>
   <style>
      .thumb{
      height: 60px;
      width: 60px;
      padding-left: 5px;
      padding-bottom: 5px;
      }
   </style>
   <script>
      window.preview_this_image = function (input) {
      
      
      
          if (input.files && input.files[0]) {
      
              $(input.files).each(function () {
      
                  var reader = new FileReader();
      
                  reader.readAsDataURL(this);
      
                  reader.onload = function (e) {
      
                      $("#previewImg").append("<span><img class='thumb' src='" + e.target.result + "'><img border='0' src='../images/erase.png'  border='0' class='del_this' style='z-index:999;margin-top:-34px;'></span>");
      
                  }
      
              });
      
          }
      
      }
      
   </script>
   <!-- END CONTENT -->
   <!-- BEGIN QUICK SIDEBAR -->
   <?php //include('includes/quick_sidebar.php');?>
   <!-- END QUICK SIDEBAR -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include('includes/footer.php'); ?>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/form-samples.js"></script>
<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
   jQuery(document).ready(function() {    
   
      // initiate layout and plugins
   
      Metronic.init(); // init metronic core components
   
   Layout.init(); // init current layout
   
   QuickSidebar.init(); // init quick sidebar
   
   Demo.init(); // init demo features
   
      FormSamples.init();
   
      
   
      
   
      
   
   });
   
   
   
   
       
   
</script>
<script>
   $(document).ready(function(){
   
       $(".san_open").parent().parent().addClass("active open");
   
   });
   
   document.getElementById("firstfield").focus();
   
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>

