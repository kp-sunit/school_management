

<link  href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link  href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
<?php
   include_once("./includes/session.php");
   
   //include_once("includes/config.php");
   
   include_once("./includes/config.php");
   
   $url = basename(__FILE__) . "?" . (isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : 'cc=cc');
   
   if (isset($_GET['action']) && $_GET['action'] == 'inactive') {
       $item_id = $_GET['cid'];
   
       $deleteQry = "UPDATE `exammanage_admin` set `sattus`='0' WHERE `id` = '" . mysql_real_escape_string($item_id) . "'";
       mysql_query($deleteQry);
       header('Location:list_teacher.php');
       exit();
   }
   
   
   if (isset($_GET['action']) && $_GET['action'] == 'active') {
       $item_id = $_GET['cid'];
   
       $deleteQry = "UPDATE `exammanage_admin` set `sattus`='1' WHERE `id` = '" . mysql_real_escape_string($item_id) . "'";
       mysql_query($deleteQry);
       header('Location:list_teacher.php');
       exit();
   }
   
   
   
   
   if (isset($_GET['action']) && $_GET['action'] == 'delete') {
   
       $item_id = $_GET['cid'];
   
   
   
       $deleteQry = "DELETE FROM `exammanage_teachers` WHERE `id` = '" . mysql_real_escape_string($item_id) . "'";
   
       mysql_query($deleteQry);
   
       header('Location:list_teacher.php');
   
       exit();
   }
   
   
   
   
   
   
   
   if ($_REQUEST['action'] == 'edit') {
   
       $categoryRowset = mysql_fetch_array(mysql_query("SELECT * FROM `exammanage_teachers` WHERE `id`='" . mysql_real_escape_string($_REQUEST['id']) . "'"));
   }
   ?>
<script language="javascript">
   function del(aa, bb)
   
   {
   
       var a = confirm("Are you sure, you want to delete this?")
   
       if (a)
   
       {
   
           location.href = "list_teacher.php?cid=" + aa + "&action=delete"
   
       }
   
   }
   
   
   
   function inactive(aa)
   {
       location.href = "list_teacher.php?cid=" + aa + "&action=inactive"
   }
   
   function active(aa)
   {
       location.href = "list_teacher.php?cid=" + aa + "&action=active";
   }
   
   
   
</script>
<?php include("includes/header.php"); ?>
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
   <!-- BEGIN SIDEBAR -->
   <?php include("includes/left_panel.php"); ?>
   <!-- END SIDEBAR -->
   <!-- BEGIN CONTENT -->
   <div class="page-content-wrapper">
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- /.modal -->
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN STYLE CUSTOMIZER -->
         <!-- END STYLE CUSTOMIZER -->
         <!-- BEGIN PAGE HEADER-->
         <h3 class="page-title"> View Subject </h3>
         <div class="page-bar">
            <ul class="page-breadcrumb">
               <li> <i class="fa fa-home"></i> <a href="index.php">Home</a> <i class="fa fa-angle-right"></i> </li>
               <li> <a href="#">Student list</a> <i class="fa fa-angle-right"></i> </li>
               <!--<li>
                  <a href="#">Editable Datatables</a>
                  
                  </li>-->
            </ul>
         </div>
         <!-- END PAGE HEADER-->
         <!-- BEGIN PAGE CONTENT-->
         <div class="row">
            <div class="col-md-12">
               <div class="portlet box blue">
                  <div class="portlet-title">
                     <div class="caption">
                        <i class="fa fa-gift"></i>Search Student
                     </div>
                     <div class="tools">
                     </div>
                  </div>
                  <div class="portlet-body form">
                     <!-- BEGIN FORM-->
                     <form  class="form-horizontal" method="post" action="" enctype="multipart/form-data" id="filter_form">
                        <div class="form-body">
                           <div class="form-group">
                              <label class="col-md-3 control-label">Select Shift</label>
                              <div class="col-md-5">
                                 <select class="form-control"  name="shift_id" >
                                    <option value=""> Select Shift</option>
                                    <?php
                                       $fetch_shift = mysql_query("select * from `shiftname`  where 1");
                                       
                                       $numshift = mysql_num_rows($fetch_shift);
                                       
                                       if ($numshift > 0) {
                                       
                                           while ($shift = mysql_fetch_array($fetch_shift)) {
                                               ?>
                                    <option <?php
                                       if ($_REQUEST['shift_id'] == $shift['id']) {
                                           echo 'selected';
                                       }
                                       ?> value="<?php echo $shift['id']; ?>"><?php echo $shift['shiftname']; ?></option>
                                    <?php
                                       }
                                       }
                                       ?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-3 control-label">Select Class</label>
                              <div class="col-md-5">
                                 <select class="form-control"  name="class_id" >
                                    <option value=""> Select Class</option>
                                    <?php
                                       $fetch_class = mysql_query("select * from `classname`  where status=1 order by frontorder");
                                       
                                       $numclass = mysql_num_rows($fetch_class);
                                       
                                       if ($numclass > 0) {
                                       
                                           while ($class = mysql_fetch_array($fetch_class)) {
                                               ?>
                                    <option <?php
                                       if ($_REQUEST['class_id'] == $class['id']) {
                                           echo 'selected';
                                       }
                                       ?> value="<?php echo $class['id']; ?>"><?php echo $class['classname']; ?></option>
                                    <?php
                                       }
                                       }
                                       ?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-3 control-label">Select Section</label>
                              <div class="col-md-5">
                                 <select class="form-control"  name="section_id" >
                                    <option value=""> Select Section</option>
                                    <?php
                                       $fetch_section = mysql_query("select * from `sectionname`  where 1");
                                       
                                       $numsection = mysql_num_rows($fetch_section);
                                       
                                       if ($numsection > 0) {
                                       
                                           while ($section = mysql_fetch_array($fetch_section)) {
                                               ?>
                                    <option <?php
                                       if ($_REQUEST['section_id'] == $section['id']) {
                                           echo 'selected';
                                       }
                                       ?> value="<?php echo $section['id']; ?>"><?php echo $section['sectionname']; ?></option>
                                    <?php
                                       }
                                       }
                                       ?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group" style="display:none;">
                              <label class="col-md-3 control-label">Group</label>
                              <div class="col-md-5">
                                 <select class="form-control" name="group">
                                    <option value="">Select</option>
                                    <?php
                                       //Query to get the Designation list
                                       
                                                                                       $sql_designation = "SELECT * FROM school_groupstudent WHERE name <> ''";
                                       
                                                                                       $res_designation = mysql_query($sql_designation);
                                       
                                       
                                       
                                                                                       if (mysql_num_rows($res_designation) > 0) {
                                       
                                                                                           while ($row_designation = mysql_fetch_array($res_designation)) {
                                                                                               ?>
                                    <option <?php
                                       if ($_REQUEST['group'] == $row_designation['id']) {
                                           echo 'selected';
                                       }
                                       ?> value="<?php echo $row_designation['id']; ?>"><?php echo $row_designation['name']; ?></option>
                                    <?php
                                       }
                                       }
                                       ?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                           </div>
                        </div>
                        <div class="form-actions fluid">
                           <div class="row">
                              <div class="col-md-offset-3 col-md-9">
                                 <button type="submit" class="btn blue"  name="submit" id="sbmit_btn">Submit</button>
                              </div>
                           </div>
                        </div>
                     </form>
                     <!-- END FORM-->
                  </div>
               </div>
            </div>
         </div>
         <?php if (isset($_REQUEST['submit'])) { 
            if(!empty($_REQUEST['class_id']))
            {
                $search_class=mysql_fetch_assoc(mysql_query("select * from `classname`  where id='".$_REQUEST['class_id']."'"));
            }
            if(!empty($_REQUEST['section_id']))
            {
                $search_section=mysql_fetch_assoc(mysql_query("select * from `sectionname`  where id='".$_REQUEST['section_id']."'"));
            }
            if(!empty($_REQUEST['shift_id']))
            {
                $search_shift=mysql_fetch_assoc(mysql_query("select * from `shiftname`  where id='".$_REQUEST['shift_id']."'"));
                
            }
            
            ?>
         <div class="row">
            <div class="col-md-12">
               <!-- BEGIN EXAMPLE TABLE PORTLET-->
               <div class="portlet box blue">
                  <div class="portlet-title">
                     <div class="caption">
                        View Subject
                        <!--<i class="fa fa-edit"></i>Editable Table-->
                     </div>
                  </div>
                  <div class="portlet-body">
                     <div class="table-toolbar">
                        <div class="row">
                        </div>
                     </div>
                     <form name="bulk_action_form" action="" method="post" onsubmit="return deleteConfirm();" >
                        <div class="row">
                           <div class="col-md-12 text-center">
                              <?php if ($rowadmin['image'] == '') {
                                 ?>
                              <img src=""  alt="logo" class="logo-default" style="height:25px; width:100px;" />
                              <?php
                                 } else {
                                     $image_link = SITE_URL . 'upload/documents/' . $rowadmin['image'];
                                     ?>
                              <img src="<?php echo $image_link ?>" alt="logo" class="logo-default" style="height:25px;" />
                              <?php } ?>
                              <strong><?php echo $rowpro['institution'] ?></strong>
                           </div>
                        </div>
                        <table class="table table-striped table-hover" style="" id="filter_tbl">
                           <tr >
                               <td>Shift<span style=" font-weight:bold;">-<?php echo !empty($search_shift)?$search_shift['shiftname']:"N/A" ?></span>   |     Class<span style="font-weight:bold;">-<?php echo !empty($search_class)?$search_class['classname']:"N/A" ?></span>  |    Section<span style="font-weight:bold;">-<?php echo !empty($search_section)?$search_section['sectionname']:"N/A" ?></span></td>
                           </tr>
                        </table>
                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                           <thead>
                              <tr>
                                 <th>ID</th>
                                 <th>Photo</th>
                                 <th>Roll</th>
                                 <th>Name</th>
                                 <th>Date of Birth/ <br />
                                    Gender/<br>
                                    Religion
                                 </th>
                                 <th>Shift/ <br />
                                    Class/<br>
                                    Section
                                 </th>
                                 <th>Group/  <br />
                                    Session/<br>
                                    Type
                                 </th>
                                 <th>Subject Code</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tfoot>
                              <tr>
                                 <th>ID</th>
                                 <th></th>
                                 <th>Roll</th>
                                 <th>Roll</th>
                                 <th>Date of Birth/ <br />
                                    Gender/<br>
                                    Religion
                                 </th>
                                 <th>Shift/ <br />
                                    Class/<br>
                                    Section
                                 </th>
                                 <th>Group/  <br />
                                    Session/<br>
                                    Type
                                 </th>
                                 <th>Subject Code</th>
                                 <th></th>
                              </tr>
                           </tfoot>
                           <tbody>
                              <?php
                                 $sql = "select * from school_students   where 1 and is_deleted=0";
                                 
                                 if ($_REQUEST['class_id'] != '') {
                                     $sql .= " and class_id = '" . $_REQUEST['class_id'] . "'";
                                 }
                                 
                                 if ($_REQUEST['section_id'] != '') {
                                     $sql .= " and section_id = '" . $_REQUEST['section_id'] . "'";
                                 }
                                 if ($_REQUEST['shift_id'] != '') {
                                     $sql .= " and shiftid = '" . $_REQUEST['shift_id'] . "'";
                                 }
                                 
                                 if ($_REQUEST['group'] != '') {
                                     $sql .= " and group = '" . $_REQUEST['group'] . "'";
                                 }
                                 
                                 $fetch_product = mysql_query($sql);
                                 $num = mysql_num_rows($fetch_product);
                                 
                                 
                                 if ($num > 0) {
                                     ?>
                              <?php
                                 while ($product = mysql_fetch_array($fetch_product)) {
                                 
                                 
                                     $rowimage = mysql_fetch_array(mysql_query("SELECT `image`,`email`,`mobile` FROM `school_admin` WHERE `id`='" . mysql_real_escape_string($product['userid']) . "'"));
                                 
                                 
                                 
                                     if ($rowimage['image'] != '') {
                                         $image_link = 'upload/documents/' . $rowimage['image'];
                                     } else {
                                         $image_link = 'upload/no.png';
                                     }
                                 
                                 
                                     $rowshift = mysql_fetch_array(mysql_query("SELECT `shiftname` FROM `shiftname` WHERE `id`='" . mysql_real_escape_string($product['shiftid']) . "'"));
                                 
                                     $rowclass = mysql_fetch_array(mysql_query("SELECT `classname` FROM `classname` WHERE `id`='" . mysql_real_escape_string($product['class_id']) . "'"));
                                 
                                     $rowsection = mysql_fetch_array(mysql_query("SELECT `sectionname` FROM `sectionname` WHERE `id`='" . mysql_real_escape_string($product['section_id']) . "'"));
                                 
                                     $rowgroup = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_groupstudent` WHERE `id`='" . mysql_real_escape_string($product['group']) . "'"));
                                     $rowreligion = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_religion` WHERE `id`='" . mysql_real_escape_string($product['religion']) . "'"));
                                     $rowsession = mysql_fetch_array(mysql_query("SELECT `scheme` FROM `school_exam_scheme` WHERE `id`='" . mysql_real_escape_string($product['session_id']) . "'"));
                                     $rowstudenttype = mysql_fetch_array(mysql_query("SELECT `name` FROM `school_studenttype` WHERE `id`='" . mysql_real_escape_string($product['studenttype']) . "'"));
                                 //                                                echo "select * from school_assign_subject where student_primary_id='".$product['id']."'";
                                 //                                                exit;
                                     $row_school_assign_subject=mysql_fetch_assoc(mysql_query("select * from school_assign_subject where student_primary_id='".$product['id']."'"));
                                 //                                                echo "<pre>";
                                 //                                                print_r($row_school_assign_subject);
                                 //                                                exit;
                                     $select_subjects=array();
                                     $assign_subjects= array();
                                     if(!empty($row_school_assign_subject))
                                     {
                                         if($row_school_assign_subject["group_subject_id"])
                                         {
                                             $assign_subjects[]=$row_school_assign_subject["group_subject_id"];
                                         }
                                         if($row_school_assign_subject["religion_subject_id"])
                                         {
                                             $assign_subjects[]=$row_school_assign_subject["religion_subject_id"];
                                         }
                                         if($row_school_assign_subject["thirdfourth_subject_id"])
                                         {
                                             $assign_subjects[]=$row_school_assign_subject["thirdfourth_subject_id"];
                                         }
                                         if($row_school_assign_subject["fourth_subject_id"])
                                         {
                                             $assign_subjects[]=$row_school_assign_subject["fourth_subject_id"];
                                         }
                                          $assign_subjects=implode(",",$assign_subjects);
                                        
                                        
                                         $fetch_subject=mysql_query("select * from `allsubject`  where id in(".$assign_subjects.")") or die(mysql_error());
                                         while($row_subject=mysql_fetch_assoc($fetch_subject))
                                         {
                                             $select_subjects[]=$row_subject['subjectcode'];
                                         }
                                        
                                         
                                     }
                                     else{
                                         $select_subjects="";
                                     }
                                     
                                     
                                     
                                     
                                     ?>
                              <tr>
                                 <td><?php echo $product["id"]; ?></td>
                                 <td><img src="<?php echo $image_link ?>" alt="" style="width:75px; height:75px;" /></td>
                                 <td><?php echo stripslashes($product['roll']); ?></td>
                                 <td><?php echo stripslashes($product['name']); ?></td>
                                 <td>
                                    <?php echo stripslashes($product['dob']); ?>/<br>
                                    <?php echo stripslashes($product['gender']); ?>/<br>
                                    <?php echo $rowreligion['name'] ?>
                                 </td>
                                 <td><?php echo stripslashes($rowshift['shiftname']); ?> / <br><?php echo stripslashes($rowclass['classname']); ?>/<br><?php echo stripslashes($rowsection['sectionname']); ?></td>
                                 <td><?php echo stripslashes($rowgroup['name']); ?> / <br /><?php echo stripslashes($rowsession['scheme']); ?>/<br/><?php echo stripslashes($rowstudenttype['name']); ?></td>
                                 <td><?php echo implode(",",$select_subjects); ?></td>
                                 <td>
                                    <a href="edit_assign_subject.php?id=<?php echo $product['id'] ?>&action=edit">Edit </a>   <br />
                                    <a href="view_assign_subject.php?id=<?php echo $product['id']; ?>&action=view">View </a> &nbsp;
                                 </td>
                              </tr>
                              <?php
                                 }
                                 
                                 
                                 
                                 
                                 } else {
                                 ?>
                              <tr>
                                 <td colspan="4">Sorry, no record found.</td>
                              </tr>
                              <?php
                                 }
                                 ?>
                           </tbody>
                        </table>
                        <a href="javascript:void(0)" class="btn blue" id="print_btn">Print</a>
                        <a href="javascript:void(0)" class="btn blue" id="download_btn">PDF</a>
                        <a href="javascript:void(0)" class="btn blue" id="excel_btn">EXCEL</a>
                     </form>
                  </div>
               </div>
               <!-- END EXAMPLE TABLE PORTLET-->
            </div>
         </div>
         <?php } ?>
         <!-- END PAGE CONTENT -->
      </div>
   </div>
   <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<div class="page-footer">
   <?php include("includes/footer.php"); ?>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>
<script>
   jQuery(document).ready(function () {
   
       Metronic.init(); // init metronic core components
   
       Layout.init(); // init current layout
   
       QuickSidebar.init(); // init quick sidebar
   
       Demo.init(); // init demo features
   
   //   TableEditable.init();
   $("#download_btn").click(function(){
   $("#filter_form").attr("action","download_studentsubject.php");  
   $("#sbmit_btn").trigger("click");
   })
   
   $("#print_btn").click(function(){
   $("#filter_form").attr("action","print_studentsubject.php");
   $("#sbmit_btn").trigger("click");
   });
   $("#excel_btn").click(function(){
   $("#filter_form").attr("action","excel_studentsubject.php");
   $("#sbmit_btn").trigger("click");
   });
   });
   
</script>
<script type="text/javascript">
   function deleteConfirm() {
   
       var result = confirm("Are you sure to delete product?");
   
       if (result) {
   
           return true;
   
       } else {
   
           return false;
   
       }
   
   }
   
   
   
   $(document).ready(function () {
   
   
       var table=$('#sample_editable_1').DataTable({
           dom: 'Bfrtip',
           buttons: [
               {
                   extend: 'excelHtml5',
                   messageTop: null,
                   filename: 'Data export',
                   exportOptions: {
                       columns: [0, 1, 2, 3, 4, 5, 6,7]
                   }
               },
               {
                   extend: 'pdfHtml5',
                   messageTop: null,
                   filename: 'Data export',
                   exportOptions: {
                       columns: [0, 1, 2, 3, 4, 5, 6,7]
                   }
               },
           ],
           lengthMenu: [
               [5, 15, 20, -1],
               [5, 15, 20, "All"] // change per page values here
   
           ],
       });
       $('.dataTables_filter').find('input').addClass('form-control input-small input-inline');
            $('#sample_editable_1 tfoot th').each( function () {
                if($(this).text()!='')
                {
               var title = $(this).text();
               $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
           }
   } );
   
   
       table.columns().every( function () {
           var that = this;
   
           $( 'input', this.footer() ).on( 'keyup change', function () {
               if ( that.search() !== this.value ) {
                   that
                       .search( this.value )
                       .draw();
               }
           } );
       } );
       $('.selectradio').click(function () {
           if ($(this).val() == 'eiin') {
               $('#show_eiin').show();
           } else {
   
               $('#show_eiin').hide();
   
           }
   
   
   
   
   
       });
   
   
       $('#select_all').on('click', function () {
   
           if (this.checked) {
   
               $('.checkbox').each(function () {
   
                   this.checked = true;
   
               });
   
           } else {
   
               $('.checkbox').each(function () {
   
                   this.checked = false;
   
               });
   
           }
   
       });
   
   
   
       $('.checkbox').on('click', function () {
   
           if ($('.checkbox:checked').length == $('.checkbox').length)
   
           {
   
               $('#select_all').prop('checked', true);
   
           } else {
   
               $('#select_all').prop('checked', false);
   
           }
   
       });
   
   });
   
   
   
   
   
   
   
</script>
<script>
   $(document).ready(function () {
   
       $(".san_open").parent().parent().addClass("active open");
   
   });
   
</script>
<style type="text/css">
   tfoot {
   display: table-header-group;
   }
   tfoot input {
   width: 100%;
   padding: 6px;
   box-sizing: border-box;
   font-size: 12px;
   }
   #filter_tbl{border:1px solid #000 !important}
   #filter_tbl tr td {
   border-top:1px solid #000 !important;
   }
</style>
</body><!-- END BODY -->
</html>

