<?php
ob_start();
?>
<link  href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link  href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
<?php
include_once("./includes/session.php");

//include_once("includes/config.php");

include_once("./includes/config.php");

$url = basename(__FILE__) . "?" . (isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : 'cc=cc');


if(isset($_REQUEST['adddata']))
{
    $montharr=!empty($_REQUEST['montharr'])?$_REQUEST['montharr']:'';
   
    foreach ($_REQUEST['montharr'] as $key=>$month)
    {
        $year=!empty($_REQUEST['yearval'])?$_REQUEST['yearval']:'';  
        $dval=cal_days_in_month(CAL_GREGORIAN,$month,$year);
        $working_day= array();
        for($count=1;$count<=$dval;$count++)
        {
            $date =$year."-".sprintf("%02d",$month)."-".sprintf("%02d",$count);
            if(!empty($_REQUEST[$date]))
            {
                $working_day[$date]= 1;
            }
            else
            {
                $working_day[$date]= 0;
            }
        }
        $check_all=!empty($_REQUEST["check_all".$year.'_'.$month])?1:0;
        mysql_query("insert into school_schooldays set select_year='".$year."',select_month='".$month."',working_day='".json_encode($working_day)."',check_all='".$check_all."'") or die(mysql_error());
        

        
        
    }
  header("Location:view_schoolday.php");
    
   

}








?>
<script language="javascript">

    function del(aa, bb)

    {

        var a = confirm("Are you sure, you want to delete this?")

        if (a)

        {

            location.href = "view_student.php?cid=" + aa + "&action=delete&parentid="+bb;

        }

    }



    function inactive(aa)
    {
        location.href = "list_teacher.php?cid=" + aa + "&action=inactive"
    }

    function active(aa)
    {
        location.href = "list_teacher.php?cid=" + aa + "&action=active";
    }



</script>
<?php include("includes/header.php"); ?>

<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php include("includes/left_panel.php"); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN STYLE CUSTOMIZER -->
            <!-- END STYLE CUSTOMIZER -->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">  School Days  </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li> <i class="fa fa-home"></i> <a href="index.php">Home</a> <i class="fa fa-angle-right"></i> </li>
                    <li> <a href="#"> School Days </a> <i class="fa fa-angle-right"></i> </li>
                    <!--<li>
                    
                                                                    <a href="#">Editable Datatables</a>
                    
                                                            </li>-->
                </ul>
               
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->

            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>Search 
                            </div>
                            <div class="tools">



                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form  class="form-horizontal" method="post" action="" enctype="multipart/form-data" id="student_filter">


                                <div class="form-body">

                                   	


                               	


                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Year</label>
                                        <div class="col-md-5">

                                            <select class="form-control"  name="year" id="year" required="" >
                                                <option value=""> Select Year</option>
                                                <?php

                                                for($i=2010;$i<=date('Y');$i++)
                                                {
                                             
                                                        ?>
                                    <option <?php
                                                        if ($_REQUEST['year'] == $i) {
                                                            echo 'selected';
                                                        }
                                                        ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>

                                                        <?php
                                                    }
                                                
                                                ?>

                                            </select>
                                        </div>

                                    </div>	


                                               <div class="form-group">
                                        <label class="col-md-3 control-label">Month</label>
                                        <div class="col-md-5">

 
<?php
// set the month array
$formattedMonthArray = array("1" => "January", "2" => "February", "3" => "March", "4" => "April",
                    "5" => "May", "6" => "June", "7" => "July", "8" => "August",
                    "9" => "September", "10" => "October", "11" => "November", "12" => "December",
                );
?>
<!-- displaying the dropdown list -->
<select name="month[]" id="month" required="" class="form-control"  multiple="">
    <?php
    
    $monthArray=$formattedMonthArray;

    foreach ($monthArray as $key=>$month) {
        
        // if you want to select a particular month
       // $selected='';
       $selected = ($key == $_REQUEST['month']) ? 'selected' : '';
        // if you want to add extra 0 before the month uncomment the line below
        //$month = str_pad($month, 2, "0", STR_PAD_LEFT);
        echo '<option '.$selected.' value="'.$key.'">'.$month.'</option>';
    }

    ?>
</select>
                                        </div>

                                    </div>  		






                                    <div class="form-group">

                                    </div>

                                </div>

                                <div class="form-actions fluid">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn blue" disabled=""  name="submit" id="submit_btn">Submit</button>

                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <?php if (isset($_REQUEST['submit'])) { ?>

                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">  School Days 
                               
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-toolbar">
                                    <div class="row">
                                        
                                    </div>
                                </div>
                                <form name="bulk_action_form" action="" method="post" >
                                    <input type="hidden" name="adddata" value="1">
                                    <input type="hidden" name="yearval" value="<?php echo $_REQUEST['year']; ?>">
                                 
                                    <div class="table-scrollable">
                                        <table class="table table-striped table-hover table-bordered">
                                            <tr>
                                    
                                                 <?php 
                                                for($m=0;$m<count($_REQUEST['month']);$m++) 
                                                    { 
                                                        ?>
                            <input type="hidden" name="montharr[]" value="<?php echo $_REQUEST['month'][$m]; ?>">
                         
                                                        <td>
                                                        <table class="table table-striped table-hover table-bordered">
                                <tr><th colspan="3"><?php echo  $formattedMonthArray[$_REQUEST['month'][$m]]; ?></th></tr>
                                <tr> 
                                    <td>Date</td> 
                                    <td>Day</td> 
                                    <td><input type="checkbox" name="check_all<?php echo $_REQUEST['year'].'_'.$_REQUEST['month'][$m] ?>" value="1" class="check_all" id="<?php echo $_REQUEST['year'].'_'.$_REQUEST['month'][$m] ?>"></td>
                                </tr>

                                <?php   
                                $dval=cal_days_in_month(CAL_GREGORIAN,$_REQUEST['month'][$m],$_REQUEST['year']);
                                for($ms=1;$ms<=$dval;$ms++) 
                                {

                                    $date =$_REQUEST['year']."-".sprintf("%02d",$_REQUEST['month'][$m])."-".sprintf("%02d",$ms);
                                    $unixTimestamp = strtotime($date);
                                    $dayOfWeek = date("l", $unixTimestamp);
                                ?>

                                <tr>            
                                    <td><?php echo $ms ?></td> <td><?php echo $dayOfWeek; ?></td> 
                                    <td>
                                        <input type="checkbox" class="<?php echo $_REQUEST['year'].'_'.$_REQUEST['month'][$m].'chk'; ?> datecheckbx" name="<?php echo $date; ?>" value="1">
                                    </td>
                                </tr>
                                <?php } ?>
                                                            
                                                        </table>
                                                    </td>
                                               
                                                <?php } ?>
                                            </tr>
                                                </table>
                                    <input type="submit" class="btn blue"  value="Submit" style=" float:right; margin-bottom:8px;"/>
                                </div>
                                     
                                </form>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>

            <?php } ?>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<div class="page-footer">
    <?php include("includes/footer.php"); ?>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>

<script src="assets/global/plugins/respond.min.js"></script>

<script src="assets/global/plugins/excanvas.min.js"></script> 

<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<!--<script src="assets/admin/pages/scripts/table-editable.js"></script>-->

<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>
<script>

jQuery(document).ready(function () {

    Metronic.init(); // init metronic core components

    Layout.init(); // init current layout

    QuickSidebar.init(); // init quick sidebar

    Demo.init(); // init demo features

   // TableEditable.init();

});

$('.delete_all').on('click', function(e) { 
var allVals = [];  
var allValspar = [];  
$(".sub_chk:checked").each(function() {  
allVals.push($(this).attr('data-id'));
allValspar.push($(this).attr('data-parent'));
});  
//alert(allVals.length); return false;  
if(allVals.length <=0)  
{  
alert("Please select row."); 
return false; 
} 

location.href = "view_student.php?cid=" + allVals + "&action=deletemulti&parentid="+allValspar;

}); 

</script>
<script type="text/javascript">

    function deleteConfirm() {

        var result = confirm("Are you sure to delete product?");

        if (result) {

            return true;

        } else {

            return false;

        }

    }



    $(document).ready(function () {



        var table = $('#sample_editable_1').DataTable( {


        dom: 'Bfrtip',

       buttons: [
            
//            {
//                extend: 'excelHtml5',
//                 messageTop: null,
//                  filename: 'Data export',
//                exportOptions: {
//                    columns: [ 1,2,3,4,5,6,7,8,9,10 ]
//                }
//            },
            {
                extend: 'pdfHtml5',
                messageTop: null,
                 filename: 'Data export',
                exportOptions: {
                    columns: [ 1,2,3,4,5,6,7,8,9,10 ]
                }
            },
        ],
        lengthMenu: [

                [5, 15, 20, -1],

                [5, 15, 20, "All"] // change per page values here

            ],

           
    } );

           $('.dataTables_filter').find('input').addClass('form-control input-small input-inline');
           $('#sample_editable_1 tfoot th').each( function () {
           var title = $(this).text();
          $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });


             table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


             $('#year').change(function(){


       $.ajax({
   
           type: "post",
   
           url: "ajax_monthcheck.php",
           dataType: "json",
   
           data: {year: $(this).val()},
   
           success: function (msg) {
   var myJSON = JSON.stringify(msg);

  var newj= JSON.parse(myJSON);
  


   $.each(newj, function (index, value) {
$("#month option[value='"+value+"']").remove();
  
 
});
   
        if(newj.length==12)
                {

        $('#month').html("<option value=''>You have already selected all the values</option>");


                }


      $('#submit_btn').prop("disabled", false);          
   
           }
   
       });

             });




           $('.dataTables_filter').find('input').addClass('form-control input-small input-inline');

           $(".download_excel").click(function(){
           $("#student_filter").attr("action","student_excel.php");  
           $("#submit_btn").trigger("click");
           });
        $('.selectradio').click(function () {
            if ($(this).val() == 'eiin') {
                $('#show_eiin').show();
            } else {

                $('#show_eiin').hide();

            }





        });


        $('#select_all').on('click', function () {

            if (this.checked) {

                $('.checkbox').each(function () {

                    this.checked = true;

                });

            } else {

                $('.checkbox').each(function () {

                    this.checked = false;

                });

            }

        });



        $('.checkbox').on('click', function () {

            if ($('.checkbox:checked').length == $('.checkbox').length)

            {

                $('#select_all').prop('checked', true);

            } else {

                $('#select_all').prop('checked', false);

            }

        });
        $(".check_all").click(function(){
        chkbxid=$(this).attr("id");
        if(this.checked)
        {
            $("."+chkbxid+"chk").prop("checked",true);
            $("."+chkbxid+"chk").parent().addClass("checked");
        }
        else
        {
           $("."+chkbxid+"chk").prop("checked",false);
           $("."+chkbxid+"chk").parent().removeClass("checked");

        }
        
        });
        $(".datecheckbx").click(function(){
         string=$(this).attr("class");
         $current_class=string.replace('datecheckbx', '').trim(); 
         $total_checked=parseInt($('input.'+$current_class+':checked').length)
         $totalbox=parseInt($('input.'+$current_class+'').length);
         if(this.checked)
         {
             if($totalbox==$total_checked)
             {
                 $all_chkbx_id=$current_class.replace("chk",'').trim();
                 $("#"+$all_chkbx_id).prop("checked",true);
                 $("#"+$all_chkbx_id).parent().addClass("checked");
             }
         }
         else
         {
             $all_chkbx_id=$current_class.replace("chk",'').trim();
             $("#"+$all_chkbx_id).prop("checked",false);
             $("#"+$all_chkbx_id).parent().removeClass("checked");
         }
        });

    });







</script>
<script>



    $(document).ready(function () {

        $(".san_open").parent().parent().addClass("active open");

    });

</script>

<style type="text/css">
 tfoot {
    display: table-header-group;
}
    tfoot input {
        width: 100%;
        padding: 6px;
        box-sizing: border-box;
        font-size: 12px;
    }
</style>
</body><!-- END BODY -->
</html>