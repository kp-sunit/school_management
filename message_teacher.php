<?php
include_once("./includes/session.php");

//include_once("includes/config.php");

include_once("./includes/config.php");

$url = basename(__FILE__) . "?" . (isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : 'cc=cc');
?>

<style type="text/css">
    .modal-header{
        background: #3598dc;
    }
    .modal-title{
        border-bottom: 0 none;
        color: rgb(255, 255, 255);
        margin-bottom: 0;
        padding: 0 10px;
    }
    .modal_sms{
        border: 1px solid rgb(229, 229, 229);
    }
    .sms_message{
        border: 1px solid rgb(229, 229, 229);
        width: 100%;
    }
</style>

<?php include("includes/header.php"); ?>

<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php include("includes/left_panel.php"); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title"> Teacher </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li> <i class="fa fa-home"></i> <a href="index.php">Home</a> <i class="fa fa-angle-right"></i> </li>
                    <li> <a href="#">Teacher list</a> <i class="fa fa-angle-right"></i> </li>
                </ul>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>Search Teacher
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form  class="form-horizontal" method="post" action="" enctype="multipart/form-data">


                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Designation</label>
                                        <div class="col-md-5">

                                            <select class="form-control"  name="designation" >
                                                <option value=""> Select Designation</option>
                                                <?php
                                                $fetch_class = mysql_query("select * from `school_designation`  where 1 order by `id`");

                                                $numclass = mysql_num_rows($fetch_class);

                                                if ($numclass > 0) {

                                                    while ($class = mysql_fetch_array($fetch_class)) {
                                                        ?>
                                                        <option <?php
                                                        if ($_REQUEST['designation'] == $class['id']) {
                                                            echo 'selected';
                                                        }
                                                        ?> value="<?php echo $class['id']; ?>"><?php echo $class['name']; ?></option>

                                                        <?php
                                                    }
                                                }
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Name</label>
                                        <div class="col-md-5">
                                            <input type="text" name="name" class="form-control" value="<?php echo $_REQUEST['name']; ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Mobile</label>
                                        <div class="col-md-5">
                                            <input type="text" name="mobile" class="form-control" value="<?php echo $_REQUEST['mobile']; ?>" />
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                    </div>
                                </div>

                                <div class="form-actions fluid">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn blue"  name="submit">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <?php if (isset($_REQUEST['submit'])) { ?>


                <?php
                //	$fetch_product=mysql_query("select * from exammanage_teachers  where `status`='1'");

                /* $fetch_product = mysql_query("select * from exammanage_teachers  where 1 eiin = '".$_SESSION['username']."' and status = '2'"); */
                $sql = "select * from school_teachers   where 1";

                if ($_REQUEST['designation'] != '') {
                    $sql .= " and designation = '" . $_REQUEST['designation'] . "'";
                }

                if ($_REQUEST['name'] != '') {
                    $sql .= " AND (fname LIKE '%" . $_REQUEST['name'] . "%' OR mname LIKE '%" . $_REQUEST['name'] . "%' OR lname LIKE '%" . $_REQUEST['name'] . "%' )";
                }
                if ($_REQUEST['mobile'] != '') {
                    $sql .= " AND mobile LIKE '%" . $_REQUEST['mobile'] . "%'";
                }
//                if ($_REQUEST['name'] != '') {
//                    $sql .= " and shiftid = '" . $_REQUEST['shift_id'] . "'";
//                }
                //ECHO $sql;
                $fetch_product = mysql_query($sql);
                $num = mysql_num_rows($fetch_product);
                ?>

                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"> Teacher
                                <!--<i class="fa fa-edit"></i>Editable Table-->
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-toolbar">
                                    <div class="row">                                       
                                    </div>
                                </div>
                                <form name="bulk_action_form" action="" method="post" onsubmit="return deleteConfirm();" >
                                    <div class="hide search_query"><?php echo $sql; ?></div>
                                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                        <?php if ($num > 0) { ?> <tr><td align="center" colspan="32"><input type="checkbox" name="select_all"  id="select_all" value=""/>Check All</td></tr> <?php } ?>
                                        <thead>
                                            <tr>
                                                <th>Name</th>                                               
                                                <th>Designation</th>                                               
                                                <th>Select Teachers</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($num > 0) {

                                                while ($product = mysql_fetch_array($fetch_product)) {

                                                    $designationInfo = mysql_fetch_array(mysql_query("select * from `school_designation`  where 1 and id ='" . $product['designation'] . "'"));
                                                    ?>
                                                    <tr>

                                                        <td><?php echo stripslashes($product['fname']) . ' ' . stripslashes($product['mname']) . ' ' . stripslashes($product['lname']); ?></td>
                                                        <td><?php echo $designationInfo['name']; ?></td>
                                                        <td>
            <!--                                                            <input type="checkbox" name="selected_values[]" class="selectcheckBox" value="<?php echo $product['user_id']; ?>" checked="checked"   />                                                        </td>-->
                                                            <input type="checkbox" name="selected_values[]" class="selectcheckBox" value="<?php echo $product['user_id']; ?>"    />                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td colspan="4">Sorry, no record found.</td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <!--<input type="submit" class="btn btn-danger" name="bulk_delete_submit" value="Delete"/>-->
                                </form>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>

                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form  class="form-horizontal" method="post" action="" enctype="multipart/form-data">				
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-12">
                                    <button type="button" class="btn blue open_message_modal" data-toggle="modal" data-target="#myModal">Send SMS</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            <?php } ?>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<div class="page-footer">
    <?php include("includes/footer.php"); ?>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>

<script src="assets/global/plugins/respond.min.js"></script>

<script src="assets/global/plugins/excanvas.min.js"></script> 

<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/table-editable.js"></script>
<script>

                                jQuery(document).ready(function () {

                                    Metronic.init(); // init metronic core components

                                    Layout.init(); // init current layout

                                    QuickSidebar.init(); // init quick sidebar

                                    Demo.init(); // init demo features

                                    //TableEditable.init();

                                    $('#sample_editable_1').dataTable({
                                        aLengthMenu: [
                                            [25, 50, 100, 200, -1],
                                            [25, 50, 100, 200, "All"]
                                        ],
                                        iDisplayLength: -1
                                    });

                                });

</script>
<script type="text/javascript">


    $(document).ready(function () {
        $('#select_all').on('click', function () {

            if (this.checked) {

                //$('.checkbox').each(function () {
                $('.selectcheckBox').each(function () {
                    this.checked = true;
                    $(this).attr('checked', 'checked');
                    $(this).closest('span').addClass('checked');
                });
            } else {
                $('.selectcheckBox').each(function () {
                    this.checked = false;
                    $(this).removeAttr('checked').closest('span').removeClass('checked');
                });
            }
        });




        $(document).on('click', '.btn_send_sms', function (event) {
            event.preventDefault();
            var message = $('.sms_message').val();
            var search_query = $('.search_query').text();
            var selected_values = '';
            selected_values = $(".selectcheckBox:checked").map(function () {
                return this.value;
            }).get().join(",");
            if (message.trim().length <= 0) {
                alert('Please type your message before send !!!');
                return false;
            }
            if (selected_values == '') {
                alert('Please select checkbox to send SMS !!!');
                return false;
            }
            $.ajax({
                url: "ajax_send_sms.php",
                type: "POST",
                dataType: "json",
                data: {
                    message: message,
                    search_query: search_query,
                    selected_values: selected_values,
                    action: 'send_sms_to_teachers'
                },
                success: function (data) {
                    if (data.error == '0') {
                        $('.sms_message').val('');
                        alert('Sms succssfully sent to teachers !!!');
                    }
                }
            });

        });
    });





    $(document).ready(function () {
        $(".san_open").parent().parent().addClass("active open");
    });

</script>


<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Type SMS Here</h4>
            </div>
            <div class="modal-body">
                <textarea name="sms_message" class="sms_message" rows="7">                    
                </textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn_send_sms" data-dismiss="modal">Send SMS</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
</body><!-- END BODY -->
</html>