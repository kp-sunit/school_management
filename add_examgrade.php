<?php 
  ob_start();  
  include_once("./includes/session.php");
  //include_once("includes/config.php"); 
   include_once("./includes/config.php");
   $url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
   
   ?>
<?php
   if(isset($_REQUEST['submit']))
   
   {
   
   	$name = isset($_POST['name']) ? $_POST['name'] : '';
   	$grade_point = isset($_POST['grade_point']) ? $_POST['grade_point'] : '';
   	$marks_from = isset($_POST['marks_from']) ? $_POST['marks_from'] : '';
   	$marks_upto = isset($_POST['marks_upto']) ? $_POST['marks_upto'] : '';
   	$id = isset($_POST['id']) ? $_POST['id'] : '';
   	$comment = isset($_POST['comment']) ? $_POST['comment'] : '';
   	$order = isset($_POST['order']) ? $_POST['order'] : '';
   	$fields = array(
   
   		'name' => mysql_real_escape_string($name),
   		'grade_point'=> mysql_real_escape_string($grade_point),
   		'marks_from'=> mysql_real_escape_string($marks_from),
   		'marks_upto' => mysql_real_escape_string($marks_upto),
   		'comment' => mysql_real_escape_string($comment),
   		'order' => mysql_real_escape_string($order),
   		);
   		$fieldsList = array();
   
   		foreach ($fields as $field => $value) {
   
   			$fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
   
   		}
   
   	 if($_REQUEST['action']=='edit')
   
   	  {		  
   
   	 $editQuery = "UPDATE `school_grades` SET " . implode(', ', $fieldsList)
   
   			. " WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'";
   
   		//	exit;
   		if (mysql_query($editQuery)) {
   			$_SESSION['msg'] = "Teaching plan Updated Successfully";
   
   		}
   
   		else {
   
   			$_SESSION['msg'] = "Error occuried while updating Teaching plan";
   
   		}
   		header('Location:list_grade.php');
   
   		exit();
   
   	
   
   	 }
   
   	 else
   
   	 {
   	 $addQuery = "INSERT INTO `school_grades` (`" . implode('`,`', array_keys($fields)) . "`)"
   
   			. " VALUES ('" . implode("','", array_values($fields)) . "')";
   
   			
   
   			//exit;
   		mysql_query($addQuery) or die(mysql_error());   
   		header('Location:list_grade.php');
   		exit();
   	 }
  
   } 
   if($_REQUEST['action']=='edit')
   
   {
   
   $categoryRowset = mysql_fetch_array(mysql_query("SELECT * FROM `school_grades` WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'"));
   
   
   
   }
   
   ?>
<?php include('includes/header.php');?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<?php include('includes/left_panel.php');?>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
   <div class="page-content">
      <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <!-- /.modal -->
      <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <!-- BEGIN STYLE CUSTOMIZER -->
      <?php //include('includes/style_customize.php');?>
      <!-- END STYLE CUSTOMIZER -->
      <!-- BEGIN PAGE HEADER-->
      <h3 class="page-title">Exam Grade </h3>
      <div class="page-bar">
         <ul class="page-breadcrumb">
            <li>
               <i class="fa fa-home"></i>
               <a href="dashboard.php">Home</a>
               <i class="fa fa-angle-right"></i>
            </li>
            <li>
               <a href="#">Exam Grade </a>
            </li>
         </ul>
      </div>
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->
      <div class="row">
         <div class="col-md-12">
            <div class="portlet box blue">
               <div class="portlet-title">
                  <div class="caption">
                     <i class="fa fa-gift"></i><?php echo $_REQUEST['action']=='edit'?"Edit":"Add";?> Exam Grade
                  </div>
                  <div class="tools">
                  </div>
               </div>
               <div class="portlet-body form">
                  <!-- BEGIN FORM-->
                  <form  class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                     <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
                     <input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>" />
                     <div class="form-body">
                        <div class="form-group">
                           <label class="col-md-3 control-label">Name</label>
                           <div class="col-md-4">
                               <input type="text" name="name" class="form-control" value="<?php echo $categoryRowset["name"] ?>" required>
                            
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-3 control-label">Grade Point</label>
                            <div class="col-md-4">

                              <input type="text" name="grade_point" class="form-control" value="<?php echo $categoryRowset["grade_point"] ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-3 control-label">Marks From</label>
                           <div class="col-md-4">
                             <input type="text" name="marks_from" class="form-control" value="<?php echo $categoryRowset["marks_from"] ?>" required onkeypress='return ((event.charCode >= 48 && event.charCode <= 57) || event.charCode==0)'>

                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-3 control-label">Marks Upto</label>
                           <div class="col-md-4">
                             <input type="text" name="marks_upto" class="form-control" value="<?php echo $categoryRowset["marks_upto"] ?>" required  onkeypress='return ((event.charCode >= 48 && event.charCode <= 57) || event.charCode==0)'>
                           </div>
                        </div>
                         <div class="form-group">
                           <label class="col-md-3 control-label">Comment</label>
                           <div class="col-md-4">
                              <textarea class="form-control input-lg" name="comment" id="editor1" required><?php echo stripslashes($categoryRowset['comment']); ?></textarea>
                           </div>
                        </div>
                         <div class="form-group">
                           <label class="col-md-3 control-label">Order</label>
                           <div class="col-md-4">
                             <input type="text" name="order" class="form-control" value="<?php echo $categoryRowset["order"] ?>" required onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                           </div>
                        </div>
                        <div class="form-actions fluid">
                           <div class="row">
                              <div class="col-md-offset-3 col-md-9">
                                 <button type="submit" class="btn blue"  name="submit">Submit</button>
                              </div>
                           </div>
                        </div>
                  </form>
                  <!-- END FORM-->
                  </div>
               </div>
            </div>
         </div>
         <!-- END PAGE CONTENT-->
      </div>
   </div>
   <style>
      .thumb{
      height: 60px;
      width: 60px;
      padding-left: 5px;
      padding-bottom: 5px;
      }
   </style>
   <script>
      window.preview_this_image = function (input) {
      
      
      
          if (input.files && input.files[0]) {
      
              $(input.files).each(function () {
      
                  var reader = new FileReader();
      
                  reader.readAsDataURL(this);
      
                  reader.onload = function (e) {
      
                      $("#previewImg").append("<span><img class='thumb' src='" + e.target.result + "'><img border='0' src='../images/erase.png'  border='0' class='del_this' style='z-index:999;margin-top:-34px;'></span>");
      
                  }
      
              });
      
          }
      
      }
      
   </script>
   <!-- END CONTENT -->
   <!-- BEGIN QUICK SIDEBAR -->
   <?php //include('includes/quick_sidebar.php');?>
   <!-- END QUICK SIDEBAR -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include('includes/footer.php'); ?>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/form-samples.js"></script>
<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
   jQuery(document).ready(function() {    
   
      // initiate layout and plugins
   
      Metronic.init(); // init metronic core components
   
   Layout.init(); // init current layout
   
   QuickSidebar.init(); // init quick sidebar
   
   Demo.init(); // init demo features
   
      FormSamples.init();
   
      
   
      
   
      
   
   });
   
   
   
   
       
   
</script>
<script>
   $(document).ready(function(){
   
       $(".san_open").parent().parent().addClass("active open");
   
   });
   
   document.getElementById("firstfield").focus();
   
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>

