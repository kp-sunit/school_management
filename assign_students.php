<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<link  href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link  href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
<?php
include_once("./includes/session.php");
include_once("./includes/config.php");

$url = basename(__FILE__) . "?" . (isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : 'cc=cc');

$teacherClassDetails = mysql_query("SELECT * FROM school_setclassteacher where teacher_id = '" . $_SESSION['myy'] . "'");
while ($teacherDetails = mysql_fetch_assoc($teacherClassDetails)) {
    $class_assigned[] = $teacherDetails['class_id'];
    $section_assigned[] = $teacherDetails['section_id'];
    $shift_assigned[] = $teacherDetails['shift_id'];
}

if (isset($_REQUEST['submitstudent'])) {
     $loopcount = count($_POST['id']);
//     echo "<pre>";
//     print_r($_REQUEST);
//     exit;
    for ($i = 0; $i < $loopcount; $i++) {
        $fields = array(
            'student_primary_id' => mysql_real_escape_string($_POST['id'][$i]),
            'studentid' => mysql_real_escape_string($_POST['studentid'][$i]),
            'group_id' => mysql_real_escape_string($_POST['group'][$i]),
            'group_subject_id' => mysql_real_escape_string($_POST['group_subject'][$i]),
            'religion_subject_id' => mysql_real_escape_string($_POST['allsubject'][$i]),
            'fourth_subject_id' => mysql_real_escape_string($_POST['fourth_subject'][$i]),
            'thirdfourth_subject_id'=>mysql_real_escape_string($_POST['thirdfourth_subject_id'][$i])
        );
        $fieldsList = array();
        foreach ($fields as $field => $value) {
            $fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
        }

         $checkExists = mysql_num_rows(mysql_query("SELECT * FROM `school_assign_subject` "
                        . " WHERE `student_primary_id`='" . $fields['student_primary_id'] . "' "));
        if ($checkExists == 0) {
            $addQuery = "INSERT INTO `school_assign_subject` (`" . implode('`,`', array_keys($fields)) . "`)"
                    . " VALUES ('" . implode("','", array_values($fields)) . "')";

            if (mysql_query($addQuery)) {
                $last_id = mysql_insert_id();
                $_SESSION['msg'] = "Inserted Successfully";
            } else {
                $_SESSION['msg'] = "Data not inserted  !!!";
            }
        } else {
            $editQuery = "UPDATE `school_assign_subject` SET " . implode(', ', $fieldsList)
                    . " WHERE `studentid` = '" . mysql_real_escape_string($fields['studentid']) . "'";
            mysql_query($editQuery);
            $_SESSION['msg'] = "Data Updated Successfully";
        }
    }
    
}
?>
<?php include("includes/header.php"); ?>

<div class="clearfix"> 
</div>
<div class="page-container">
    <?php include("includes/left_panel.php"); ?>
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title"> Student 
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li> 
                        <i class="fa fa-home"> </i> 
                        <a href="index.php">Home</a> 
                        <i class="fa fa-angle-right"></i> 
                    </li>
                    <li> 
                        <a href="#">Assign Student </a> 
                        <i class="fa fa-angle-right"></i> 
                    </li>
                </ul>
            </div>
            <div class="row">
                <?php if ($_SESSION['msg'] != '') {
                    ?>
                    <p style="color:#009900; text-align:center">Data Inserted Successfully.</p>
                    <?php
                    $_SESSION['msg'] = '';
                }
                ?>		
                <div class="col-md-12">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift">
                                </i>Assign Student
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form  class="form-horizontal" method="post" action="" enctype="multipart/form-data" >
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Class
                                        </label>
                                        <div class="col-md-5">
                                            <select class="form-control"  name="class_id" required="" >
                                                <option value=""> select Class
                                                </option>
                                                <?php
                                                $fetch_class = mysql_query("select * from `classname`  where 1 order by `frontorder`");
                                                $numclass = mysql_num_rows($fetch_class);
                                                if ($numclass > 0) {
                                                    while ($class = mysql_fetch_array($fetch_class)) {
                                                        if (in_array($class['id'], $class_assigned)) {
                                                            ?>
                                                            <option 
                                                            <?php
                                                            if ($_REQUEST['class_id'] == $class['id']) {
                                                                echo 'selected';
                                                            }
                                                            ?> value="<?php echo $class['id']; ?>">
                                                                    <?php echo $class['classname']; ?>
                                                            </option>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Section
                                        </label>
                                        <div class="col-md-5">
                                            <select class="form-control"  name="section_id" >
                                                <option value=""> select Section
                                                </option>
                                                <?php
                                                $fetch_section = mysql_query("select * from `sectionname`  where 1");
                                                $numsection = mysql_num_rows($fetch_section);
                                                if ($numsection > 0) {
                                                    while ($section = mysql_fetch_array($fetch_section)) {
                                                        if (in_array($section['id'], $section_assigned)) {
                                                            ?>
                                                            <option 
                                                            <?php
                                                            if ($_REQUEST['section_id'] == $section['id']) {
                                                                echo 'selected';
                                                            }
                                                            ?> value="<?php echo $section['id']; ?>">
                                                                    <?php echo $section['sectionname']; ?>
                                                            </option>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Shift
                                        </label>
                                        <div class="col-md-5">
                                            <select class="form-control"  name="shift_id" >
                                                <option value=""> select Shift
                                                </option>
                                                <?php
                                                $fetch_shift = mysql_query("select * from `shiftname`  where 1");
                                                $numshift = mysql_num_rows($fetch_shift);
                                                if ($numshift > 0) {
                                                    while ($shift = mysql_fetch_array($fetch_shift)) {
                                                        if (in_array($shift['id'], $shift_assigned)) {
                                                            ?>
                                                            <option 
                                                            <?php
                                                            if ($_REQUEST['shift_id'] == $shift['id']) {
                                                                echo 'selected';
                                                            }
                                                            ?> value="<?php echo $shift['id']; ?>">
                                                                    <?php echo $shift['shiftname']; ?>
                                                            </option>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>                                   		
                                    <div class="form-group">
                                    </div>
                                </div>
                                <div class="form-actions fluid">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn blue"  name="submit">Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <?php if (isset($_REQUEST['submit'])) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"> Student </div>
                            </div>
                            <div class="portlet-body" style="padding-bottom:26px;">
                                <div class="table-toolbar">
                                    <div class="row">
                                    </div>
                                </div>
                                <form name="bulk_action_form" action="" method="post" onsubmit= >
                                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                        <?php
                                        if ($_REQUEST['class_id'] != '' || $_REQUEST['section_id'] != '' || $_REQUEST['shift_id'] != '') {
                                            $sql = "select * from school_students   where 1 and is_deleted=0";
                                            if ($_REQUEST['class_id'] != '') {
                                                $sql .= " and class_id = '" . $_REQUEST['class_id'] . "'";
                                            }
                                            if ($_REQUEST['section_id'] != '') {
                                                $sql .= " and section_id = '" . $_REQUEST['section_id'] . "'";
                                            }
                                            if ($_REQUEST['shift_id'] != '') {
                                                $sql .= " and shiftid = '" . $_REQUEST['shift_id'] . "'";
                                            }
                                            $fetch_product = mysql_query($sql);
                                            $num = mysql_num_rows($fetch_product);
                                            
                                            $fetch_subjects_group = mysql_fetch_array(mysql_query("SELECT * FROM `school_subjectgroup` WHERE class_id='" . mysql_real_escape_string($_REQUEST['class_id']) . "'"));
                                            //print_r($fetch_subjects_group);exit;
                                            $fetch_subjects = mysql_query("select * from `allsubject`  where id IN(" . $fetch_subjects_group['religion_subject'] . ")order by `id`");
                                            $numclass_subjects = mysql_num_rows($fetch_subjects);

                                            if ($fetch_subjects_group['is_exists'] == '1') {
                                                //  echo "select * from `school_groupstudent`  where id IN(" . $fetch_subjects_group['allgroup'] . ") order by `id`";
                                                $fetch_group = mysql_query("select * from `school_groupstudent`  where id IN(" . $fetch_subjects_group['allgroup'] . ") order by `id`");
                                                $numclass_group = mysql_num_rows($fetch_group);
                                            } else {
                                                $numclass_group = 0;
                                            }

                                            if ($fetch_subjects_group['is_fourth_exists'] == '1') {
                                                $fetch_4th_subject = mysql_query("select * from `allsubject`  where id IN(" . $fetch_subjects_group['fourth_subject'] . ") order by `id`");
                                                $num_fourhsubject = mysql_num_rows($fetch_4th_subject);
                                            } else {
                                                $num_fourhsubject = 0;
                                            }
                                        } else {
                                            $num = 0;
                                        }
                                        ?>
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Photo</th>
                                                <th>Roll No </th> 
                                                <th>Name </th>
                                                <?php if ($numclass_subjects > 0) { ?>
                                                    <th>Select Religion Subject  </th>
                                                <?php } if ($numclass_group > 0) { ?>
                                                    <th>Select Group    </th>                                             
                                                    <th>Select Group Subject </th>
                                                    <th>Select 3<sup>rd</sup> & 4<sup>th</sup> Subject </th>
                                                <?php } ?>
                                                <?php if ($fetch_subjects_group['is_fourth_exists'] == '1') { ?>
                                                    <th>Select 4th Subject </th>
                                                <?php } ?>
                                                <th>View Profile</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($num > 0) {
                                                $loop_c = 0;
                                                while ($product = mysql_fetch_array($fetch_product)) {

                                                    $fetch_subjects_group = mysql_fetch_array(mysql_query("SELECT * FROM `school_subjectgroup` WHERE class_id='" . mysql_real_escape_string($_REQUEST['class_id']) . "'"));
                                                    $fetch_subjects = mysql_query("select * from `allsubject`  where id IN(" . $fetch_subjects_group['religion_subject'] . ")order by `id`");
                                                    $numclass_subjects = mysql_num_rows($fetch_subjects);

                                                    $assignedStudentDetails = mysql_fetch_assoc(mysql_query("SELECT * FROM school_assign_subject WHERE student_primary_id='" . $product['id'] . "'"));

                                                    if ($fetch_subjects_group['is_exists'] == '1') {
                                                        //  echo "select * from `school_groupstudent`  where id IN(" . $fetch_subjects_group['allgroup'] . ") order by `id`";
                                                    } else {
                                                    }

                                                    if ($fetch_subjects_group['is_fourth_exists'] == '1') {
                                                        $fetch_4th_subject = mysql_query("select * from `allsubject`  where id IN(" . $fetch_subjects_group['fourth_subject'] . ") order by `id`");
                                                        $num_fourhsubject = mysql_num_rows($fetch_4th_subject);
                                                    } else {
                                                        $num_fourhsubject = 0;
                                                    }


                                                     $rowimage = mysql_fetch_array(mysql_query("SELECT `image`,`email`,`mobile` FROM `school_admin` WHERE `id`='" . mysql_real_escape_string($product['userid']) . "'"));



                                                if ($rowimage['image'] != '') {
                                                    $image_link = 'upload/documents/' . $rowimage['image'];
                                                } else {
                                                    $image_link = 'upload/no.png';
                                                }
                                                    ?>
                                                    <tr>
                                                        <td class="hidden_info">
                                                            <input type="hidden" class="primary" name="id[]" value="<?php echo $product['id'] ?>" />                                                       
                                                            <input type="hidden" class="studentid"  name="studentid[]" value="<?php echo $product['studentid'] ?>" />                                                       
                                                            <?php echo stripslashes($product['studentid']); ?>
                                                        </td>
                                                        <td><img src="<?php echo $image_link ?>" alt="" style="width:75px; height:75px;" /></td>
                                                        <td class="rollno">  <?php echo stripslashes($product['roll']); ?> </td>
                                                        <td class="name"><?php echo stripslashes($product['name']); ?> </td> 
                                                        <?php if ($numclass_subjects > 0) { ?>
                                                            <td> 
                                                                <select class="form-control religion_subject"  name="allsubject[]">
                                                                    <?php
                                                                    while ($subjectnew = mysql_fetch_array($fetch_subjects)) {
                                                                        ?>
                                                                        <option value="<?php echo $subjectnew['id']; ?>" <?php if ($assignedStudentDetails['religion_subject_id'] == $subjectnew['id']) { ?> selected <?php } ?>><?php echo $subjectnew['subjectname']; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </td>
                                                        <?php } ?>
                                                        <?php 
                                                        if ($fetch_subjects_group['is_exists']==1) { 
                                                            
                                                            $list_groups=$fetch_subjects_group["allgroup"];
                                                            $sql_groups=mysql_query("select * from `school_new_group`  where id IN(".$list_groups.")") or die(mysql_error());
                                                            
                                                            
                                                            ?>
                                                            <td>
                                                                <select class="form-control group group"  name="group[]">
                                                                    <?php
                                                                    while ($row_groups = mysql_fetch_array($sql_groups)) {
                                                                        ?>
                                                                        <option value="<?php echo $row_groups['id']; ?>" <?php if ($assignedStudentDetails['group_id'] == $row_groups['id']) { ?> selected <?php } ?>><?php echo $row_groups['name']; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </td>
                                                        <?php } ?>
                                                        <?php
                                                        if ($fetch_subjects_group['is_exists']==1) {
                                                        $fetch_group_subject=mysql_query("select * from `allsubject`  where id IN(".$fetch_subjects_group['group_subject'].")");    
                                                                
                                                                
                                                        ?>
                                                                <td>
                                                                    <select class="form-control group_subject"  name="group_subject[]" >
                                                                        <?php
                                                                        while ($new_group_subject = mysql_fetch_array($fetch_group_subject)) {
                                                                        $subject_arr=mysql_fetch_assoc(mysql_query("select * from `allsubject`  where id='".$new_group_subject."'"));   
                                                                        ?>
                                                                            <option value="<?php echo $new_group_subject['id']; ?>"  <?php if ($assignedStudentDetails['group_subject_id'] == $new_group_subject['id']) { ?> selected <?php } ?>><?php echo $new_group_subject['subjectname']; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </td>
                                                                <?php
                                                           
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($fetch_subjects_group['is_exists']==1) {
                                                        $fetch_group_subject=mysql_query("select * from `allsubject`  where id IN(".$fetch_subjects_group['gr_third_fourth_sub'].")");    
                                                              
                                                        ?>
                                                                <td>
                                                                    <select class="form-control thirdfourth_subject_id"  name="thirdfourth_subject_id[]" >
                                                                        <?php
                                                                        while ($new_group_subject = mysql_fetch_array($fetch_group_subject)) {
                                                                        ?>
                                                                            <option value="<?php echo $new_group_subject['id']; ?>"  <?php if ($assignedStudentDetails['thirdfourth_subject_id'] == $new_group_subject['id']) { ?> selected <?php } ?>><?php echo $new_group_subject['subjectname']; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </td>
                                                                <?php
                                                           
                                                        }
                                                        ?>        
                                                        <?php if ($num_fourhsubject > 0) { ?>
                                                            <td>

                                                                <select class="form-control fourth_subject"  name="fourth_subject[]">
                                                                    <?php
                                                                    while ($group_fouth_sub = mysql_fetch_array($fetch_4th_subject)) {
                                                                        ?>
                                                                        <option value="<?php echo $group_fouth_sub['id']; ?>"  <?php if ($assignedStudentDetails['fourth_subject_id'] == $group_fouth_sub['id']) { ?> selected <?php } ?>><?php echo $group_fouth_sub['subjectname']; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>

                                                            </td>
                                                        <?php } ?>
                                                            <td style=" text-align:right;"> 
                                                            <a href="javascript:void(0)" class="assign_this_student">Submit  </a> &nbsp;
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $loop_c++;
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td colspan="7">Sorry, no record found.
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <button type="submit" class="btn blue submitstudent" name="submitstudent" style=" float:right;">All Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js">
</script>
<div class="page-footer">
    <?php include("includes/footer.php"); ?>
</div>
<script src="assets/global/plugins/jquery.min.js" type="text/javascript">
</script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript">
</script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript">
</script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript">
</script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript">
</script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript">
</script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript">
</script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript">
</script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript">
</script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript">
</script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js">
</script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js">
</script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js">
</script>
<script src="assets/global/scripts/metronic.js" type="text/javascript">
</script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript">
</script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript">
</script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript">
</script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js">
</script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js">
</script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js">
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js">
</script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js">
</script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js">
</script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js">
</script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js">
</script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js">
</script>
<script>
    jQuery(document).ready(function () {
        Metronic.init();
        // init metronic core components
        Layout.init();
        // init current layout
        QuickSidebar.init();
        // init quick sidebar
        Demo.init();
        // init demo features
        //   TableEditable.init();

        $('.assign_this_student').click(function () {
            var assign_primary_id = $(this).closest('tr').find('.primary').val();
            var student_primary_id = $(this).closest('tr').find('.studentid').val();
            var religion_subject = $(this).closest('tr').find('.religion_subject').val();
            var group = $(this).closest('tr').find('.group').val();
            var group_subject = $(this).closest('tr').find('.group_subject').val();
            var fourth_subject = $(this).closest('tr').find('.fourth_subject').val();
            var thirdfourth_subject_id = $(this).closest('tr').find('.thirdfourth_subject_id').val();
            $.ajax({
                type: "post",
                dataType: 'json',
                url: "ajax_common.php",
                data: {
                    action: 'assign_single_student',
                    assign_primary_id: assign_primary_id,
                    student_primary_id: student_primary_id,
                    religion_subject: religion_subject,
                    group: group,
                    group_subject: group_subject,
                    fourth_subject: fourth_subject,
                    thirdfourth_subject_id:thirdfourth_subject_id
                },
                success: function (data) {
                    alert(data.msg);
                },error:function(data){
                    alert(data.msg);
                }
            });
        });
    });</script>
<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete product?");
        if (result) {
            return true;
        } else {
            return false;
        }
    }
</script>
<script>
    $(document).ready(function () {
    $(".san_open").parent().parent().addClass("active open");
     $('#sample_editable_1').DataTable({
    dom: 'Bfrtip',
            buttons: [
            {
            extend: 'excelHtml5',
                    messageTop: null,
                    filename: 'Data export',
                    exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5]
                    }
            }, {
            extend: 'pdfHtml5',
                    messageTop: null,
                    filename: 'Data export',
                    exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5]
                    }
            }
            ,
            ],
            lengthMenu: [
            [5, 15, 20, - 1],
            [5, 15, 20, "All"] // change per page values here
            ],
    });
           
//       $('.assign_this_student').click(function () {
//    var assign_primary_id = $(this).closest('tr').find('.primary').val();
//            var student_primary_id = $(this).closest('tr').find('.studentid').val();
//            var religion_subject = $(this).closest('tr').find('.religion_subject').val();
//            var group = $(this).closest('tr').find('.group').val();
//            var group_subject = $(this).closest('tr').find('.group_subject').val();
//            var fourth_subject = $(this).closest('tr').find('.fourth_subject').val();
//            var fourth_subject = $(this).closest('tr').find('.fourth_subject').val();
//            $.ajax({
//            url: "ajax_common.php",
//                    method: 'POST',
//                    data: {'username': $(this).val()}, //then getting $_POST['username']
//                    success: function (datas) {
//                    if (datas == 1)
//                    {
//                    $("#username").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Username not available</div>");
//                            $("#username").val('');
//                    }
//                    },
//                    error: function (XMLHttpRequest, textStatus, errorThrown) {
//                    console.log(textStatus);
//                    }
//            });
//            $.ajax({
//            url: "ajax_common.php",
//                    method: 'POST',
//                    dataType : 'json',
//                    data: {
//                    action:'assign_single_student',
//                            assign_primary_id:assign_primary_id,
//                            student_primary_id:student_primary_id,
//                            religion_subject:religion_subject,
//                            group:group,
//                            group_subject:group_subject,
//                            fourth_subject:fourth_subject
//                    },
//                    success: function (data) {
//                    if (data.error == 1) {
//                    alert('There is some problem, Please try again later !!!')
//                    } else {
//                    alert('Data successfully saved !!!')
//                    }
//                    },
//            });
//            });
    });
</script>
</body>
</html>
