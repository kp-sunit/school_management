<?php 
   include_once("./includes/session.php");
   
   //include_once("includes/config.php");
   
   include_once("./includes/config.php");
   require('fpdf181/fpdf.php');
function hex2dec($couleur = "#000000"){
    $R = substr($couleur, 1, 2);
    $rouge = hexdec($R);
    $V = substr($couleur, 3, 2);
    $vert = hexdec($V);
    $B = substr($couleur, 5, 2);
    $bleu = hexdec($B);
    $tbl_couleur = array();
    $tbl_couleur['R']=$rouge;
    $tbl_couleur['V']=$vert;
    $tbl_couleur['B']=$bleu;
    return $tbl_couleur;
}

//conversion pixel -> millimeter at 72 dpi
function px2mm($px){
    return $px*25.4/72;
}

function txtentities($html){
    $trans = get_html_translation_table(HTML_ENTITIES);
    $trans = array_flip($trans);
    return strtr($html, $trans);
}
////////////////////////////////////

class PDF_HTML extends FPDF
{
//variables of html parser
protected $B;
protected $I;
protected $U;
protected $HREF;
protected $fontList;
protected $issetfont;
protected $issetcolor;

function __construct($orientation='P', $unit='mm', $format='A4')
{
    //Call parent constructor
    parent::__construct($orientation,$unit,$format);
    //Initialization
    $this->B=0;
    $this->I=0;
    $this->U=0;
    $this->HREF='';
    $this->fontlist=array('arial', 'times', 'courier', 'helvetica', 'symbol');
    $this->issetfont=false;
    $this->issetcolor=false;
}

function WriteHTML($html)
{
    //HTML parser
    $html=strip_tags($html,"<b><u><i><a><img><p><br><strong><em><font><tr><blockquote>"); //supprime tous les tags sauf ceux reconnus
    $html=str_replace("\n",' ',$html); //remplace retour à la ligne par un espace
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE); //éclate la chaîne avec les balises
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(5,stripslashes(txtentities($e)));
        }
        else
        {
            //Tag
            if($e[0]=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag, $attr)
{
    //Opening tag
    switch($tag){
        case 'STRONG':
            $this->SetStyle('B',true);
            break;
        case 'EM':
            $this->SetStyle('I',true);
            break;
        case 'B':
        case 'I':
        case 'U':
            $this->SetStyle($tag,true);
            break;
        case 'A':
            $this->HREF=$attr['HREF'];
            break;
        case 'IMG':
            if(isset($attr['SRC']) && (isset($attr['WIDTH']) || isset($attr['HEIGHT']))) {
                if(!isset($attr['WIDTH']))
                    $attr['WIDTH'] = 0;
                if(!isset($attr['HEIGHT']))
                    $attr['HEIGHT'] = 0;
                $this->Image($attr['SRC'], $this->GetX(), $this->GetY(), px2mm($attr['WIDTH']), px2mm($attr['HEIGHT']));
            }
            break;
        case 'TR':
        case 'BLOCKQUOTE':
        case 'BR':
            $this->Ln(5);
            break;
        case 'P':
            $this->Ln(10);
            break;
        case 'FONT':
            if (isset($attr['COLOR']) && $attr['COLOR']!='') {
                $coul=hex2dec($attr['COLOR']);
                $this->SetTextColor($coul['R'],$coul['V'],$coul['B']);
                $this->issetcolor=true;
            }
            if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist)) {
                $this->SetFont(strtolower($attr['FACE']));
                $this->issetfont=true;
            }
            break;
    }
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='STRONG')
        $tag='B';
    if($tag=='EM')
        $tag='I';
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
    if($tag=='FONT'){
        if ($this->issetcolor==true) {
            $this->SetTextColor(0);
        }
        if ($this->issetfont) {
            $this->SetFont('arial');
            $this->issetfont=false;
        }
    }
}

function SetStyle($tag, $enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
    {
        if($this->$s>0)
            $style.=$s;
    }
    $this->SetFont('',$style);
}

function PutLink($URL, $txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(5,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}

}//end of class
   
   $url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
   
   ?>
<?php
   if(isset($_REQUEST['submit']))
   {
       $sql="select * from `school_students` where 1";
       if(!empty($_REQUEST["studentid"]))
       {
           $sql.=" and studentid='".$_REQUEST["studentid"]."'";
       }
       if(!empty($_REQUEST["shift_id"]))
       {
           $sql.=" and shiftid='".$_REQUEST["shift_id"]."'";
       }
       if(!empty($_REQUEST["class_id"]))
       {
           $sql.=" and class_id='".$_REQUEST["class_id"]."'";
       }
       if(!empty($_REQUEST["section_id"]))
       {
           $sql.=" and section_id='".$_REQUEST["section_id"]."'";
       }
       if(!empty($_REQUEST["group"]))
       {
           $sql.=" and school_students.group='".$_REQUEST["group"]."'";
       }
       
       $rs=mysql_query($sql) or die(mysql_error());
       $html="<style type='text/css'>.pagebreak { page-break-before: always; }</style>";
	while($row=mysql_fetch_assoc($rs))
	{
            

		$fetch_class=mysql_query("select * from `classname`  where id='".$row['class_id']."'");	
		$class=mysql_fetch_array($fetch_class);
                $sqlpro=mysql_query("SELECT * FROM `school_adminprofile` where userid=1"); 
                $rowpro=mysql_fetch_array($sqlpro);

                $sqladmin=mysql_query("SELECT * FROM `school_admin` where id=1"); 
                $rowadmin=mysql_fetch_array($sqladmin);
                 if($rowadmin['image']==''){
                   $image_link='http://mhcds.dartmouth.edu/images/made/uploads/mhcds/images/img_profile_goldberg_301_303_s_c1.jpg';
                 }else{
                    $image_link=SITE_URL.'upload/documents/'.$rowadmin['image'];
                 }
                 $profile_img=!empty($row['image'])?"./upload/documents/".$row['image']:"./upload/no.png";
            $html.='<div class="full_width" style="width:100%;float: left;padding:5px;border: 3px #002060 dotted;overflow: hidden;position: relative;margin: 0 auto;">
        <div class="logo_part" style="width: 98%;float: left;padding: 10px; border: 2px solid #000;display: flex;align-items:center;margin: 0 auto;">
            <div class="logo" style="width: 20%;float:left;">
                <img src="'.$image_link.'">
            </div>
            <div class="name_logo" style="width:70%;float: left; text-align: center;">
                <h3 style="color: #da026e;font-size:15px;font-weight: 700; margin: 0;">'.$rowpro['institution'].'</h3>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div class="admit" style="width: 70%; margin:20px auto; text-align: center;color:#000099;float: left;font-size:15px;font-weight: 700;">Admit Card</div>
        <div class="logo_part" style="width: 98%;float: left;padding: 15px; border: 1px solid #000;margin: 0 auto;">
            <div class="information" style="width: 68%;float: left; padding: 5px;">
            
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;font-size:12px;">Roll </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;font-size:12px;">: '.sprintf("%03d", $row['roll']).' </h4>
                    </div>
                </div>
                
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width:35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;font-size:12px;">Class </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;font-size:12px;">: '.$class['classname'].' </h4>
                    </div>
                </div>
                
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;font-size:12px;">Name </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;font-size:12px;">: '.$row['name'].' </h4>
                    </div>
                </div>
                
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;font-size:12px;">Fathers Name </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;font-size:12px;">: '.$row['guardianname'].' </h4>
                    </div>
                </div>
                
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;font-size:12px;">Mothers Name </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;font-size:12px;">: '.$row['mothername'].' </h4>
                    </div>
                </div>
                <div class="infos" style="width:100%;float: left;">
                    <div class="fields" style="width: 35%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;font-size:12px;">Date of Birth </h4>
                    </div>
                    <div class="fields" style="width: 65%;float:left">
                        <h4 style="color: #000;font-size:20px;font-weight: 600; margin: 18px 0;font-size:12px;">: '.$row['dob'].'</h4>
                    </div>
                </div>
            </div>
            <div class="pics" style="width: 20%;float: right;padding: 5px;">
                <div class="ad_pic" style="padding: 5px;text-align: center;">
                    <img src="'.$profile_img.'" style="border: 1px solid #000; padding: 5px; margin: 0 auto; width: 225px;height: 250px">
                </div>
                <div class="ad_pic" style="padding: 5px;text-align: center;margin: 15px auto;">
                    <img src="./upload/documents/'.$rowpro['signaturehead'].'" style="margin: 0 auto; width: 264px;height: 90px">
                </div>
                <div class="ad_pic" style="padding: 5px;text-align: center;margin: 0 auto;">
                    <h4 style="color: #000;font-size:14px;font-weight: 600;margin: 2px 0;">Signature Head of Institute</h4>
                 
                </div>
                
            </div>
        </div>
        <div class="admit" style="background:#d9d9d9; width: 100%; margin:20px auto; text-align: center;color:#921600;float: left;font-size: 15px;font-weight: 700;">General Instruction of Applicants</div>
        <div class="logo_part" style="width: 98%;float: left;padding: 15px; border: 1px solid #000;margin: 0 auto;">
            <div class="information" style="width: 100%;float: left; padding: 5px;">
            
                <div class="infos" style="width:70%;float: left;">
                    <div class="fields" style="width: 100%;float:left">
                        <h4 style="color: #000;font-size:12px;font-weight: 600; margin: 10px 0;font-size:12px;">'.$rowpro['instructionpdf'].' </h4>
                    </div>
                </div>
              
                </div>
            </div>
        </div>
        <div class="pagebreak"> </div>
        ';

            
        }
        
        include("mpdf60/mpdf.php");
        //echo $html;exit;
        $mpdf=new mPDF('utf-8', 'A4-P'); 
        $mpdf->WriteHTML($html);
        $mpdf->Output('admitcard.pdf','I');
        exit;
        
   }
   ?>
<?php include('includes/header.php');?>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<?php include('includes/left_panel.php');?>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
   <div class="page-content">
      <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <!-- /.modal -->
      <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <!-- BEGIN STYLE CUSTOMIZER -->
      <?php //include('includes/style_customize.php');?>
      <!-- END STYLE CUSTOMIZER -->
      <!-- BEGIN PAGE HEADER-->
      <h3 class="page-title"> Admit Card  </h3>
      <div class="page-bar">
         <ul class="page-breadcrumb">
            <li>
               <i class="fa fa-home"></i>
               <a href="dashboard.php">Home</a>
               <i class="fa fa-angle-right"></i>
            </li>
            <li>
               <a href="#">Admit Card </a>
            </li>
         </ul>
      </div>
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->
      <div class="row">
         <div class="col-md-12">
            <div class="portlet box blue">
               <div class="portlet-title">
                  <div class="caption">
                     <i class="fa fa-gift"></i><?php //echo $_REQUEST['action']=='edit'?"Edit":"Add";?> Admit Card
                  </div>
                  <div class="tools">
                  </div>
               </div>
               <div class="portlet-body form">
                  <!-- BEGIN FORM-->
                  <form  class="form-horizontal" method="post" action="" enctype="multipart/form-data" id="admit_form">
                     <div class="form-body">
                        <div class="form-group">
                           <label class="col-md-3 control-label">Shift</label>
                           <div class="col-md-4">
                              <select class="form-control"  name="shift_id" >
                                 <option value=""> select Shift</option>
                                 <?php 		$fetch_shift=mysql_query("select * from `shiftname`  where 1");	
                                    $numshift=mysql_num_rows($fetch_shift);
                                    
                                    if($numshift>0)
                                    
                                    {
                                    
                                    while($shift=mysql_fetch_array($fetch_shift))
                                    
                                    { ?>
                                 <option <?php if($categoryRowset['shift_id']==$shift['id']) {echo 'selected';} ?> value="<?php echo $shift['id']; ?>"><?php echo $shift['shiftname']; ?></option>
                                 <?php } }?>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-3 control-label">Class</label>
                           <div class="col-md-4">
                              <select class="form-control"  name="class_id" >
                                 <option value=""> select Class</option>
                                 <?php 		
                                    $fetch_class=mysql_query("select * from `classname`  where 1 and status=1 order by `frontorder`");	
                                    $numclass=mysql_num_rows($fetch_class);
                                    if($numclass>0)
                                    
                                    {
                                    
                                    while($class=mysql_fetch_array($fetch_class))
                                    
                                    { ?>
                                 <option  value="<?php echo $class['id']; ?>"><?php echo $class['classname']; ?></option>
                                 <?php } }?>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-3 control-label">Section</label>
                           <div class="col-md-4">
                              <select class="form-control"  name="section_id" >
                                 <option value=""> select Section</option>
                                 <?php 		
                                    $fetch_section=mysql_query("select * from `sectionname`  where 1");	
                                    $numsection=mysql_num_rows($fetch_section);
                                    if($numsection>0)
                                    {
                                    while($section=mysql_fetch_array($fetch_section))
                                    { ?>
                                 <option  value="<?php echo $section['id']; ?>"><?php echo $section['sectionname']; ?></option>
                                 <?php } }?>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-3 control-label">Group</label>
                           <div class="col-md-4">
                              <select class="form-control"  name="group" >
                                 <option value=""> select Group</option>
                                 <?php 		
                                    $fetch_group=mysql_query("SELECT * FROM school_new_group WHERE name <> ''");	
                                    $numgroup=mysql_num_rows($fetch_group);
                                    if($numgroup>0)
                                    {
                                    while($group=mysql_fetch_array($fetch_group))
                                    { ?>
                                 <option  value="<?php echo $group['id']; ?>"><?php echo $group['name']; ?></option>
                                 <?php } }?>
                              </select>
                           </div>
                        </div>
<!--                        <div class="form-group">
                           <label class="col-md-3 control-label">Subject</label>
                           <div class="col-md-4">
                              <select class="form-control"  name="subject_id[]" multiple="" required>
                                 <option value=""> Select Subject</option>
                                 <?php 		$fetch_subject=mysql_query("SELECT * FROM `allsubject` where 1 order by list_order asc");	
                                    $numsubject=mysql_num_rows($fetch_subject);
                                    
                                    if($numsubject>0)
                                    
                                    {
                                    
                                    while($subject=mysql_fetch_array($fetch_subject))
                                    
                                    { ?>
                                 <option <?php if(in_array($subject['id'], $subjectarr)) {echo 'selected';} ?> value="<?php echo $subject['id']; ?>"><?php echo $subject['subjectname']; ?></option>
                                 <?php } }?>
                              </select>
                           </div>
                        </div>-->
                        <div class="form-group">
                           <label class="col-md-3 control-label">Student ID</label>
                           <div class="col-md-4">
                               <input type="text" class="form-control" name="studentid">
                           </div>
                        </div> 
                        <div class="form-actions fluid">
                           <div class="row">
                              <div class="col-md-offset-3 col-md-9">
                                 <button type="submit" class="btn blue"  name="submit">Submit</button>
                                 <button type="submit" class="btn blue"  name="print" id="printadmit">Print</button>
                              </div>
                           </div>
                        </div>
                  </form>
                  <!-- END FORM-->
                  </div>
               </div>
            </div>
         </div>
         <!-- END PAGE CONTENT-->
      </div>
   </div>
   <style>
      .thumb{
      height: 60px;
      width: 60px;
      padding-left: 5px;
      padding-bottom: 5px;
      }
   </style>
   <script>
      window.preview_this_image = function (input) {
      
      
      
          if (input.files && input.files[0]) {
      
              $(input.files).each(function () {
      
                  var reader = new FileReader();
      
                  reader.readAsDataURL(this);
      
                  reader.onload = function (e) {
      
                      $("#previewImg").append("<span><img class='thumb' src='" + e.target.result + "'><img border='0' src='../images/erase.png'  border='0' class='del_this' style='z-index:999;margin-top:-34px;'></span>");
      
                  }
      
              });
      
          }
      
      }
      
   </script>
   <!-- END CONTENT -->
   <!-- BEGIN QUICK SIDEBAR -->
   <?php //include('includes/quick_sidebar.php');?>
   <!-- END QUICK SIDEBAR -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include('includes/footer.php'); ?>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/form-samples.js"></script>
<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
   jQuery(document).ready(function() {    
   
      // initiate layout and plugins
   
      Metronic.init(); // init metronic core components
   
   Layout.init(); // init current layout
   
   QuickSidebar.init(); // init quick sidebar
   
   Demo.init(); // init demo features
   
      FormSamples.init();
   $("#printadmit").click(function(){
   $("#admit_form").attr("target","_blank")  
   $("#admit_form").attr("action","print_admitcard.php")  
   window.location.reload();
    });
      
   
       if (jQuery().datepicker) {
   
               $('.date-picker').datepicker({
   
                   rtl: Metronic.isRTL(),
   
                   orientation: "left",
   
                   autoclose: true,
   
                   language: "xx"
   
               });
   
           }
   
      
   
   });
   
   
   
   $(document).ready(function(){
   
   
   
      $('.del_this').click(function (event) {
   
                   var id = $(this).data('imgid');
   
   
   
                   var divs = $(this);
   
                   var formdata = {id: id, action: "deleteImg"};
   
                   $.ajax({
   
                       url: "del_ajax.php",
   
                       type: "POST",
   
                       dataType: "json",
   
                       data: formdata,
   
                       success: function (data) {
   
                           if (data.ack == 1)
   
                           {
   
   
   
                               $(divs).closest('.div_div').remove();
   
                           }
   
   
   
   
   
   
   
                       }
   
   
   
                   });
   
               });
   
   
   
   
   
   
   
   $(document).on("click",".del_this",function(){
   
   $(this).parent().remove();
   
   
   
   });
   
        
   
       
   
        });
   
       
   
</script>
<script>
   $(document).ready(function(){
   
       $(".san_open").parent().parent().addClass("active open");
   
   });
   
   document.getElementById("firstfield").focus();
   
</script>
<!-- END JAVASCRIPTS -->
</body>

<!-- END BODY -->
</html>

