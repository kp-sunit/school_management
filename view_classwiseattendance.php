<link  href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link  href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
<?php 

include_once("./includes/session.php");

//include_once("includes/config.php");

include_once("./includes/config.php"); 

$url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
$row_attendencetype=mysql_fetch_assoc(mysql_query("select * from school_attendencetype where id=1"));
?>
<?php 	
include("includes/header.php"); 	
header('Content-Type: text/html; charset=utf-8');
?>

<div class="clearfix">

</div>

<!-- BEGIN CONTAINER -->

<div class="page-container">

	<!-- BEGIN SIDEBAR -->

	<?php include("includes/left_panel.php"); ?>

	<!-- END SIDEBAR -->

	<!-- BEGIN CONTENT -->

	<div class="page-content-wrapper">

		<div class="page-content">

			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			

			<!-- /.modal -->

			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN STYLE CUSTOMIZER -->

			

			<!-- END STYLE CUSTOMIZER -->

			<!-- BEGIN PAGE HEADER-->

			<h3 class="page-title">

			Class Wise Attendance

			</h3>

			<div class="page-bar">

				<ul class="page-breadcrumb">

					<li>

						<i class="fa fa-home"></i>

						<a href="index.php">Home</a>

						<i class="fa fa-angle-right"></i>

					</li>

					<li>

						<a href="#">Class Wise Attendance</a>

						<i class="fa fa-angle-right"></i>

					</li>

					<!--<li>

						<a href="#">Editable Datatables</a>

					</li>-->

				</ul>

				<!--<div class="btn-group" style="float:right">

				<button id="sample_editable_1_new" class="btn blue" onclick="location.href='add_group.php'">

				Add New Group/Subject  <i class="fa fa-plus"></i>

				</button>

				</div>-->

			</div>

			<!-- END PAGE HEADER-->

			<!-- BEGIN PAGE CONTENT-->

			

	

			

			

			

			<div class="row">

				<div class="col-md-12">

					<!-- BEGIN EXAMPLE TABLE PORTLET-->

					<div class="portlet box blue">

						<div class="portlet-title">

							<div class="caption">

                           Class Wise Attendance

								<!--<i class="fa fa-edit"></i>Editable Table-->

							</div>

						

						</div>

						<div class="portlet-body">

							<div class="table-toolbar">

								<div class="row">



								</div>

							</div>

							<table class="table table-striped table-hover table-bordered" id="sample_editable_1">

							<thead>

							<tr>

                                                                <th>Class</th>
								<th>Total Student</th>
								<th>Total Present</th>
                                                                <th>Total Absent</th>
                                                                <th>Present (%)</th>
                                                                <th>Absent (%)</th>
                                                                <th>Action</th>
                                                                

									

							</tr>

                                                 

							</thead>
                                                        

							<tbody>

		<?php



			$fetch_product=mysql_query("select * from classname where status=1 order by frontorder");	

                        $num=mysql_num_rows($fetch_product);

                        if($num>0)

                        {

                        while($product=mysql_fetch_array($fetch_product))

                        {
                            
	

                    ?>

							

<tr>



<td><?php echo $product['classname'] ?></td>
<?php
$present_student=array();
$attend=array();
$latestuden_id=array();
$date=date('Y-m-d');
$total_student=mysql_num_rows(mysql_query("select * from school_students where class_id='".$product['id']."'  and is_deleted=0"));
 if($row_attendencetype['attendencetype']==1)
 {
    $attend_sql=mysql_query("select * from `school_managestudentattendance` where  date='".$date."' and attentype=1 and class_id='".$product['id']."'") or die(mysql_error());    
    $row_attend=mysql_fetch_assoc($attend_sql); 
 }
 else
 {
      $attend_sql=mysql_query("select * from `school_managestudentattendance` where  date='".$date."' and attentype=2 and class_id='".$product['id']."'") or die(mysql_error());    

 }
 $is_roll_call=mysql_num_rows($attend_sql);
 $total_present=0;
 $total_absent=0;
 $total_present_percent=0;
 $total_absent_percent=0;
if($is_roll_call>0)
{
if($row_attendencetype['attendencetype']==1)
{
if(!empty($row_attend['studen_id']))
{
$attend=explode(",",$row_attend['studen_id']);
}
if(!empty($row_attend['latestuden_id']))
{
$latestuden_id=explode(",",$row_attend['latestuden_id']);
}
}else{
    while($row_attend=mysql_fetch_assoc($attend_sql))
    {
        if(!empty($row_attend['studen_id']))
        {
            $studen_id=explode(",",$row_attend['studen_id']);
            foreach ($studen_id as $st_id)
            {
               $attend[]= $st_id;
            }
        }
        if(!empty($row_attend['latestuden_id']))
        {
            $latestuden=explode(",",$row_attend['latestuden_id']);
            foreach ($latestuden as $st_id)
            {
                $latestuden_id[]=$st_id;
            }
         }
    }
}
$present_student=array_unique(array_merge($attend,$latestuden_id));
$total_present=count($present_student);
$total_absent=$total_student-$total_present;
if($total_student>0)
{
$total_present_percent=($total_present*100)/$total_student;
$total_absent_percent=($total_absent*100)/$total_student;
}
else{
  $total_present_percent=0;
  $total_absent_percent=0;
}
}
?>
<td style=" text-align:center; vertical-align:middle;"><?php echo $total_student; ?></td>
<td style=" text-align:center; vertical-align:middle;"><?php echo $total_present; ?></td>
<td style=" text-align:center; vertical-align:middle;"><?php echo $total_absent; ?></td>
<td style=" text-align:center; vertical-align:middle;"><?php echo number_format((float)$total_present_percent, 2, '.', ''); ?></td>
<td style=" text-align:center; vertical-align:middle;"><?php echo number_format((float)$total_absent_percent, 2, '.', ''); ?></td>
<td>


<a href="view_attendancebyclass.php?id=<?php echo $product['id']; ?>">View </a>



</td>

</tr>

                                                       <?php

                                                        }

                                                        }

                                                        else

                                                        {

                                                            ?>

                                                        <tr>

                    <td colspan="4">Sorry, no record found.</td>

                  </tr>

                                                        

                                                        <?php

                                                        }

                                                       ?>

                                                        

                                                        

                                                        

                                                        

							</tbody>

							</table>

                 

</form

						</div>

					</div>

					<!-- END EXAMPLE TABLE PORTLET-->

				</div>

			</div>

			<!-- END PAGE CONTENT -->

		</div>

	</div>

	<!-- END CONTENT -->

	

</div>

<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<div class="page-footer">

	<?php include("includes/footer.php"); ?>

</div>

<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->

<!--[if lt IE 9]>

<script src="assets/global/plugins/respond.min.js"></script>

<script src="assets/global/plugins/excanvas.min.js"></script> 

<![endif]-->

<style>
.table>thead>tr>th {
    vertical-align: top;
    }
</style>

<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>

<script>

jQuery(document).ready(function() {       

   Metronic.init(); // init metronic core components

Layout.init(); // init current layout

QuickSidebar.init(); // init quick sidebar

Demo.init(); // init demo features

   //TableEditable.init();

});

</script>



<script type="text/javascript">

function deleteConfirm(){

    var result = confirm("Are you sure to delete product?");

    if(result){

        return true;

    }else{

        return false;

    }

}



//$(document).ready(function(){
//    var table=$('#sample_editable_1').DataTable( {
//   
//   
//           dom: 'Bfrtip',
//           buttons:[],
//           pageLength:30,
////          buttons: [
////              
////              {
////                 extend: 'excelHtml5',
////                    messageTop: null,
////                    filename: 'Data export',
////                  exportOptions: {
////                      columns: [ 0 ]
////                  }
////               },
////               {
////                  extend: 'pdfHtml5',
////                  messageTop: null,
////                    filename: 'Data export',
////                   exportOptions: {
////                       columns: [ 0 ]
////                   }
////               },
////           ],
//      
//           lengthMenu: [
//   
//                   [5, 15, 20, -1],
//   
//                   [5, 15, 20, "All"] // change per page values here
//   
//               ],
//       } );
//    $('.dataTables_filter').find('input').addClass('form-control input-small input-inline');
//             $('#sample_editable_1 tfoot th').each( function () {
//                 if($(this).text()!='')
//                 {
//                var title = $(this).text();
//                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
//            }
//    } );
//
//
//             table.columns().every( function () {
//            var that = this;
//
//            $( 'input', this.footer() ).on( 'keyup change', function () {
//                if ( that.search() !== this.value ) {
//                    that
//                        .search( this.value )
//                        .draw();
//                }
//            } );
//        } );
//    $('#select_all').on('click',function(){
//
//        if(this.checked){
//
//            $('.checkbox').each(function(){
//
//                this.checked = true;
//
//            });
//
//        }else{
//
//             $('.checkbox').each(function(){
//
//                this.checked = false;
//
//            });
//
//        }
//
//    });
//
//    
//
//    $('.checkbox').on('click',function(){
//
//        if($('.checkbox:checked').length == $('.checkbox').length)
//
//        {
//
//            $('#select_all').prop('checked',true);
//
//        }else{
//
//            $('#select_all').prop('checked',false);
//
//        }
//
//    });
//
//});

    

 



</script>

<script>



$(document).ready(function(){

    $(".san_open").parent().parent().addClass("active open");
//    $(".dt-buttons").append('<a class="dt-button"  onclick=download_pdf("print_listattendance.php")><span>PDF</span></a>');
//    $(".dt-buttons").append('<a class="dt-button"  onclick=deleteConfirmAll()><span>Delete All</span></a>');

});

//document.getElementById("focusElement").focus();

</script>
<style type="text/css">
 tfoot {
    display: table-header-group;
}
    tfoot input {
        width: 100%;
        padding: 6px;
        box-sizing: border-box;
        font-size: 12px;
    }
</style>
</body>

<!-- END BODY -->

</html>

