<?php 
include_once("./includes/session.php");
//include_once("includes/config.php");
include_once("./includes/config.php");
$url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
?>
<?php

if(isset($_REQUEST['submit']))
{

	$accounttype = isset($_POST['accounttype']) ? $_POST['accounttype'] : '';
	$subaccounttype = isset($_POST['subaccounttype']) ? $_POST['subaccounttype'] : '';
	$subaccounttype=implode(",",$subaccounttype);

	$shift_id = isset($_POST['shift_id']) ? $_POST['shift_id'] : '';
	$class_id = isset($_POST['class_id']) ? $_POST['class_id'] : '';
	$section_id = isset($_POST['section_id']) ? $_POST['section_id'] : '';
	$group_id = isset($_POST['group_id']) ? $_POST['group_id'] : '';
	
	$amount = isset($_POST['amount']) ? $_POST['amount'] : '';
	$id = isset($_POST['id']) ? $_POST['id'] : '';
	
	

	$fields = array(
		'accounttype' => mysql_real_escape_string($accounttype),
		'subaccounttype' => mysql_real_escape_string($subaccounttype),
		'shift_id' => mysql_real_escape_string($shift_id),
		'class_id' => mysql_real_escape_string($class_id),
		'section_id' => mysql_real_escape_string($section_id),
		'group_id' => mysql_real_escape_string($group_id),
		
		'amount' => mysql_real_escape_string($amount),

		);
	
	

		$fieldsList = array();
		foreach ($fields as $field => $value) {
			$fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
		}
					 
	 if($_REQUEST['action']=='edit')
	  {		  
	 $editQuery = "UPDATE `school_addmissionentryfees` SET " . implode(', ', $fieldsList)
			. " WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'";
			
		//	exit;

		if (mysql_query($editQuery)) {
		
		
		
			$_SESSION['msg'] = "Category Updated Successfully";
		}
		else {
			$_SESSION['msg'] = "Error occuried while updating Category";
		}

			
			
		header('Location:list_admission_entry_fees.php');
		exit();
	
	 }
	 else
	 {
	 
	 $addQuery = "INSERT INTO `school_addmissionentryfees` (`" . implode('`,`', array_keys($fields)) . "`)"
			. " VALUES ('" . implode("','", array_values($fields)) . "')";
			
			//exit;
		mysql_query($addQuery);
		$last_id=mysql_insert_id();
		
	

		header('Location:list_admission_entry_fees.php');
		exit();
	
	 }
				
				
}

$subaccounttype=array();
if($_REQUEST['action']=='edit')
{
$categoryRowset = mysql_fetch_array(mysql_query("SELECT * FROM `school_addmissionentryfees` WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'"));

$subaccounttype=explode(",",$categoryRowset['subaccounttype']);

}
?>

<?php include('includes/header.php');?>
<!-- END HEADER -->


<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include('includes/left_panel.php');?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN STYLE CUSTOMIZER -->
			<?php //include('includes/style_customize.php');?>
			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">Entry Fees </h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.php">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Entry Fees </a>
						
					</li>
					
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php echo $_REQUEST['action']=='edit'?"Edit":"Add";?> Entry Fees
										</div>
										<div class="tools">
											
											
											
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
		<form  class="form-horizontal" method="post" action="add_admission_entry_fees.php" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
		
		<input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>" />
		
		
		<div class="form-body">

		
		<div class="form-group">
		<label class="col-md-3 control-label">Shift</label>
		<div class="col-md-4">
		<select class="form-control"  name="shift_id" required>
		<option value=""> select Shift</option>
		
		<?php 		
		$fetch_shift=mysql_query("select * from `shiftname`  where 1");	
		$numshift=mysql_num_rows($fetch_shift);
		if($numshift>0)
		{
		while($shift=mysql_fetch_array($fetch_shift))
		{ ?>
		
		<option <?php if($categoryRowset['shift_id']==$shift['id']) {echo 'selected';} ?> value="<?php echo $shift['id']; ?>"><?php echo $shift['shiftname']; ?></option>
		<?php } }?>
		</select>
		
		</div>
		</div>
		
		<div class="form-group">
		<label class="col-md-3 control-label">Class</label>
		<div class="col-md-4">
		<select class="form-control"  name="class_id" required>
		<option value=""> select Class</option>
		<?php 		
		$fetch_class=mysql_query("select * from `classname`  where 1 order by `frontorder`");	
		$numclass=mysql_num_rows($fetch_class);
		if($numclass>0)
		{
		while($class=mysql_fetch_array($fetch_class))
		{ ?>
		<option <?php if($categoryRowset['class_id']==$class['id']) {echo 'selected';} ?> value="<?php echo $class['id']; ?>"><?php echo $class['classname']; ?></option>
		<?php } }?>
		</select>
		</div>
		</div>
		
		<div class="form-group">
		<label class="col-md-3 control-label">Section</label>
		<div class="col-md-4">
		<select class="form-control"  name="section_id" required>
		<option value=""> select Section</option>
		<?php 		
		$fetch_shift=mysql_query("select * from `sectionname`  where 1");			
		$numshift=mysql_num_rows($fetch_shift);		
		if($numshift>0)		
		{		
		while($shift=mysql_fetch_array($fetch_shift))		
		{ ?>
		<option <?php if($categoryRowset['section_id']==$shift['id']) {echo 'selected';} ?> value="<?php echo $shift['id']; ?>"><?php echo $shift['sectionname']; ?></option>		
		<?php } }?>		
		</select>
		
		</div>
		</div>
		
		<div class="form-group">
		<label class="col-md-3 control-label">Group</label>
		<div class="col-md-4">
		<select class="form-control"  name="group_id" required>
		<!--<option value=""> select Group</option>-->
		<?php 		
		$fetch_shift=mysql_query("select * from `school_new_group`  where 1");			
		$numshift=mysql_num_rows($fetch_shift);		
		if($numshift>0)		
		{		
		while($shift=mysql_fetch_array($fetch_shift))		
		{ ?>
		<option <?php if($categoryRowset['group_id']==$shift['id']) {echo 'selected';} ?> value="<?php echo $shift['id']; ?>"><?php echo $shift['name']; ?></option>		
		<?php } }?>		
		</select>
		
		</div>
		</div>
		
				<div class="form-group">
		<label class="col-md-3 control-label">Select Account Type</label>
		<div class="col-md-4">
		<select class="form-control"  name="accounttype" id="accounttype" required>
		<option value=""> select Account Type</option>
		<?php 		
		$fetch_shift=mysql_query("select * from `school_account_type`  where 1 order by orderfront asc");			
		$numshift=mysql_num_rows($fetch_shift);		
		if($numshift>0)		
		{		
		while($shift=mysql_fetch_array($fetch_shift))		
		{ ?>
		<option <?php if($categoryRowset['accounttype']==$shift['id']) {echo 'selected';} ?> value="<?php echo $shift['id']; ?>"><?php echo $shift['name']; ?></option>		
		<?php } }?>		
		</select>
		
		</div>
		</div>	
		
		
		<div class="form-group">
		<label class="col-md-3 control-label">Select Sub Account Type</label>
		<div class="col-md-4">
		<select class="form-control"  name="subaccounttype[]" id="subaccounttype" multiple="" required>
	
		<?php 		
		$fetch_shift=mysql_query("select * from `school_account_sub`  where 1 order by orderfront asc");			
		$numshift=mysql_num_rows($fetch_shift);		
		if($numshift>0)		
		{		
		while($shift=mysql_fetch_array($fetch_shift))		
		{ ?>
		<option data-id="<?php echo $shift['type']; ?>" style="display:none;" <?php if(in_array($shift['id'], $subaccounttype)) {echo 'selected';} ?> value="<?php echo $shift['id']; ?>"><?php echo $shift['name']; ?></option>		
		<?php } }?>			
		</select>
		
		</div>
		</div>
	
	<div class="form-group">
	<label class="col-md-3 control-label">Amount</label>
	<div class="col-md-4">
	<input type="text" class="form-control" placeholder=""  value="<?php echo $categoryRowset['amount'];?>" name="amount">
	
	</div>
	</div>		
	
		
		
	<div class="form-group" style="display:none">
<label class="col-md-3 control-label">Image Upload</label>
<div class="col-md-2">
<input type="file" name="image" class=" btn blue"  >

</div>

</div>
	
		</div>
		
		<div class="form-actions fluid">
		<div class="row">
		<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn blue"  name="submit">Submit</button>
		
		</div>
		</div>
		</div>
		</form>
										<!-- END FORM-->
									</div>
								</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	
	
	
<style>
.thumb{
    height: 60px;
    width: 60px;
    padding-left: 5px;
    padding-bottom: 5px;
}

</style>

<script>

     
window.preview_this_image = function (input) {

    if (input.files && input.files[0]) {
        $(input.files).each(function () {
            var reader = new FileReader();
            reader.readAsDataURL(this);
            reader.onload = function (e) {
                $("#previewImg").append("<span><img class='thumb' src='" + e.target.result + "'><img border='0' src='../images/erase.png'  border='0' class='del_this' style='z-index:999;margin-top:-34px;'></span>");
            }
        });
    }
}
</script>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	<?php //include('includes/quick_sidebar.php');?>
	<!-- END QUICK SIDEBAR -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include('includes/footer.php'); ?>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/form-samples.js"></script>
<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>

<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init(); // init quick sidebar
Demo.init(); // init demo features
   FormSamples.init();
   
    if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true,
                language: "xx"
            });
        }
   
});

$(document).ready(function(){
		<?php 
	if($_REQUEST['action']=='edit')
{  ?>
		var selectedVal=<?php echo $categoryRowset['accounttype']; ?>;
		
		$('#subaccounttype > option[data-id="'+selectedVal+'"]').show();

	<?php } ?>

	$('#accounttype').change(function(){

		var selectedVal=$(this).val();
		$('#subaccounttype').val('');
		$('#subaccounttype > option').hide();
		$('#subaccounttype > option[data-id="'+selectedVal+'"]').show();

	});

   $('.del_this').click(function (event) {
                var id = $(this).data('imgid');

                var divs = $(this);
                var formdata = {id: id, action: "deleteImg"};
                $.ajax({
                    url: "del_ajax.php",
                    type: "POST",
                    dataType: "json",
                    data: formdata,
                    success: function (data) {
                        if (data.ack == 1)
                        {

                            $(divs).closest('.div_div').remove();
                        }



                    }

                });
            });



$(document).on("click",".del_this",function(){
$(this).parent().remove();

});
     
    
     });
    
</script>
<script>

$(document).ready(function(){
    $(".san_open").parent().parent().addClass("active open");
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
