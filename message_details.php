<?php
include_once("./includes/session.php");
include_once("./includes/config.php");
$url = basename(__FILE__) . "?" . (isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : 'cc=cc');
?>
<link href="assets/admin/pages/css/timeline.css" rel="stylesheet" type="text/css"/>


<?php include("includes/header.php"); ?>
<?php
//2=>Stuff,3=>Teacher,4=>Student,5=>Parent
$user_type = $_REQUEST['type'];
switch ($user_type) {
    case '1':
        $query = "SELECT COUNT(*) as is_new_message FROM school_sent_sms_admin WHERE id = '" . $_REQUEST['id'] . "' and is_new= '1'";
        $TABLE = 'school_sent_sms_admin';
        break;
    case '3':
        $query = "SELECT COUNT(*) as is_new_message FROM school_sent_sms_teacher WHERE id = '" . $_REQUEST['id'] . "' and is_new= '1'";
        $TABLE = 'school_sent_sms_teacher';
        break;
    case '4':
        $query = "SELECT COUNT(*) as is_new_message FROM school_sent_sms_student WHERE id = '" . $_REQUEST['id'] . "' and is_new= '1'";
        $TABLE = 'school_sent_sms_student';
        break;
    case '5':
        $query = "SELECT COUNT(*) as is_new_message FROM school_sent_sms_guardian WHERE id = '" . $_REQUEST['id'] . "' and is_new= '1'";
        $TABLE = 'school_sent_sms_guardian';
        break;
    default :
        break;
}
$is_already_shown = mysql_fetch_assoc(mysql_query($query));
if ($is_already_shown['is_new_message'] > 0) {
    mysql_query("UPDATE " . $TABLE . " SET is_new='0' WHERE id = '" . $_REQUEST['id'] . "'");
    ?>
    <script type="text/javascript">
        $(window).load(function () {

            // $('span.badge-default').remove();
        })
    </script>
    <?php
}
?>


<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php include("includes/left_panel.php"); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title"> Guardian </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li> <i class="fa fa-home"></i> <a href="index.php">Home</a> <i class="fa fa-angle-right"></i> </li>
                    <li> <a href="#"> Messages Details</a> <i class="fa fa-angle-right"></i> </li>
                </ul>

            </div>
            <div class="timeline">


                <?php
                switch ($user_type) {
                    case '1': // admin
                        $sql_query = "SELECT tbl_msg . * , 
tbl_school_admin.id AS tbl_school_admin_id, tbl_school_admin.image,tbl_school_admin.full_name, tbl_school_admin.is_admin FROM `school_sent_sms_admin` AS `tbl_msg` 
INNER JOIN `school_admin` AS `tbl_school_admin` 
ON `tbl_school_admin`.`id` = `tbl_msg`.`from_user_id` 
WHERE tbl_msg.to_user_id = '" . $user_id . "' 
    AND  tbl_msg.id = '" . $_REQUEST['id'] . "' 
ORDER BY tbl_msg.send_date DESC";
                        $messagesList = mysql_query($sql_query);
                        break;
                    case '3': // teacher
                        $sql_query = "SELECT tbl_msg .*,tbl_school_admin.id AS tbl_school_admin_id, tbl_school_admin.image, tbl_school_admin.is_admin,tbl_school_admin.full_name
FROM `school_sent_sms_teacher` AS `tbl_msg`
INNER JOIN `school_admin` AS `tbl_school_admin` ON `tbl_school_admin`.`id` = `tbl_msg`.`from_teacher_id` 
WHERE tbl_msg.to_teacher_id = '" . $user_id . "' 
AND  tbl_msg.id = '" . $_REQUEST['id'] . "' 
ORDER BY tbl_msg.send_date DESC";

                        $messagesList = mysql_query($sql_query);
                        break;
                    case '4': //student

                        $sql_query = "SELECT tbl_msg . * , 
tbl_school_admin.id AS tbl_school_admin_id, tbl_school_admin.image,tbl_school_admin.full_name, tbl_school_admin.is_admin FROM `school_sent_sms_student` AS `tbl_msg` 
INNER JOIN `school_admin` AS `tbl_school_admin` 
ON `tbl_school_admin`.`id` = `tbl_msg`.`teacher_id` 
WHERE tbl_msg.student_id = '" . $user_id . "' 
AND tbl_msg.id = '" . $_REQUEST['id'] . "' 
ORDER BY tbl_msg.send_date DESC";
                        $messagesList = mysql_query($sql_query);
                        break;
                    case '5': // gardian
                        $sql_query = "SELECT tbl_msg . * , 
tbl_school_admin.id AS tbl_school_admin_id, tbl_school_admin.image,tbl_school_admin.full_name, tbl_school_admin.is_admin FROM `school_sent_sms_guardian` AS `tbl_msg` 
INNER JOIN `school_admin` AS `tbl_school_admin` 
ON `tbl_school_admin`.`id` = `tbl_msg`.`teacher_id` 
WHERE tbl_msg.guardian_id = '" . $user_id . "' 
AND  tbl_msg.id = '" . $_REQUEST['id'] . "' 
ORDER BY tbl_msg.send_date DESC";
                        $messagesList = mysql_query($sql_query);
                        break;
                    default:
                        break;
                }
                ?>
                <?php while ($messagesInfo = mysql_fetch_assoc($messagesList)) { ?>


                    <?php
                    // print_r($messagesInfo);
                    switch ($user_type) {
                        case '1': // admin
                            switch ($messagesInfo['from_user_type']) {
                                case '4':  // from student
                                    $sender_type = 'Student';
                                    $student_sql = "SELECT tbl1.*,classname.id as classname_id,classname,sectionname.id as sectionname_id,sectionname.sectionname FROM school_students as tbl1 "
                                            . " INNER JOIN classname ON tbl1.class_id = classname.id"
                                            . " INNER JOIN sectionname ON tbl1.section_id = sectionname.id"
                                            . "  WHERE tbl1.userid ='" . $messagesInfo['from_user_id'] . "' ";
                                    $senderDetails = mysql_fetch_assoc(mysql_query($student_sql));
                                    $from_user_details = '<span class="timeline-body-time font-grey-cascade">[ SECTION: ' . $senderDetails['sectionname'] . ' , CLASS: ' . $senderDetails['classname'] . ']</spn>';
                                    break;
                                case '3':  // from teacher
                                    $sender_type = 'Teacher';
                                    $teacher_sql = "SELECT tbl1.*,sd.id as designation_id,name FROM school_teachers as tbl1 "
                                            . " INNER JOIN school_designation as sd ON tbl1.designation = sd.id"
                                            . "  WHERE tbl1.user_id ='" . $messagesInfo['from_user_id'] . "' ";
                                    $senderDetails = mysql_fetch_assoc(mysql_query($teacher_sql));
                                    $from_user_details = '<span class="timeline-body-time font-grey-cascade">[ Designation: ' . $senderDetails['name'] . ' ' . $senderDetails['name'] . ' ]</spn>';
                                    break;
                                case '5':  // from parent
                                    $sender_type = 'Parent';
                                    $parent_sql = "Select tbl1.id as student_id,tbl1.name ,tbl1.parentid,tbl1.class_id,tbl1.section_id,
                                        classname.id,classname.classname,sectionname.id,sectionname.sectionname FROM school_students as tbl1 
                                        INNER JOIN classname 
                                        ON classname.id = tbl1.class_id 
                                        INNER JOIN sectionname 
                                        ON sectionname.id = tbl1.section_id"
                                            . " WHERE tbl1.parentid ='" . $messagesInfo['from_user_id'] . "' ";
                                    $senderDetails = mysql_fetch_assoc(mysql_query($parent_sql));

                                    //$from_user_details = '<span class="timeline-body-time font-grey-cascade"> (Parent) [ SECTION: ' . $senderDetails['sectionname'] . ' , CLASS: ' . $senderDetails['classname'] . ']</spn>';
                                    $from_user_details = '<span class="timeline-body-time font-grey-cascade"> (' . $sender_type . ') [ STUDENT NAME: ' . $senderDetails['name'] . ', SECTION: ' . $senderDetails['sectionname'] . ' , CLASS: ' . $senderDetails['classname'] . ']</spn>';
                                    break;

                                default:
                                    break;
                            }
                            break;
                        case '3': // teacher
                            switch ($messagesInfo['from_user_type']) {
                                case '1':  // from admin
                                    $sender_type = 'Admin';
                                    $querySql = "SELECT * FROM school_adminprofile as tbl1 "
                                            . "  WHERE tbl1.userid ='" . $messagesInfo['from_teacher_id'] . "' ";
                                    $senderDetails = mysql_fetch_assoc(mysql_query($querySql));
                                    $from_user_details = '<span class="timeline-body-time font-grey-cascade">[ Institution: ' . $senderDetails['institution'] . ' ]</spn>';

                                    break;
                                case '4':  // from student
                                    $sender_type = 'Student';
                                    $querySql = "SELECT tbl1.*,classname.id as classname_id,classname,sectionname.id as sectionname_id,sectionname.sectionname FROM school_students as tbl1 "
                                            . " INNER JOIN classname ON tbl1.class_id = classname.id"
                                            . " INNER JOIN sectionname ON tbl1.section_id = sectionname.id"
                                            . "  WHERE tbl1.userid ='" . $messagesInfo['from_teacher_id'] . "' ";  // from teacher id is acturally senderid like student,admin or parent
                                    $senderDetails = mysql_fetch_assoc(mysql_query($querySql));
                                    $from_user_details = '<span class="timeline-body-time font-grey-cascade">[ SECTION: ' . $senderDetails['sectionname'] . ' , CLASS: ' . $senderDetails['classname'] . ']</spn>';

                                    break;
                                case '5':  // from parent / guardian
                                    $sender_type = 'Guardian';
                                    $parent_sql = "Select tbl1.id as student_id,tbl1.name ,tbl1.parentid,tbl1.class_id,tbl1.section_id,
                                        classname.id,classname.classname,sectionname.id,sectionname.sectionname FROM school_students as tbl1 
                                        INNER JOIN classname 
                                        ON classname.id = tbl1.class_id 
                                        INNER JOIN sectionname 
                                        ON sectionname.id = tbl1.section_id"
                                            . " WHERE tbl1.parentid ='" . $messagesInfo['from_teacher_id'] . "' ";
                                    $senderDetails = mysql_fetch_assoc(mysql_query($parent_sql));
                                    $from_user_details = '<span class="timeline-body-time font-grey-cascade"> (' . $sender_type . ') [ STUDENT NAME: ' . $senderDetails['name'] . ', SECTION: ' . $senderDetails['sectionname'] . ' , CLASS: ' . $senderDetails['classname'] . ']</spn>';

                                    break;
                                default:
                                    break;
                            }
                            break;
                        case '4':  // student 
                            switch ($messagesInfo['from_user_type']) {
                                case '1':  // from admin
                                    $sender_type = 'Admin';
                                    $querySql = "SELECT * FROM school_adminprofile as tbl1 "
                                            . "  WHERE tbl1.userid ='" . $messagesInfo['teacher_id'] . "' ";  // teacher_id  is the from user id here . can be admin, or any type of user
                                    $senderDetails = mysql_fetch_assoc(mysql_query($querySql));
                                    $from_user_details = '<span class="timeline-body-time font-grey-cascade">[ Institution: ' . $senderDetails['institution'] . ' ]</spn>';
                                    break;
                                case '3':  // from teacher
                                    $sender_type = 'Teacher';
                                    $teacher_sql = "SELECT tbl1.*,sd.id as designation_id,name FROM school_teachers as tbl1 "
                                            . " INNER JOIN school_designation as sd ON tbl1.designation = sd.id"
                                            . "  WHERE tbl1.user_id ='" . $messagesInfo['teacher_id'] . "' ";
                                    $senderDetails = mysql_fetch_assoc(mysql_query($teacher_sql));
                                    $from_user_details = '<span class="timeline-body-time font-grey-cascade">[ Designation: ' . $senderDetails['name'] . ' ' . $senderDetails['name'] . ' ]</spn>';
                                    break;
                                default :
                                    break;
                            }
                            break;
                        case '5': // parent
                            switch ($messagesInfo['from_user_type']) {
                                case '1':  // from admin 
                                    $sender_type = 'Admin';
                                    $querySql = "SELECT * FROM school_adminprofile as tbl1 "
                                            . "  WHERE tbl1.userid ='" . $messagesInfo['teacher_id'] . "' ";  // teacher_id  is the from user id here . can be admin, or any type of user
                                    $senderDetails = mysql_fetch_assoc(mysql_query($querySql));
                                    $from_user_details = '<span class="timeline-body-time font-grey-cascade">[ Institution: ' . $senderDetails['institution'] . ' ]</spn>';
                                    break;
                                 case '3':  // from teacher
                                    $sender_type = 'Teacher';
                                    $teacher_sql = "SELECT tbl1.*,sd.id as designation_id,name FROM school_teachers as tbl1 "
                                            . " INNER JOIN school_designation as sd ON tbl1.designation = sd.id"
                                            . "  WHERE tbl1.user_id ='" . $messagesInfo['teacher_id'] . "' ";
                                    $senderDetails = mysql_fetch_assoc(mysql_query($teacher_sql));
                                    $from_user_details = '<span class="timeline-body-time font-grey-cascade">[ Designation: ' . $senderDetails['name'] . ' ' . $senderDetails['name'] . ' ]</spn>';
                                    break;
                                default :
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    ?>


                    <!-- TIMELINE ITEM -->
                    <div class="timeline-item">
                        <div class="timeline-badge">
                            <img class="timeline-badge-userpic" src="upload/documents/<?php echo empty($messagesInfo['image']) ? 'no_image.jpg' : $messagesInfo['image']; ?>" title="<?php echo $messagesInfo['full_name']; ?> messaged you" style="height: 80px; width: 80px;">
                        </div>
                        <div class="timeline-body">
                            <div class="timeline-body-arrow">
                            </div>
                            <div class="timeline-body-head">
                                <div class="timeline-body-head-caption">
                                    <a href="javascript:;" class="timeline-body-title font-blue-madison"><?php echo $messagesInfo['full_name']; ?></a>
                                    <?php echo $from_user_details; ?>
                                    <span class="timeline-body-time font-grey-cascade"> Messaged on <?php echo date('Y-m-d H:i:s', strtotime($messagesInfo['send_date'])); ?></span>
                                </div>
                            </div>
                            <div class="timeline-body-content">
                                <span class="font-grey-cascade">
                                    <?php echo strip_tags($messagesInfo['message']); ?>    
                                </span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<div class="page-footer">
    <?php include("includes/footer.php"); ?>
</div>

<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script src="assets/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/timeline.js" type="text/javascript"></script>
<script>

    jQuery(document).ready(function () {

        Metronic.init(); // init metronic core components

        Layout.init(); // init current layout

        QuickSidebar.init(); // init quick sidebar

        Demo.init(); // init demo features

        TableEditable.init();

    });

</script>
<script type="text/javascript">
    $(document).ready(function () {

        $(".san_open").parent().parent().addClass("active open");
    });
</script>


</body><!-- END BODY -->
</html>