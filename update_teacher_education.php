<?php 
include_once("./includes/session.php");
//include_once("includes/config.php");
include_once("./includes/config.php");
$url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
?>
<?php

if(isset($_REQUEST['submit']))
{

	

	$teacher_id = isset($_POST['teacher_id']) ? $_POST['teacher_id'] : '';
	$education_level = isset($_POST['education_level']) ? $_POST['education_level'] : '';
	$exam_title = isset($_POST['exam_title']) ? $_POST['exam_title'] : '';
	$board = isset($_POST['board']) ? $_POST['board'] : '';
	$institution_name = isset($_POST['institution_name']) ? $_POST['institution_name'] : '';
	$passyear = isset($_POST['passyear']) ? $_POST['passyear'] : '';
	$main_subject = isset($_POST['main_subject']) ? $_POST['main_subject'] : '';
	$result_type = isset($_POST['result_type']) ? $_POST['result_type'] : '';
	$result_dev = isset($_POST['result_dev']) ? $_POST['result_dev'] : '';
	$result_gra = isset($_POST['result_gra']) ? $_POST['result_gra'] : '';
	$gra_point = isset($_POST['gra_point']) ? $_POST['gra_point'] : '';
	$gra_scale = isset($_POST['gra_scale']) ? $_POST['gra_scale'] : '';
	$duration = isset($_POST['duration']) ? $_POST['duration'] : '';
	
	$fields = array(
		'teacher_id' => mysql_real_escape_string($teacher_id),
		'education_level' => mysql_real_escape_string($education_level),
		'exam_title' => mysql_real_escape_string($exam_title),
		'board' => mysql_real_escape_string($board),
		'institution_name' => mysql_real_escape_string($institution_name),
		'passyear' => mysql_real_escape_string($passyear),
		'main_subject' => mysql_real_escape_string($main_subject),
		'result_type' => mysql_real_escape_string($result_type),
		'result_dev' => mysql_real_escape_string($result_dev),
		'result_gra' => mysql_real_escape_string($result_gra),
		'gra_point' => mysql_real_escape_string($gra_point),
		'gra_scale' => mysql_real_escape_string($gra_scale),
		'duration' => mysql_real_escape_string($duration)
		);

		$fieldsList = array();
		foreach ($fields as $field => $value) {
			$fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";
		}
					 
	 if($_REQUEST['action']=='edit')
	  {		  
	 $editQuery = "UPDATE `school_teachers_educations` SET " . implode(', ', $fieldsList)
			. " WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'";
			
		//	exit;

		if (mysql_query($editQuery)) {
			$_SESSION['msg'] = "Education details Updated Successfully";
		}
		else {
			$_SESSION['msg'] = "Error occuried while updating Education details";
		}
		if($_FILES['image']['tmp_name']!='')
			{
			$target_path="upload/documents/";
			$userfile_name = $_FILES['image']['name'];
			$userfile_tmp = $_FILES['image']['tmp_name'];
			$img_name =time().$userfile_name;
			$img=$target_path.$img_name;
			move_uploaded_file($userfile_tmp, $img);
			
			mysql_query("UPDATE `school_teachers_educations` set `image`='".$img_name."' WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'");	
			}		
			
			
		header('Location:update_teacher_education.php?upd');
		exit();
	
	 }
	 else
	 {
	 
	$addQuery = "INSERT INTO `school_teachers_educations` (`" . implode('`,`', array_keys($fields)) . "`)"
			. " VALUES ('" . implode("','", array_values($fields)) . "')";
			
		//exit;	
		 mysql_query($addQuery);
		$last_id=mysql_insert_id();
		
		if($_FILES['image']['tmp_name']!='')
			{
			$target_path="upload/documents/";
			$userfile_name = $_FILES['image']['name'];
			$userfile_tmp = $_FILES['image']['tmp_name'];
			$img_name =time().$userfile_name;
			$img=$target_path.$img_name;
			move_uploaded_file($userfile_tmp, $img);
			
			mysql_query("UPDATE `school_teachers_educations` set `image`='".$img_name."' WHERE `id`='".mysql_real_escape_string($last_id)."'");	
			}	
		
		if ($last_id) {
			$_SESSION['msg'] = "Education details Added Successfully";
		}
		else {
			$_SESSION['msg'] = "Error occuried while adding Education details";
		}

		header('Location:update_teacher_education.php?ins');
		exit();
	
	 }
				
				
}


if(isset($_GET['action']) && $_GET['action']=='delete')

{

	$item_id=$_GET['cid'];

	

	$deleteQry = "DELETE FROM `school_teachers_educations` WHERE `id` = '" . mysql_real_escape_string($item_id) . "'";

	mysql_query($deleteQry);	

	header('Location:update_teacher_education.php');

	exit();

}




if($_REQUEST['action']=='edit')
{
$categoryRowset = mysql_fetch_array(mysql_query("SELECT * FROM `school_teachers_educations` WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'"));

}

//Division list

$sql_division = mysql_query("SELECT * FROM `school_divisions` WHERE id <> ''");

$sql_districts = mysql_query("SELECT * FROM `school_districts` WHERE id <> ''");
?>

<script language="javascript">

   function del(aa,bb)

   {

      var a=confirm("Are you sure, you want to delete this?")

      if (a)

      {

        location.href="update_teacher_education.php?cid="+ aa +"&action=delete"

      }  

   } 

   




   </script>


<?php include("includes/header.php"); ?>

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include("includes/left_panel.php"); ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN STYLE CUSTOMIZER -->
			
			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Education Info
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.php">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					
					
					<li>
						<a href="#">Education Info</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<!--<li>
						<a href="#">Editable Datatables</a>
					</li>-->
				</ul>
				<!-- <div class="btn-group" style="float:right">
				<button id="sample_editable_1_new" class="btn blue" onclick="location.href='#'">
				Add New Teacher  <i class="fa fa-plus"></i>
				</button>
				</div> -->
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<?php 
						if(isset($_REQUEST['upd']))
						{
							echo '<p style="color:green; text-align:center;">'.$_SESSION['msg'].'</p>';
						}elseif ($_REQUEST['ins']) {
							echo '<p style="color:green; text-align:center;">'.$_SESSION['msg'].'</p>';
						}
					?>
					<p></p>
					<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php echo $_REQUEST['action']=='edit'?"Edit":"Add";?> Education
										</div>
										<div class="tools">
											
											
											
										</div>
									</div>
										<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<form  class="form-horizontal" method="post" action="update_teacher_education.php" enctype="multipart/form-data">
										<input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />

										<input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>" />

										<input type="hidden" name="teacher_id" value="<?php echo $_SESSION['username'];?>" />
										<div class="form-body">
										
										<div class="form-group">
										<label class="col-md-2 control-label">Education Level</label>
										<div class="col-md-4">
										<input type="text" class="form-control"  placeholder="Enter text"  value="<?php echo $categoryRowset['education_level'];?>" name="education_level" required>
										</div>
										
																				<label class="col-md-2 control-label">Examination name (Ex. SSC,HSC etc.)</label>
										<div class="col-md-4">
										<select class="form-control" id="" name="exam_title">
										<option value="">-Select-</option>
										<?php 
										
											$sql_exm = "SELECT * FROM  `school_exams` where 1";
											$result_exm = mysql_query($sql_exm);

											while($row1_exm= mysql_fetch_array($result_exm))
											{ 
											?>
											<option value="<?php echo $row1_exm['name']; ?>" <?php if($categoryRowset['exam_title']==$row1_exm['name']) { echo "selected";}?> > <?php echo $row1_exm['name']; ?></option>
										<?php
											}
										
										?>
										
										</select>

										</div>
										</div>
										
										





										<div class="form-group">
										<label class="col-md-2 control-label">Board</label>
										<div class="col-md-4">
										<select class="form-control" id="" name="board">
										<option value="">-Select-</option>
										<?php 
										
											$sql_brd = "SELECT * FROM  `school_board` where 1";
											$result_brd = mysql_query($sql_brd);

											while($row1_brd = mysql_fetch_array($result_brd))
											{ 
											?>
											<option value="<?php echo $row1_brd['name']; ?>" <?php if($categoryRowset['board']==$row1_brd['name']) { echo "selected";}?> > <?php echo $row1_brd['name']; ?></option>
										<?php
											}
										
										?>
										
										</select>

										</div>
										
																				<label class="col-md-2 control-label">Institute</label>
										<div class="col-md-4">
										<input type="text" class="form-control"  placeholder="Enter Institute"  value="<?php echo $categoryRowset['institution_name'];?>" name="institution_name" required>
										</div>
										</div>
										
										
										



										<div class="form-group">
										<label class="col-md-2 control-label">Year of passing</label>
										<div class="col-md-4">
										<select class="form-control" id="" name="passyear">
										<option value="">-Select-</option>
										<?php 
												for($i=date('Y');$i>=1967;$i--)
												{
										?>
										<option value="<?php echo $i; ?>" <?php if($categoryRowset['passyear'] == $i){ ?>selected<?php }?>><?php echo $i; ?></option>
										<?php			
												}

										?>
										
										</select>

										</div>
										
												<label class="col-md-2 control-label">Group/Subject</label>
										<div class="col-md-4">
										<select class="form-control" id="" name="main_subject">
										<option value="">-Select-</option>
										<?php 
										
										$sql_sub = "SELECT * FROM  `school_group` where `status`='0' order by `name`";
										$result_sub = mysql_query($sql_sub);
										
										while($row1_sub= mysql_fetch_array($result_sub))
										{ 
										?>
										<option value="<?php echo $row1_sub['name']; ?>" <?php if($categoryRowset['main_subject']==$row1_sub['name']) { echo "selected";}?> ><?php echo $row1_sub['name']; ?></option>
										<?php
										}
										
										?>
										
										</select>

										</div>
										</div>

										

										<div class="form-group">
										<label class="col-md-2 control-label">Result Type</label>
										<div class="col-md-4">
						<select class="form-control" id="result_type" name="result_type" onChange="get_sub_res(this.value)">
							<option value="">-Select-</option>
							<?php 
								$sql_res_type = mysql_query("SELECT * FROM  `school_result_type` where id <> ''");
								while($row_res_type = mysql_fetch_array($sql_res_type)){
							?>
								<option value="<?php echo $row_res_type['id'];?>" <?php if($row_res_type['id'] == $categoryRowset['result_type']){?>selected<?php }?>><?php echo $row_res_type['name'];?></option>
							<?php
								}
							?>
						</select>

										</div>
										
															<label class="col-md-2 control-label">Division/Grade</label>
										<div class="col-md-4">
										<select class="form-control" id="result_dev" name="result_dev">
											<option value="">-Select Result Type</option>
											<?php /*?><?php 
								$sql_res_sub = mysql_query("SELECT * FROM  `school_result_division` where id <> ''");
								while($row_res_sub = mysql_fetch_array($sql_res_sub)){
							?>
								<option value="<?php echo $row_res_sub['id'];?>" <?php if($row_res_sub['id'] == $categoryRowset['result_dev']){?>selected<?php }?>><?php echo $row_res_sub['name'];?></option>
							<?php
								}
							?><?php */?>
										</select>

										</div>
										</div>

										

										<div class="form-group" style="display:none;">
										<label class="col-md-2 control-label">Result Grade</label>
										<div class="col-md-4">
										<select class="form-control" id="" name="result_gra">
											<option value="">-Select-</option>
											<option value="A+" <?php if($categoryRowset['result_gra'] == 'A+'){ ?>selected<?php }?>>A+</option>
											<option value="A" <?php if($categoryRowset['result_gra'] == 'A'){ ?>selected<?php }?>>A</option>
											<option value="B+" <?php if($categoryRowset['result_gra'] == 'B+'){ ?>selected<?php }?>>B+</option>
											<option value="B" <?php if($categoryRowset['result_gra'] == 'B'){ ?>selected<?php }?>>B</option>
											<option value="A-" <?php if($categoryRowset['result_gra'] == 'A-'){ ?>selected<?php }?>>A-</option>
											<option value="C+" <?php if($categoryRowset['result_gra'] == 'C+'){ ?>selected<?php }?>>C+</option>
											<option value="C" <?php if($categoryRowset['result_gra'] == 'C'){ ?>selected<?php }?>>C</option>
											<option value="D" <?php if($categoryRowset['result_gra'] == 'D'){ ?>selected<?php }?>>D</option>
										</select>

										</div>
										
													<label class="col-md-2 control-label">Grade Point</label>
										<div class="col-md-4">
										<input type="text" class="form-control"  placeholder="Enter text"  value="<?php echo $categoryRowset['gra_point'];?>" name="gra_point">
										</div>
										</div>

										
										
										<div class="form-group">
										<label class="col-md-2 control-label">Grade Scale</label>
										<div class="col-md-4">


										<select class="form-control" id="" name="gra_scale">
										<option value="">-Select-</option>
										<?php 

										$sql_gr = "SELECT * FROM  `school_grade_scale` where 1";
										$result_gr = mysql_query($sql_gr);

										while($row1_gr= mysql_fetch_array($result_gr))
										{ 
										?>
										<option value="<?php echo $row1_gr['name']; ?>" <?php if($categoryRowset['gra_scale']==$row1_gr['name']) { echo "selected";}?> > <?php echo $row1_gr['name']; ?></option>
										<?php
										}

										?>

										</select>

										</div>
										
											<label class="col-md-2 control-label">Duration</label>
										<div class="col-md-4">
										<input type="text" class="form-control"  placeholder="Enter text"  value="<?php echo $categoryRowset['duration'];?>" name="duration" required>
										</div>
										</div>


										

										<div class="form-group">
										<label class="col-md-2 control-label">Supporting Documents</label>
										<div class="col-md-4">
										<input type="file" class="form-control"  placeholder="Enter document"  value="" name="image">
										</div>
										<?php 
											if($categoryRowset['image']){echo '<a href="upload/documents/'.$categoryRowset['image'].'" target="_blank">View</a>';}
										?>
										</div>

										
										</div>

										<div class="form-actions fluid">
										<div class="row">
										<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn blue"  name="submit">Submit</button>

										</div>
										</div>
										</div>
										</form>
										<!-- END FORM-->
										</div>
								</div>
				</div>

				<?php 
						if(!isset($_REQUEST['action'])){
				?>
				<div class="col-md-12">
					
					<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
			<h3>Education Details</h3>
				<thead>
					<tr>
						<th>Educational Level</th>
						<th>Examination</th>
						<th>Subject</th>
						<th>Institution</th>
						<th>Board</th>
						<th>Passing Year</th>
						<th>Result Type</th>
						<th>Result Division</th>
						<th>Result Grade</th>
						<th>Grade Scale</th>
						<th>Duration</th>
						<th>Action</th>
					</tr>

				</thead>
				<tbody>
				<?php 
					$sql_education = "SELECT * FROM  `school_teachers_educations` WHERE `teacher_id` = '".$_SESSION['username']."'";
					$res_education = mysql_query($sql_education);

					while ($row_education = mysql_fetch_array($res_education)) 
					{
					
				?>
					<tr>
						<td><?php echo $row_education['education_level']; ?></td>
						<td><?php echo $row_education['exam_title']; ?></td>
						<td><?php echo $row_education['main_subject']; ?></td>
						<td><?php echo $row_education['institution_name']; ?></td>
						<td><?php echo $row_education['board']; ?></td>
						<td><?php echo $row_education['passyear']; ?></td>
						<td><?php if($row_education['result_type'] == 2){echo 'Division';}elseif($row_education['result_type'] == 3){echo 'GPA';}else{echo 'CGPA';} ?></td>
						<td><?php if($row_education['result_dev'] == 1){echo 'Second Division';}elseif($row_education['result_dev'] == 2){echo 'First Division';}elseif($row_education['result_dev'] == 3){echo 'Thrid Division';} ?></td>
						<td><?php if($row_education['result_dev'] == 7){echo 'A+';}elseif($row_education['result_dev'] == 8){echo 'A';}elseif($row_education['result_dev'] == 9){echo 'B+';}elseif($row_education['result_dev'] == 10){echo 'B';}elseif($row_education['result_dev'] == 11){echo 'C+';}elseif($row_education['result_dev'] == 12){echo 'C';}elseif($row_education['result_dev'] == 13){echo 'D+';}elseif($row_education['result_dev'] == 14){echo 'D';} ?></td>
						
						<td><?php echo $row_education['gra_scale']; ?></td>
						<td><?php echo $row_education['duration']; ?></td>
						<td>

						<a href="update_teacher_education.php?id=<?php echo $row_education['id']; ?>&action=edit"><i class="glyphicon glyphicon-pencil"></i> </a>	&nbsp;

						<a onClick="javascript:del('<?php echo $row_education['id']; ?>')"><i class="glyphicon glyphicon-trash"></i> </a>

						</td>
					</tr>
				<?php 
					}
				?>	
				</tbody>
			</table>
				</div>
				<?php } ?>
			</div>
			<!-- END PAGE CONTENT -->
		</div>
	</div>
	<!-- END CONTENT -->
	
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<div class="page-footer">
	<?php include("includes/footer.php"); ?>
</div>
<style>
#grade{display:none}
</style>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/table-editable.js"></script>
<script>
jQuery(document).ready(function() {       
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init(); // init quick sidebar
Demo.init(); // init demo features
   TableEditable.init();
});
</script>

<script type="text/javascript">
function deleteConfirm(){
    var result = confirm("Are you sure to delete product?");
    if(result){
        return true;
    }else{
        return false;
    }
}

$(document).ready(function(){
    $('#select_all').on('click',function(){
        if(this.checked){
            $('.checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.checkbox').on('click',function(){
        if($('.checkbox:checked').length == $('.checkbox').length)
        {
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
    });
});
    
 



$(document).ready(function(){
    $(".san_open").parent().parent().addClass("active open");
});


function get_sub_res(id){
	 $.ajax({
                    type: "post",
                    url: "ajax_result.php?action=res",
                    data: {id: id},
                    success: function (msg) {
                        $('#result_dev').html(msg);
						
						
                    }
                });
				
				if(id == 4){
					$('#grade').css("display", "block");
				}else{
					$('#grade').css("display", "none");
				}
}
</script>
</body>
<!-- END BODY -->
</html>
